package com.reeftechnology.realestate.webui.appmanager;

import com.reeftechnology.realestate.webui.model.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class StructureHelper extends LocationHelper {

    public StructureHelper(WebDriver driver) {
        super(driver);
    }

    public void initCreation() {
        click(By.cssSelector("[data-testid=\"dti-add-new-structure-btn\"]"));
        waitForElementPresence(By.cssSelector("[data-testid=\"dti-structure-name-input\"]"));
    }

    public void fillStructureForm(StructureData structureData) {
        type(By.cssSelector("[data-testid=\"dti-structure-name-input\"]"), structureData.getStructureName());

        //select type
        click(By.cssSelector("[data-testid=\"dti-structure-form-type-select\"]"));
        click(By.cssSelector("li[data-value=\"" + structureData.getStructureType() + "\"]"));

        //select subtype
        click(By.cssSelector("[data-testid=\"dti-structure-form-subtype-select\"]"));
        click(By.cssSelector("li[data-value=\"" + structureData.getStructureSubType() + "\"]"));

        //select mode
        click(By.cssSelector("[data-testid=\"dti-structure-form-mode-select\"]"));
        click(By.cssSelector("li[data-value=\"" + structureData.getStructureMode() + "\"]"));

        //select equipment type
        click(By.cssSelector("[data-testid=\"dti-structure-form-equipment-type-select\"]"));
        click(By.cssSelector("li[data-value=\"" + structureData.getStructureEquipmentType() + "\"]"));

        type(By.cssSelector("[data-testid=\"dti-structure-form-equipment-description-input\"]"), structureData.getDescription());
    }

    public void submitStructureCreation() {
        click(By.cssSelector("[data-testid=\"dti-structure-form-dialog-submit-btn\"]"));
    }

    public void create(StructureData structureData) {
        //navigate to structure form
        click(By.cssSelector("[data-testid=\"dti-form-tab-structures\"]"));
        if (isElementPresent(By.cssSelector("img[data-testid=\"dti-checklist-img\"]"))) {
            initCreation();
            fillStructureForm(structureData);
            submitStructureCreation();
        }
    }

    public void edit() {
        click(By.cssSelector("button[aria-label=\"Edit\"]"));
    }

    public SurfaceData infoFromSurfaceForm() {
        String surfaceType = wd.findElements(By.cssSelector("p.MuiTypography-body2.MuiTypography-gutterBottom")).get(0).getText();
        String surfaceMode = wd.findElements(By.cssSelector("p.MuiTypography-body2.MuiTypography-gutterBottom")).get(1).getText();
        String description = wd.findElements(By.cssSelector("p.MuiTypography-body2.MuiTypography-gutterBottom")).get(2).getText();
        return new SurfaceData().withType(surfaceType.toUpperCase()).withMode(surfaceMode.toUpperCase())
                .withDescription(description);
    }

    public void modify(StructureData structureData) {
        click(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
        //click on edit surfaces button
        click(By.cssSelector("[data-testid=\"surfaces-details\"] button[aria-label=\"Edit\"]"));
        fillStructureForm(structureData);
        submitStructureCreation();
    }
}

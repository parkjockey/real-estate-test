package com.reeftechnology.realestate.webui.appmanager;

import com.reeftechnology.realestate.webui.model.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class LocationHelper extends HelperBase {

    public LocationHelper(WebDriver driver) {
        super(driver);
    }

    public void provideRealEstateID(LocationData location) {
        if (location.getFullID() == null) {
            click(By.cssSelector("[data-testid=\"dti-address-line1-input\"]"));
            click(By.xpath("//input[@value=\"autoGenerate\"]/../.."));
        } else {
            click(By.xpath("//input[@value=\"manualInput\"]/../.."));
            type(By.cssSelector("input[name=\"realEstateId\"]"), location.getFullID());
        }
    }

    public void addCampusToLocation(String campusType, String campusName) {
        click(By.cssSelector("[data-testid=\"dti-link-campus-button\"]"));
        waitForElementVisibility(By.cssSelector("[data-testid=\"dti-reef-undefined-dialog-close-btn\"]"));

        //select campus type - Landlord
        click(By.cssSelector("div[role=\"dialog\"] div[role=\"button\"]"));
        click(By.cssSelector("li[data-value=\"" + campusType + "\"]"));

        //type campus name and id - CU-1064629
        type(By.cssSelector("[name=\"campusId\"]"), campusName);
        clickElementByIndex(By.cssSelector("div.real-estate-MuiAutocomplete-popper div li"), 0);

        //click on connect campus button
        click(By.cssSelector("[data-testid=\"dti-undefined-dialog-submit-btn\"]"));
    }

    public void fillAddLocationForm(LocationData location) {
        type(By.name("locationName"), location.getLocationName());

        provideRealEstateID(location);

        if (!location.getAddressLine1().isEmpty()) {
            //navigate to address form
            type(By.name("addressLine1"), location.getAddressLine1());
            type(By.name("addressLine2"), location.getAddressLine2());
            type(By.name("city"), location.getCity());
            type(By.name("zipCode"), location.getZipCode());
            click(By.cssSelector("[data-testid=\"dti-country-select\"]"));
            if (isElementPresent(By.cssSelector("ul[role=\"listbox\"]"))) {
                click(By.cssSelector("li[data-value=\"" + location.getCountry() + "\"]"));
            }
            click(By.cssSelector("[data-testid='dti-state-select']"));
            if (isElementPresent(By.cssSelector("ul[role=\"listbox\"]"))) {
                click(By.cssSelector("li[data-value=\"" + location.getState() + "\"]"));
            }
        }
    }

    public void fillLocationForm(LocationData location, boolean externalIdRequired,
                                 boolean propertyTypeRequired, boolean operatingCompanyRequired) {
        waitForSpinnerToDisappear();
        //navigate to location details form
        //clickElementByIndex(By.cssSelector("[data-testid=\"dti-toast-hook\"]"), 0);
        click(By.cssSelector("[data-testid=\"dti-form-tab-details\"]"));

        type(By.name("locationName"), location.getLocationName());

        type(By.name("propertyName"), location.getPropertyName());

        //select property type
        if (propertyTypeRequired) {
            scrollToBottomOfPage();
            click(By.cssSelector("[id=\"mui-component-select-propertyType\"]")); //[data-testid="dti-location-type-input"]
            click(By.cssSelector("li[data-value=\"" + location.getPropertyType() + "\"]"));
            /* new Select(wd.findElements(By.cssSelector("li[role='option']"))).selectByVisibleText(location.getRegion());*/
        }

        //select region
        if (!location.getRegion().isEmpty()) {
            scrollToBottomOfPage();
            click(By.cssSelector("[id=\"mui-component-select-region\"]"));
            click(By.cssSelector("li[data-value=\"" + location.getRegion() + "\"]"));
            /* new Select(wd.findElements(By.cssSelector("li[role='option']"))).selectByVisibleText(location.getRegion());*/
        }

        //select operating company
        if (operatingCompanyRequired) {
            scrollToBottomOfPage();
            click(By.cssSelector("[id=\"mui-component-select-operatingCompanyCode\"]"));
            click(By.cssSelector("li[data-value=\"" + location.getOperatingCompany() + "\"]"));
        }

        //selects location status as active
        scrollTotopOfPage();
        clickElementByIndex(By.cssSelector("[data-testid=\"dti-location-status-radio-group\"] label"), 0);

        if (externalIdRequired) {
            By externalIDTypeElement = By.cssSelector("[data-testid=\"dti-external-id-type-0-select\"]");
            if (!isElementPresent(externalIDTypeElement)) {
                click(By.cssSelector("[data-testid=\"dti-btn-section-add-external-id\"]"));
            }
            click(externalIDTypeElement);
            if (isElementPresent(By.cssSelector("ul[role=\"listbox\"]"))) {
                click(By.cssSelector("li[data-value=\"" + location.getExternalIdType() + "\"]"));
            }
            type(By.cssSelector("[data-testid=\"dti-external-id-value-0-input\"]"), location.getExternalIdValue());
        } else {
            System.out.println("There is no external ID field defined for that location");
        }

        if (!location.getCampusName().isEmpty()) {
            addCampusToLocation("Landlord", "California State University");
        }

        if (!location.getAddressLine1().isEmpty()) {
            //navigate to address form
            scrollTotopOfPage();
            click(By.cssSelector("[data-testid=\"dti-form-tab-address\"]"));
            type(By.name("addressLine1"), location.getAddressLine1());
            type(By.name("addressLine2"), location.getAddressLine2());
            type(By.name("city"), location.getCity());
            type(By.name("zipCode"), location.getZipCode());
            click(By.id("mui-component-select-country"));
            if (isElementPresent(By.cssSelector("ul[role=\"listbox\"]"))) {
                click(By.cssSelector("li[data-value=\"" + location.getCountry() + "\"]"));
            }
            click(By.cssSelector("[data-testid='dti-state-select']"));
            if (isElementPresent(By.cssSelector("ul[role=\"listbox\"]"))) {
                click(By.cssSelector("li[data-value=\"" + location.getState() + "\"]"));
            }
        }

        /*{
            WebElement element = wd.findElement(By.cssSelector(".MuiButton-containedPrimary > .MuiButton-label"));
            Actions builder = new Actions(wd);
            builder.moveToElement(element).perform();
        }*/

    }

    public void initLocationCreation() {
        waitForSpinnerToDisappear();
        click(By.cssSelector("[data-testid=\"add-new-location-button\"]"));
        //wait for add new location form to be visible
        waitForElementVisibility(By.cssSelector("[data-testid=\"dti-dialog-close-btn\"]"));
    }

    public void initLocationCreationFromDialogueForm() {
        click(By.cssSelector("[data-testid=\"dti-add-new-location-form-dialog-submit-btn\"]"));
    }

    public void selectLocationById(int partialId) {
        waitForElementVisibility(By.cssSelector("button[aria-label=\"expand row\"]"));
        List<WebElement> elements = wd.findElements(By.cssSelector("[data-testid=\"dti-table-header-row\"]"));
        for (WebElement e : elements) {
            String[] locationString = e.getAttribute("outerText").split("\\s+");
            String[] fullid = locationString[5].split("-");
            String[] idWithOutStatus = fullid[3].split("\n");
            int id = Integer.parseInt(idWithOutStatus[0]);
            if (id == partialId) {
                e.click();
                waitForElementPresence(By.cssSelector("[data-testid=\"action-menu-button\"]"));
                click(By.cssSelector("[data-testid=\"action-menu-button\"]"));
                return;
            }
        }
    }

    public void selectModifiedLocationById(int partialId) {
        waitForSpinnerToDisappear();
        waitForElementVisibility(By.cssSelector("button[aria-label=\"expand row\"]"));
        List<WebElement> elements = wd.findElements(By.cssSelector("[data-testid=\"dti-table-header-row\"]"));
        for (WebElement e : elements) {
            String[] locationString = e.getAttribute("outerText").split("\\s+");
            String[] fullid = locationString[4].split("-");
            String[] idWithOutStatus = fullid[3].split("\n");
            int id = Integer.parseInt(idWithOutStatus[0]);
            if (id == partialId) {
                e.click();
                waitForElementPresence(By.cssSelector("[data-testid=\"action-menu-button\"]"));
                click(By.cssSelector("[data-testid=\"action-menu-button\"]"));
                return;
            }
        }
    }

    public void selectPendingApprovalLocationById(int partialId) {
        waitForElementVisibility(By.cssSelector("button[aria-label=\"expand row\"]"));
        List<WebElement> elements = wd.findElements(By.cssSelector("tr[role=\"checkbox\"] a[title=\"Publish or reject location\"]"));
        for (WebElement e : elements) {
            String[] fullid = e.getAttribute("innerText").split("-");
            String[] idWithOutStatus = fullid[3].split("\n");
            int id = Integer.parseInt(idWithOutStatus[0]);
            if (id == partialId) {
                e.click();
                return;
            }
        }
    }

    public void initLocationModification() {
        waitForElementPresence(By.cssSelector("[data-testid=\"section-0\"]"));
        click(By.cssSelector("button[data-testid=\"map-section-edit-button\"]"));
    }

    public void submitLocationCreation() {
        scrollTotopOfPage();
        click(By.cssSelector("[data-testid=\"dti-btn-submit-location\"]"));
    }

    public void submitLocationForApproval() {
        scrollTotopOfPage();
        click(By.cssSelector("[data-testid=\"dti-btn-send-for-approval-location\"]"));
        waitForSpinnerToDisappear();
    }

    public void submitLocationModification() {
        submitLocationCreation();
    }

    public boolean noResultsFound() {
        scrollTotopOfPage();
        return isElementPresent(By.cssSelector("[data-testid=\"dti-no-results-found\"]"));
    }

    public boolean noLocationsFound() {
        waitForSpinnerToDisappear();
        scrollTotopOfPage();
        waitForElementVisibility(By.cssSelector("[data-testid=\"dti-reef-table\"]"));
        return isElementPresent(By.xpath("//*[contains(text(), \"There are no locations for you to approve at this time.\")]"));
    }

    public int count() {
        waitForElementVisibility(By.cssSelector("[data-testid=\"dti-reef-table\"]"));
        return wd.findElements(By.cssSelector("[data-testid=\"dti-table-header-row\"]")).size();
    }

    //Get locations list
    public List<LocationData> list() {
        List<LocationData> locations = new ArrayList<LocationData>();
        List<WebElement> elements = wd.findElements(By.cssSelector("tr.MuiTableRow-root.MuiTableRow-hover"));
        for (WebElement e : elements) {
            String[] locationString = e.getAttribute("outerText").split("\\s+");
            String locationName = locationString[1];
            String[] fullid = locationString[0].split("-");
            int id = Integer.parseInt(fullid[3]);
            locations.add(new LocationData().withId(id).withLocationName(locationName));
        }
        return locations;
    }

    //Locations cache
    Locations locationCache = null;

    //Get locations set
    public Locations all() {
        if (locationCache != null) {
            return new Locations(locationCache);
        }
        locationCache = new Locations();
        //waitForElementsVisibility(By.cssSelector("[data-testid=\"dti-table-header-row\"]"));
        List<WebElement> elements = wd.findElements(By.cssSelector("[data-testid=\"dti-table-header-row\"]"));
        if (!elements.isEmpty()) {
            for (WebElement e : elements) {
                String[] locationString = e.getAttribute("innerText").split("\t");
                String locationName = locationString[1];
                String[] fullid = locationString[3].split("-");
                String allAddresses = locationString[5] + locationString[6] + locationString[7];
                String[] idWithOutCountryAndState = fullid[3].split("\n");
                int id = Integer.parseInt(idWithOutCountryAndState[0]);
                locationCache.add(new LocationData().withId(id).withLocationName(locationName)
                        .withAllAddresses(allAddresses));
            }
        }
        return locationCache;
    }

    public Locations pendingAll() {
        if (locationCache != null) {
            return new Locations(locationCache);
        }
        locationCache = new Locations();

        List<WebElement> elements = wd.findElements(By.cssSelector("[data-testid=\"dti-table-header-row\"]"));
        if (!elements.isEmpty()) {
            for (WebElement e : elements) {
                String[] locationString = e.getAttribute("innerText").split("\\t");
                String locationName = locationString[1];
                String[] fullid = locationString[2].split("-");
                String allAddresses = locationString[4];
                String[] idWithOutStatus = fullid[3].split("\n");
                int id = Integer.parseInt(idWithOutStatus[0]);
                locationCache.add(new LocationData().withId(id).withLocationName(locationName)
                        .withAllAddresses(allAddresses));
            }
        }
        return locationCache;
    }

    public void searchForLocation(String location) {
        refreshPage();
        waitForSpinnerToDisappear();
        By searchInputField = By.cssSelector("[data-testid=\"find-location-input\"] input");
        waitForElementVisibility(searchInputField);
        waitForSpinnerToDisappear();
        type(searchInputField, location);
        waitForSpinnerToDisappear();
        waitForElementVisibility(By.cssSelector("[data-testid=\"dti-reef-table\"]"));
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-table-header-row\"]"))) {
            waitForElementsVisibility(By.cssSelector("[data-testid=\"dti-table-header-row\"]"));
        } else {
            waitForElementVisibility(By.cssSelector("[data-testid=\"dti-reef-table\"]"));
            System.out.println(wd.findElement(By.cssSelector("[data-testid=\"dti-reef-table\"]")).getAttribute("value"));
        }
        waitForSpinnerToDisappear();
    }

    private void waitForSpinnerToDisappear() {
        waitForElementInVisibility(By.cssSelector("div[role=\"progressbar\"]"));
    }

    public void createDraftLocation(LocationData locationData) throws IOException {
        initLocationCreation();
        fillAddLocationForm(locationData);
        initLocationCreationFromDialogueForm();
        locationCache = null;
    }

    public void create(LocationData locationData) throws IOException {
        initLocationCreation();
        fillAddLocationForm(locationData);
        initLocationCreationFromDialogueForm();
        fillLocationForm(locationData, false, false, false);
        validateWithPrecisely(true);
        submitLocationCreation();
        locationCache = null;
    }

    public void createLocationForStructure(LocationData locationData) throws IOException {
        initLocationCreation();
        fillAddLocationForm(locationData);
        initLocationCreationFromDialogueForm();
        fillLocationForm(locationData, false, true, true);
        validateWithPrecisely(true);
    }

    public void createLocationAddSurface(LocationData locationData) throws IOException {
        initLocationCreation();
        fillLocationForm(locationData, false, true, false);
        validateWithPrecisely(true);
    }

    public void createLocationAddExternalId(LocationData locationData, boolean isInputValid) throws IOException {
        initLocationCreation();
        fillLocationForm(locationData, true, true, false);
        validateWithPrecisely(true);
        submitLocationCreation();
    }

    public void createLocationAddGeocoordinates(LocationData locationData, boolean isInputValid, boolean isCoordinatesValidateInCreate, boolean usePreciselydata) throws IOException {
        initLocationCreation();
        fillLocationForm(locationData, false, true, false);
        validateWithPrecisely(usePreciselydata);
        saveGeoCoordinatesBasedOnAddressData(locationData);
        /*if (isCoordinatesValidateInCreate) {
            waitForElementPresence(By.cssSelector("button[aria-label=\"Edit\"]"));
            assertThat(locationData.getLatitude(), equalTo(getLatitude()));
            assertThat(locationData.getLongitude(), equalTo(getLongitude()));
        }*/
        submitLocationCreation();
    }

    public void createLocationAddServices(LocationData locationData) throws IOException {
        initLocationCreation();
        fillLocationForm(locationData, false, true, false);
        validateWithPrecisely(true);
    }

    public void deleteSelectedLocations() {
        click(By.name("delete"));
    }

    public void delete(LocationData location) {
        selectLocationById(location.getId());
        deleteSelectedLocations();
        locationCache = null;
    }

    public void initLocationModificationById(int id) {
        WebElement checkbox = wd.findElement(By.cssSelector(String.format("input[value='%s']", id)));
        WebElement row = checkbox.findElement(By.xpath("./../.."));
        List<WebElement> cells = row.findElements(By.tagName("td"));
        cells.get(7).findElement(By.tagName("a")).click();

//    wd.findElement(By.xpath(String.format("//input[@value='%s']/../../td[8]/a", id))).click();
//    wd.findElement(By.xpath(String.format("//tr[.//input[@value='%s']]/td[8]/a", id))).click();
//    wd.findElement(By.cssSelector(String.format("a[href='edit.php?id=%s']", id))).click();

    }

    public LocationData infoFromEditForm(LocationData location) {
        initLocationModificationById(location.getId());
        String locationName = wd.findElement(By.name("locationName")).getAttribute("value");
        String country = wd.findElement(By.name("type")).getAttribute("value");
        String address = wd.findElement(By.name("address")).getAttribute("value");
        String city = wd.findElement(By.name("city")).getAttribute("value");
        String state = wd.findElement(By.name("state")).getAttribute("value");
        String property = wd.findElement(By.name("property")).getAttribute("value");
        String zipcode = wd.findElement(By.name("zipcode")).getAttribute("value");
        wd.navigate().back();
        return new LocationData().withId(location.getId()).withLocationName(locationName)
                .withAddressLine1(address).withCity(city)
                .withCountry(country).withPropertyName(property)
                .withState(state).withZipCode(zipcode);
    }

    public boolean isCreateLocationValidationMessageVisible() {
        By errorMessage = By.cssSelector("div.real-estate-MuiTextField-root p");
        waitForElementVisibility(errorMessage);
        String validationErrorLabel = wd.findElement(errorMessage).getAttribute("innerText");
        if (validationErrorLabel.equals("Invalid characters") || validationErrorLabel.equals("Must have at least one number and one letter")) {
            return true;
        } else {
            System.out.println("Validation errors are not visible");
            return false;
        }
    }

    public boolean isValidationPopUpErrorIsVisible() {
        String validationErrorLabel = wd.findElement(By.cssSelector("div.Toastify__toast-body p")).getAttribute("innerText");
        if (validationErrorLabel.equals("Unable to perform request")) {
            return true;
        } else {
            System.out.println("Unable to perform request - Validation errors are not visible");
            return false;
        }
    }

    private String mergeAddress(LocationData createdLocation) {
        return Arrays.asList(createdLocation.getAddressLine1(), createdLocation.getAddressLine2())
                .stream().filter((s) -> !s.equals(""))
                .map(LocationHelper::cleaned)
                .collect(Collectors.joining("\n"));
    }

    public static String cleaned(String address) {
        return address.replaceAll("\\s", "")
                .replaceAll("[-()]", "");
    }

    public String getAddLocationDetailsValidationErrorMessage() {
        //navigate to location details form
        click(By.cssSelector("[data-testid=\"dti-form-tab-details\"]"));
        return wd.findElement(By.cssSelector("p.real-estate-MuiFormHelperText-root.Mui-error.real-estate-MuiFormHelperText-filled")).getText();
    }

    public boolean getUnableToRetrieveLocationsMessage() {
        waitForPageListUpdate();
        return wd.findElement(By.cssSelector("td.real-estate-MuiTableCell-root.real-estate-MuiTableCell-body"))
                .getText().equals("Unable to retrieve locations.");
    }

    public void modify(LocationData modifiedLocation, LocationData modifiedLocationUpdateData) throws IOException {
        selectLocationById(modifiedLocation.getId());
        fillLocationForm(modifiedLocationUpdateData, false, false, false);
        submitLocationModification();
    }

    public void modifyLocationExternalId(LocationData modifiedLocation, LocationData modifiedLocationUpdateData) throws IOException {
        selectLocationById(modifiedLocation.getId());
        fillLocationForm(modifiedLocationUpdateData, true, false, false);
        submitLocationModification();
    }

    public void modifyLocationGeocoordinates(LocationData modifiedLocation, LocationData modifiedLocationUpdateData, boolean usePreciselyProvidedData) throws IOException {
        selectLocationById(modifiedLocation.getId());
        fillLocationForm(modifiedLocationUpdateData, false, false, false);
        validateWithPrecisely(usePreciselyProvidedData);
        saveGeoCoordinatesBasedOnAddressData(modifiedLocationUpdateData);
        submitLocationModification();
    }

    public void modifyLocationSurface(LocationData modifiedLocation, SurfaceData modifedSurface, SurfaceData existingSurface) throws IOException {
        selectLocationById(modifiedLocation.getId());
        SurfaceHelper surfaceHelper = new SurfaceHelper(wd);
        surfaceHelper.modify(modifedSurface);
    }

    public void modifyLocationService(LocationData modifiedLocation, ServicesData modifiedService) {
        selectLocationById(modifiedLocation.getId());
        ServicesHelper serviceHelper = new ServicesHelper(wd);
        serviceHelper.modify(modifiedService);
    }

    public LocationData getLocationInfo(int id) {
        waitForSpinnerToDisappear();
        waitForPageListUpdate();
        selectModifiedLocationById(id);
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-form-tab-details\"]"))) {
            click(By.cssSelector("[data-testid=\"dti-form-tab-details\"]"));
            String locationName = wd.findElement(By.cssSelector("[data-testid=\"dti-location-name-input\"]")).getAttribute("value");
            click(By.cssSelector("[data-testid=\"dti-form-tab-address\"]"));
            String locationAddressLine1 = wd.findElement(By.cssSelector("[data-testid=\"dti-address-line1-input\"]")).getAttribute("value");
            String locationAddressLine2 = wd.findElement(By.cssSelector("[data-testid=\"dti-address-line2-input\"]")).getAttribute("value");
            String locationCity = wd.findElement(By.cssSelector("[data-testid=\"dti-city-input\"]")).getAttribute("value");
            String locationZipCode = wd.findElement(By.cssSelector("[data-testid=\"dti-zip-code-input\"]")).getAttribute("value");
            String locationCountry = wd.findElement(By.cssSelector("[data-testid=\"dti-country-input\"]")).getAttribute("value");
            String locationState = wd.findElement(By.cssSelector("[data-testid=\"dti-state-input\"]")).getAttribute("value");
            return new LocationData().withLocationName(locationName)
                    .withAddressLine1(locationAddressLine1).withAddressLine2(locationAddressLine2)
                    .withCity(locationCity).withZipCode(locationZipCode)
                    .withCountry(locationCountry).withState(locationState);
        } else {
            return new LocationData();
        }
    }

    public LocationData getLocationExternalIdInfo(int id) {
        waitForPageListUpdate();
        selectLocationById(id);
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-form-tab-details\"]"))) {
            click(By.cssSelector("[data-testid=\"dti-form-tab-details\"]"));
            String locationName = wd.findElement(By.cssSelector("[data-testid=\"dti-location-name-input\"]")).getAttribute("value");
            String locationPropertyName = wd.findElement(By.cssSelector("[data-testid=\"dti-property-name-input\"]")).getAttribute("value");
            String externalIdentifierType = wd.findElement(By.cssSelector("[data-testid=\"dti-external-id-type-0-input\"]")).getAttribute("value");
            String externalIdentifierValue = wd.findElement(By.cssSelector("[data-testid=\"dti-external-id-value-0-input\"]")).getAttribute("value");
            click(By.cssSelector("[data-testid=\"dti-form-tab-address\"]"));
            String locationAddressLine1 = wd.findElement(By.cssSelector("[data-testid=\"dti-address-line1-input\"]")).getAttribute("value");
            String locationAddressLine2 = wd.findElement(By.cssSelector("[data-testid=\"dti-address-line2-input\"]")).getAttribute("value");
            String locationCity = wd.findElement(By.cssSelector("[data-testid=\"dti-city-input\"]")).getAttribute("value");
            String locationZipCode = wd.findElement(By.cssSelector("[data-testid=\"dti-zip-code-input\"]")).getAttribute("value");
            String locationCountry = wd.findElement(By.cssSelector("[data-testid=\"dti-country-input\"]")).getAttribute("value");
            String locationState = wd.findElement(By.cssSelector("[data-testid=\"dti-state-input\"]")).getAttribute("value");

            return new LocationData().withLocationName(locationName).withPropertyName(locationPropertyName)
                    .withExternalIdType(externalIdentifierType)
                    .withExternalIdValue(externalIdentifierValue)
                    .withAddressLine1(locationAddressLine1).withAddressLine2(locationAddressLine2)
                    .withCity(locationCity).withZipCode(locationZipCode)
                    .withCountry(locationCountry).withState(locationState);
        }
        return new LocationData();
    }

    public LocationData getLocationGeocoordinatesInfo(int id) {
        selectLocationById(id);
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-form-tab-details\"]"))) {
            click(By.cssSelector("[data-testid=\"dti-form-tab-details\"]"));
            String locationName = wd.findElement(By.cssSelector("[data-testid=\"dti-location-name-input\"]")).getAttribute("value");
            String locationType = wd.findElement(By.cssSelector("[data-testid=\"dti-location-type-input\"]")).getAttribute("value");
            String locationSubType = wd.findElement(By.cssSelector("[data-testid=\"dti-location-sub-type-input\"]")).getAttribute("value");
            String locationMode = wd.findElement(By.cssSelector("[data-testid=\"dti-location-mode-input\"]")).getAttribute("value");
            click(By.cssSelector("[data-testid=\"dti-form-tab-address\"]"));
            String locationAddressLine1 = wd.findElement(By.cssSelector("[data-testid=\"dti-address-line1-input\"]")).getAttribute("value");
            String locationAddressLine2 = wd.findElement(By.cssSelector("[data-testid=\"dti-address-line2-input\"]")).getAttribute("value");
            String locationCity = wd.findElement(By.cssSelector("[data-testid=\"dti-city-input\"]")).getAttribute("value");
            String locationZipCode = wd.findElement(By.cssSelector("[data-testid=\"dti-zip-code-input\"]")).getAttribute("value");
            String locationCountry = wd.findElement(By.cssSelector("[data-testid=\"dti-country-input\"]")).getAttribute("value");
            String locationState = wd.findElement(By.cssSelector("[data-testid=\"dti-state-input\"]")).getAttribute("value");

            // get values for longitude and latitude
            click(By.cssSelector("[data-testid=\"dti-form-tab-geocoordinates\"]"));
            waitForElementPresence(By.cssSelector("[data-testid=\"reef-map\"]"));
            String longitudeValue[] = wd.findElements(By.cssSelector("[data-testid=\"reef-map\"] p.real-estate-MuiTypography-root")).get(1).getAttribute("textContent").split(" ");
            String latitudeValue[] = wd.findElements(By.cssSelector("[data-testid=\"reef-map\"] p.real-estate-MuiTypography-root")).get(0).getAttribute("textContent").split(" ");
            Double longitude = Double.parseDouble(longitudeValue[1]);
            Double latitude = Double.parseDouble(latitudeValue[1]);


            return new LocationData().withLocationName(locationName)
                    .withLocationType(locationType)
                    .withLocationSubType(locationSubType)
                    .withLocationMode(locationMode)
                    .withAddressLine1(locationAddressLine1).withAddressLine2(locationAddressLine2)
                    .withCity(locationCity).withZipCode(locationZipCode)
                    .withCountry(locationCountry).withState(locationState)
                    .withLongitude(longitude).withLatitude(latitude);
        }
        return new LocationData();
    }

    public SurfaceData getLocationSurfaceInfo(int id) {
        waitForPageListUpdate();
        selectLocationById(id);
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"))) {
            click(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
            waitForElementPresence(By.cssSelector("button[aria-label=\"Edit\"]"));
            WebElement surfaceType = wd.findElements(By.cssSelector("[data-testid=\"surfaces-details\"] p")).get(0);
            WebElement surfaceMode = wd.findElements(By.cssSelector("[data-testid=\"surfaces-details\"] p")).get(1);
            WebElement surfaceDescription = wd.findElements(By.cssSelector("[data-testid=\"surfaces-details\"] p")).get(2);
            return new SurfaceData().withType(surfaceType.getText().toUpperCase())
                    .withMode(surfaceMode.getText().toUpperCase()).withDescription(surfaceDescription.getText());
        } else {
            return new SurfaceData();
        }
    }

    public ServicesData getLocationServiceInfo(int id) {
        selectLocationById(id);
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-form-tab-services\"]"))) {
            click(By.cssSelector("[data-testid=\"dti-form-tab-services\"]"));
            WebElement serviceType = wd.findElement(By.cssSelector("[data-testid=\"dti-service-type-0-select\"]"));
            WebElement serviceSubType = wd.findElement(By.cssSelector("[data-testid=\"dti-service-sub-type-0-select\"]"));
            WebElement serviceID = wd.findElement(By.cssSelector("[data-testid=\"dti-service-id-value-0-input\"]"));
            WebElement serviceDescription = wd.findElement(By.cssSelector("[data-testid=\"dti-service-description-value-0-input\"]"));
            return new ServicesData().withType(serviceType.getText())
                    .withSubtype(serviceSubType.getText()).withServiceID(serviceID.getAttribute("value")).withDescription(serviceDescription.getAttribute("value"));
        } else {
            return new ServicesData();
        }
    }

    public AccessPointData getLocationSurfaceAccessPointInfo(int id) {
        selectLocationById(id);
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"))) {
            click(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
            SurfaceHelper surfaceHelper = new SurfaceHelper(wd);
            return surfaceHelper.infoOnAccessPointFromSurfaceForm(true, false);
        }
        return new AccessPointData();
    }

    public SpaceData getLocationSurfaceSpaceInfo(int id) {
        selectLocationById(id);
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"))) {
            click(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
            SurfaceHelper surfaceHelper = new SurfaceHelper(wd);
            return surfaceHelper.infoOnSpaceFromSurfaceForm(true, false);
        }
        return new SpaceData();
    }


    public FeatureData getLocationSurfaceFeatureInfo(int id) {
        selectLocationById(id);
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"))) {
            click(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
            SurfaceHelper surfaceHelper = new SurfaceHelper(wd);
            return surfaceHelper.infoOnFeatureFromSurfaceForm(true, false);
        }
        return new FeatureData();
    }

    public void validateWithPrecisely(boolean usePreciselyProvidedData) throws IOException {
        click(By.cssSelector("button[data-testid=\"dti-btn-validate-with-precisely\"]"));
        waitForElementVisibility(By.id("customized-dialog-title"));
        if (usePreciselyProvidedData) {
            try {
                waitForElementPresence(By.cssSelector("#additional-actions1-header label"));
                click(By.cssSelector("#additional-actions1-header label"));
                System.out.println("Confidence level value is: " + wd.findElement(By.cssSelector("[id=\"additional-actions1-header\"] span.real-estate-MuiChip-label")).getAttribute("outerText"));
                clickElementByIndex(By.cssSelector("#additional-actions1-content button"), 0);
            } catch (TimeoutException e) {
                takeScreenshot();
                System.out.println("Precisely has not found any addresses based on your input. Please double-check the address you have entered. If necessary, edit it, then try validating again");
                click(By.cssSelector("button[aria-label=\"Close\"]"));
            }
        } else {
            try {
                wd.findElements(By.cssSelector("div.real-estate-MuiDialogContent-root button.real-estate-MuiButton-outlined")).get(1).click();
            } catch (IndexOutOfBoundsException e) {
                waitForElementPresence(By.cssSelector("div.MuiDialogContent-root button"));
                takeScreenshot();
                System.out.println("Precisely has not found any addresses based on your input. Please double-check the address you have entered. If necessary, edit it, then try validating again");
                click(By.cssSelector("button[aria-label=\"Close\"]"));
            }
        }

    }

    private void takeScreenshotGeo() throws IOException {
        //take screenshot for map location
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH_mm_ss");
        // Take a screenshot of the current page
        File screenshot = ((TakesScreenshot) wd).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("reports/webui/maps/screenshot"
                + dateTime.format(formatter) + ".png"));
    }

    public void saveGeoCoordinatesBasedOnAddressData(LocationData location) throws IOException {
        //navigate to geocoordinates form
        click(By.cssSelector("[data-testid=\"dti-form-tab-geocoordinates\"]"));

        if (isElementPresent(By.cssSelector("[data-testid=\"reef-map\"]"))) {
            By pinElement = By.cssSelector("[src=\"https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2_hdpi.png\"]");
            waitForElementPresence(pinElement);
            if (wd.findElement(pinElement).isDisplayed()) {
                takeScreenshotGeo();
            } else {
                System.out.println("There is no map displayed");
            }
            // get values for longitude and latitude
            //location.withLongitude(Double.parseDouble(wd.findElements(By.cssSelector("[data-testid=\"reef-map\"] p.real-estate-MuiTypography-root")).get(1).getAttribute("value")));
            //location.withLatitude(Double.parseDouble(wd.findElements(By.cssSelector("[data-testid=\"reef-map\"] p.real-estate-MuiTypography-root")).get(0).getAttribute("value")));
            click(By.cssSelector("[data-testid=\"dti-btn-geocoordinates-save-position\"]"));
        } else {
            //click on reset position
            click(By.cssSelector("[data-testid=\"dti-btn-geocoordinates-reset-position\"]"));
        }
    }

    public void approveLocation(int locationID) {
        selectPendingApprovalLocationById(locationID);
        if (isElementPresent(By.cssSelector("[data-testid=\"location-details-approve-page\"]"))) {
            click(By.cssSelector("[data-testid=\"dti-btn-approve-location\"]"));
            waitForElementVisibility(By.xpath("//*[contains(text(), \"Approving a location\")]"));
            click(By.xpath("//span[contains(text(), \"Approve Location\")]/.."));
        }
        waitForSpinnerToDisappear();
        waitForElementVisibility(By.cssSelector("[data-testid=\"dti-reef-table\"]"));
    }

    public void openLocationToView(String createdlocationID) {
        waitForPageListUpdate();
//        click(By.cssSelector("[title=\"Publish or reject location\"]"));
        waitForElementVisibility(By.cssSelector("[data-testid=\"action-menu-view-button-" + createdlocationID + "\"]"));
        click(By.cssSelector("[data-testid=\"action-menu-view-button-" + createdlocationID + "\"]"));
    }

    public String getLocationsApprovalStatus() {
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-page-header\"]"))) {
            String locationApprovalStatus = wd.findElement(By.cssSelector("[data-testid=\"dti-page-header\"] h6"))
                    .getAttribute("textContent");
            return locationApprovalStatus;
        } else {
            return "Status undefined";
        }
    }

}
package com.reeftechnology.realestate.webui.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SessionHelper extends HelperBase {

    public SessionHelper(WebDriver wd) {
        super(wd);
    }

    public void login(String username, String password) {
        By submitButton = By.cssSelector("[id=\"login-page-continue-button\"]");
        waitForElementVisibility(submitButton);
        type(By.name("email"), username);
        click(submitButton);
        type(By.name("password"), password);
        click(By.cssSelector("[id=\"login-page-login-button\"]"));
    }

    public void loginWithSSO(String username, String password) {
        type(By.name("email"), username);
        By submitButton = By.cssSelector("input[type=\"submit\"]");
        click(By.cssSelector("[data-testid=\"button\"]"));
        //Microsoft sign in form
        waitForElementPresence(By.id("idA_PWD_SwitchToCredPicker"));
        type(By.name("loginfmt"), username);
        click(submitButton);
        waitForElementPresence(By.id("idA_PWD_ForgotPassword"));
        type(By.cssSelector("input[type=\"password\"]"), password);
        click(submitButton);
        click(submitButton);
    }

    public void logout() {
       refreshPage();
       waitForElementVisibility(By.cssSelector("button[data-qa-name]"));
       click(By.cssSelector("button[data-qa-name=\"profile-avatar\"]"));
       wd.findElement(By.xpath("//li[contains(text(), 'Logout')]")).click();
    }
}

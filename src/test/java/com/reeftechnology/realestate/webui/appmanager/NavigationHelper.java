package com.reeftechnology.realestate.webui.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Properties;

public class NavigationHelper extends HelperBase {

    Properties properties;

    public NavigationHelper(WebDriver wd, Properties properties) {
        super(wd);
        this.properties = properties;
    }

    public void locationsPage() {
        By searchLocationBtn = By.cssSelector("[data-testid=\"find-location-icon-button\"]");
        if (isElementPresent(By.cssSelector("[data-testid=\"find-location-page\"] h4"))
                && wd.findElement(By.cssSelector("[data-testid=\"find-location-page\"] h4")).getText().equals("Locations")
                && isElementPresent(searchLocationBtn)) {
            waitForElementVisibility(searchLocationBtn);
            return;
        } else {
            //waitForElementVisibility(By.cssSelector("[data-testid=\"dti-actions-card-content\"]"));
            clickElementByIndex(By.cssSelector("li[role=\"treeitem\"] p"), 0);
            if (isElementPresent(By.cssSelector("[data-testid=\"dti-actions-card-content\"]"))) {
                click(By.cssSelector("[data-testid=\"dti-actions-card\"] a[href=\"/real-estate/locations\"] p"));
                waitForElementVisibility(searchLocationBtn);
            } else {
                clickElementByIndex(By.cssSelector("[data-testid=\"dti-actions-card\"] p"), 3);
            }
        }
    }

    public void locationsApprovalPage() {
        if (isElementPresent(By.tagName("h4"))
                && wd.findElement(By.tagName("h4")).getText().equals("Pending Approval")
        ) {
            return;
        } else {
            wd.get(properties.getProperty("web.BaseUrl"));
            waitForElementVisibility(By.cssSelector("[data-testid=\"dti-actions-card-content\"]"));
            click(By.cssSelector("[data-testid=\"dti-actions-card\"] a[href=\"/real-estate/locations/pending\"]"));
        }
    }

    public void homePage() throws IOException {
        wd.get(properties.getProperty("web.BaseUrl"));
    }

}

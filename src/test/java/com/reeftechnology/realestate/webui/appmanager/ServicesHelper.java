package com.reeftechnology.realestate.webui.appmanager;

import com.reeftechnology.realestate.webui.model.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ServicesHelper extends LocationHelper {

    public ServicesHelper(WebDriver driver) {
        super(driver);
    }

    public void initSurfaceCreationFromSurfaces() {
        wd.findElements(By.cssSelector("button.real-estate-MuiButton-containedPrimary.real-estate-MuiButton-disableElevation")).get(1).click();
        waitForElementPresence(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
        //TODO clarify with dev - why for second test run web element attribute is changing
        /*click(By.cssSelector("[testid=\"add-new-surface-button\"]"));
        click(By.cssSelector("[data-testid=\"add-new-surface-button\"]"));*/
    }

    public void fillServicesForm(ServicesData servicesData) {
        click(By.cssSelector("[data-testid=\"dti-service-type-0-select\"]"));
        click(By.cssSelector("li[data-value=\"" + servicesData.getType() + "\"]"));

        click(By.cssSelector("[data-testid=\"dti-service-sub-type-0-select\"]"));
        click(By.cssSelector("li[data-value=\"" + servicesData.getSubtype() + "\"]"));

        type(By.cssSelector("input[data-testid=\"dti-service-id-value-0-input\"]"), servicesData.getServiceID());
        type(By.cssSelector("input[data-testid=\"dti-service-description-value-0-input\"]"), servicesData.getDescription());
    }

    public void create(ServicesData servicesData) {
        click(By.cssSelector("[data-testid=\"dti-form-tab-services\"]"));
        if (isElementPresent(By.cssSelector("[data-testid=\"dti-no-services-section\"]"))) {
            //click on add services button
            click(By.cssSelector("[data-testid=\"dti-add-service-record-button\"]"));
            fillServicesForm(servicesData);
            submitLocationCreation();
        } else {
            return;
        }
    }

    public void modify(ServicesData servicesData) {
        click(By.cssSelector("[data-testid=\"dti-form-tab-services\"]"));
        fillServicesForm(servicesData);
        submitLocationCreation();
    }


    public void edit() {
        click(By.cssSelector("button[aria-label=\"Edit\"]"));
    }

}

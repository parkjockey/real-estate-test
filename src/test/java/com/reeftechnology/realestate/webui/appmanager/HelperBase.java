package com.reeftechnology.realestate.webui.appmanager;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class HelperBase {
    protected WebDriver wd;

    public HelperBase(WebDriver driver) {
        this.wd = driver;
    }

    protected void click(By locator) {
        WebElement element = null;
        try {
            element = wd.findElement(locator);
            waitForElementVisibility(locator);
        } catch (NoSuchElementException e) {
            System.out.println("Unable to locate element with specified locator: " + locator);
            try {
                takeScreenshot();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        try {
            Actions actions = new Actions(wd);
            actions.moveToElement(element);
            actions.perform();
        } catch (IllegalArgumentException e) {
            System.out.println("Unable to move to element: " + element);
            e.printStackTrace();
        }
        try {
            element.click();
        } catch (ElementClickInterceptedException e) {
            System.out.println("element click intercepted with specified locator: " + e.getMessage());
            element.click();
            e.printStackTrace();
        }
    }

    protected void clickElementByIndex(By locator, int elementIndex) {
        WebElement element = null;
        try {
            element = wd.findElements(locator).get(elementIndex);
            waitForElementVisibility(locator);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Unable to locate element with specified locator: " + locator);
            try {
                takeScreenshot();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        try {
            Actions actions = new Actions(wd);
            actions.moveToElement(element);
            actions.perform();
        } catch (IllegalArgumentException e) {
            System.out.println("Unable to move to element");
            e.printStackTrace();
        }
        element.click();
    }

    protected void type(By locator, String text) {
        click(locator);
        if (text != null) {
            String existingText = wd.findElement(locator).getAttribute("value");
            if (!text.equals(existingText)) {
                //wd.findElement(locator).clear();
                wd.findElement(locator).sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), " ");
                wd.findElement(locator).sendKeys(Keys.BACK_SPACE);
                wd.findElement(locator).sendKeys(text);
            }
        }
    }

    protected void waitForElementPresence(By locator) {
//        WebDriverWait wait = new WebDriverWait(wd, 20/*seconds*/);
        //wait.until((WebDriver d) -> d.findElement(By.name("q")));

        Wait wait = new FluentWait<WebDriver>(wd)
                .withTimeout(90, TimeUnit.SECONDS)
                .pollingEvery(3, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        wait.until(presenceOfElementLocated(locator));
    }

    protected void waitForElementVisibility(By locator) {
        //WebDriverWait wait = new WebDriverWait(wd, 10/*seconds*/);
        Wait wait = new FluentWait<WebDriver>(wd)
                .withTimeout(180, TimeUnit.SECONDS)
                .pollingEvery(3, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        wait.until(visibilityOfElementLocated(locator));
    }

    protected void waitForElementsVisibility(By locator) {
//        WebDriverWait wait = new WebDriverWait(wd, 60/*seconds*/);
        Wait wait = new FluentWait<WebDriver>(wd)
                .withTimeout(90, TimeUnit.SECONDS)
                .pollingEvery(3, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        wait.until(refreshed(visibilityOfAllElementsLocatedBy(locator)));
        wait.until(visibilityOfAllElementsLocatedBy(locator));
    }

    protected void waitForElementInVisibility(By locator) {
        //WebDriverWait wait = new WebDriverWait(wd, 10/*seconds*/);
        Wait wait = new FluentWait<WebDriver>(wd)
                .withTimeout(90, TimeUnit.SECONDS)
                .pollingEvery(3, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        wait.until(invisibilityOfElementLocated(locator));
    }

    protected boolean isElementPresent(By locator) {
        try {
            wd.findElement(locator);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected boolean areElementsPresent(By locator) {
        return wd.findElements(locator).size() > 0;
    }

    protected void attach(By locator, File file) {
        if (file != null) {
            wd.findElement(locator).sendKeys(file.getAbsolutePath());
        }
    }

    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum value
     * @param max Maximum value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     */
    public static int randInt(int min, int max) {
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
        return randomNum;
    }

    public static String randID() {
        // randomly generated UUID
        String randomID = UUID.randomUUID().toString();
        return randomID;
    }

    public void waitForPageListUpdate() {
        if (isElementPresent(By.cssSelector("div.real-estate-MuiTablePagination-selectRoot"))) {
            try {
                waitForElementVisibility(By.cssSelector("[data-testid=\"dti-table-header-row\"]"));
                if(wd.findElements(By.cssSelector("[data-testid=\"dti-table-header-row\"]")).isEmpty()){
                    WebDriverWait wait = new WebDriverWait(wd, 15/*seconds*/);
                    wait.until((WebDriver d) -> d.findElements(By.cssSelector("[data-testid=\"dti-table-header-row\"]")).get(0));
                    wd.findElements(By.cssSelector("[data-testid=\"dti-table-header-row\"]")).get(0).isDisplayed();
                }
            } catch (Exception e) {
                System.out.println("There are no locations in find locations table");
                e.printStackTrace();
            }
        }
    }

    public void scrollTotopOfPage() {
        ((JavascriptExecutor) wd)
                .executeScript("window.scrollTo(0, document.body.scrollTop)");
    }

    public void scrollToBottomOfPage() {
        ((JavascriptExecutor) wd)
                .executeScript("window.scrollTo(0,document.body.scrollHeight)");
    }

    public void refreshPage() {
        wd.navigate().refresh();
    }

    public void takeScreenshot() throws IOException {
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH_mm_ss");
        // Take a screenshot of the current page
        File screenshot = ((TakesScreenshot) wd).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("reports/webui/screenshot/screens" +
               dateTime.format(formatter) + ".png"));
    }
}

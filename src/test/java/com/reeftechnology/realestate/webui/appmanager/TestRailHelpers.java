package com.reeftechnology.realestate.webui.appmanager;

import com.codepine.api.testrail.TestRail;
import com.codepine.api.testrail.model.Project;
import com.codepine.api.testrail.model.Result;
import com.codepine.api.testrail.model.Run;
import com.codepine.api.testrail.model.Suite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestRailHelpers {

    private static TestRail testRail;
    private final static String endPoint = "https://reef.testrail.io/";
    private final static String username = "nodir.sultan@reeftechnology.com";
    private final static String password = "Testrail1!";
    public static int projectId;
    public static int suiteId;
    public static Run run;
    public static List<Integer> caseIds = new ArrayList<>();

    public static TestRail createTestRailInstance() {
        if (testRail == null) {
            testRail = TestRail.builder(endPoint, username, password).build();
        }
        return testRail;
    }

    // ********* Function to fetch Project id and suite Id ***********
    public static void setProjectSuiteId(String projectName, String suiteName) {
        try {
            TestRail.Projects projects = testRail.projects();
            List<Project> projectList = projects.list().execute();
            int pid = 0;
            int sid = 0;
            for (Project project : projectList) {
                if (project.getName().equals(projectName)) {
                    pid = project.getId();
                    projectId = pid;
                    System.out.println(pid);
                    break;
                }
            }
            if (pid != 0) {
                List<Suite> suiteList = testRail.suites().list(pid).execute();
                for (Suite s : suiteList) {
                    String sName = s.getName();
                    if (sName.equals(suiteName)) {
                        sid = s.getId();
                        suiteId = sid;
                        System.out.println(sid);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ***** Create Run Function *********
    public static void createRun(String automationType) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyy kk mm s");
        Date date = new Date();
        String dateString = format.format(date);
        String runName = "Automation type - " + automationType + " - test execution " + dateString;
        try {
            run = new Run();
            run = testRail.runs().add(projectId, run.setSuiteId((suiteId)).setName(runName).setIncludeAll(false)).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // **** This will add case id into current run
    public static void updateRun(Object caseIdString) {
        try {
            if (null != caseIdString) {
                Integer caseId = Integer.parseInt(caseIdString.toString());
                caseIds.add(caseId);
                run.setCaseIds(caseIds);
                testRail.runs().update(run).execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // **** below function help to add result for a test case
// with comment pass in function *********
    public static void addResult(String comment, Integer caseId) {
        try {
            if (null != testRail) {
                List customResultFields = testRail.resultFields().list().execute();
                testRail.results().addForCase(run.getId(), caseId, new Result().setComment(comment),
                        customResultFields).execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // **** Function use to add final status with comment for a test case ***
    public static void addStatusForCase(int statusId, Integer caseId) {
        String comment = null;
        try {
            List customResultFields = testRail.resultFields().list().execute();
            testRail.results().addForCase(run.getId(), caseId, new Result().setStatusId(statusId), customResultFields).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ***** Close the current run ********
    public static void closeRun() {
        try {
            testRail.runs().close(run.getId()).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

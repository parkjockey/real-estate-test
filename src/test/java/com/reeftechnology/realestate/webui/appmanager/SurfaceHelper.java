package com.reeftechnology.realestate.webui.appmanager;

import com.reeftechnology.realestate.webui.model.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SurfaceHelper extends StructureHelper {

    public SurfaceHelper(WebDriver driver) {
        super(driver);
    }

    public void initSurfaceCreationFromStructures() {
        click(By.cssSelector("[data-testid=\"dti-add-surface-btn\"]"));
        waitForElementPresence(By.cssSelector("[data-testid=\"dti-surface-form-surfaceType-select\"]"));
    }

    public void initAddSpaceCreationFromSurfaces() {
        wd.findElements(By.cssSelector("button.real-estate-MuiButton-root")).get(3).click();
    }

    public void initAddFeatureCreationFromSurfaces() {
        wd.findElements(By.cssSelector("button.real-estate-MuiButton-root")).get(5).click();
    }

    public void initAddAccessPointFromSurfaces() {
        wd.findElements(By.cssSelector("button.real-estate-MuiButton-root")).get(4).click();
    }

    public void fillSurfaceForm(SurfaceData surface) {
        click(By.id("mui-component-select-surfaceType"));
        click(By.cssSelector("li[data-value=\"" + surface.getType() + "\"]"));

        click(By.id("mui-component-select-surfaceMode"));
        click(By.cssSelector("li[data-value=\"" + surface.getMode() + "\"]"));

        type(By.cssSelector("input[name=\"description\"]"), surface.getDescription());
    }

    public void fillSpaceForm(SpaceData space) {
        click(By.id("mui-component-select-spaces[0].spaceType"));
        click(By.cssSelector("li[data-value=\"" + space.getType() + "\"]"));

        click(By.id("mui-component-select-spaces[0].spaceMode"));
        click(By.cssSelector("li[data-value=\"" + space.getMode() + "\"]"));

        type(By.name("spaces[0].quantity"), String.valueOf(space.getQuantity()));
    }

    public void fillFeatureForm(FeatureData featureData) {
        click(By.id("mui-component-select-features[0].featureType"));
        click(By.cssSelector("li[data-value=\"" + featureData.getType() + "\"]"));
        type(By.name("features[0].description"), String.valueOf(featureData.getDescription()));
    }

    public void submitSurfaceCreation() {
        click(By.cssSelector("[data-testid=\"dti-surface-form-submit-btn\"]"));
    }

    public void fillAccessPointForm(AccessPointData accessPointData) {
        click(By.id("mui-component-select-accessPoints[0].accessPointType"));
        click(By.cssSelector("li[data-value=\"" + accessPointData.getType() + "\"]"));
        type(By.name("accessPoints[0].description"), accessPointData.getDescription());
    }

    public void submitSpaceCreation() {
        initSurfaceCreationFromStructures();
        submitLocationCreation();
    }

    public void submitFeatureCreation() {
        initSurfaceCreationFromStructures();
        submitLocationCreation();
    }

    public void submitAccessPointCreation() {
        initSurfaceCreationFromStructures();
        submitLocationCreation();
    }

    public void cancelSurfaceCreation() {
        click(By.cssSelector("[testid=\"cancel-new-surface-button\"]"));
    }

    public void create(SurfaceData surface, boolean isSurfaceOnly) {
        initSurfaceCreationFromStructures();
        fillSurfaceForm(surface);
        if (isSurfaceOnly) {
            submitSurfaceCreation();
        } else {
        }
    }

    public void addSpace(SpaceData space) {
        wd.findElements(By.cssSelector("div.real-estate-MuiGrid-root h6")).get(1).getText().equals("Block of Spaces");
        initAddSpaceCreationFromSurfaces();
        fillSpaceForm(space);
        submitSpaceCreation();
    }

    public void addFeature(FeatureData featureData) {
        wd.findElements(By.cssSelector("div.real-estate-MuiGrid-root h6")).get(3).getText().equals("Features");
        initAddFeatureCreationFromSurfaces();
        fillFeatureForm(featureData);
        submitFeatureCreation();
    }

    public void addAccessPoint(AccessPointData accessPointData) {
        wd.findElements(By.cssSelector("div.real-estate-MuiGrid-root h6")).get(2).getText().equals("Access Points");
        initAddAccessPointFromSurfaces();
        fillAccessPointForm(accessPointData);
        submitAccessPointCreation();
    }

    public void edit() {
        click(By.cssSelector("button[aria-label=\"Edit\"]"));
    }

    public SurfaceData infoFromSurfaceForm() {
        String surfaceType = wd.findElements(By.cssSelector("p.MuiTypography-body2.MuiTypography-gutterBottom")).get(0).getText();
        String surfaceMode = wd.findElements(By.cssSelector("p.MuiTypography-body2.MuiTypography-gutterBottom")).get(1).getText();
        String description = wd.findElements(By.cssSelector("p.MuiTypography-body2.MuiTypography-gutterBottom")).get(2).getText();
        return new SurfaceData().withType(surfaceType.toUpperCase()).withMode(surfaceMode.toUpperCase())
                .withDescription(description);
    }

    public SpaceData infoOnSpaceFromSurfaceForm(boolean isSpaceCreated, boolean isSpaceModified) {
        if (isSpaceCreated) {
            edit();
        }
        String spaceType = wd.findElement(By.name("spaces[0].spaceType")).getAttribute("value");
        String spaceMode = wd.findElement(By.name("spaces[0].spaceMode")).getAttribute("value");
        String spaceQuantity = wd.findElement(By.name("spaces[0].quantity")).getAttribute("value");

        if (isSpaceModified) {
            submitSurfaceCreation();
        } else {
            wd.findElements(By.cssSelector("button.real-estate-MuiButton-disableElevation")).get(6).click();
            //submitLocationCreation();
        }

        return new SpaceData().withType(spaceType.toUpperCase()).withMode(spaceMode.toUpperCase())
                .withQuantity(Integer.parseInt(spaceQuantity));
    }

    public FeatureData infoOnFeatureFromSurfaceForm(boolean isFeatureCreated, boolean isFeaturetModified) {
        if (isFeatureCreated) {
            edit();
        }
        String featureType = wd.findElement(By.name("features[0].featureType")).getAttribute("value");
        String featureDescription = wd.findElement(By.name("features[0].description")).getAttribute("value");
        if (isFeaturetModified) {
            submitSurfaceCreation();
        } else {
            wd.findElements(By.cssSelector("button.real-estate-MuiButton-disableElevation")).get(6).click();
        }
        return new FeatureData().withType(featureType).withDescription(featureDescription);
    }

    public AccessPointData infoOnAccessPointFromSurfaceForm(boolean isAccessPointCreated, boolean isAccessPontModified) {
        if (isAccessPointCreated) {
            edit();
        }
        String accessPointType = wd.findElement(By.name("accessPoints[0].accessPointType")).getAttribute("value");
        String accessPointDescription = wd.findElement(By.name("accessPoints[0].description")).getAttribute("value");
        if (isAccessPontModified) {
            submitSurfaceCreation();
        } else {
            wd.findElements(By.cssSelector("button.real-estate-MuiButton-disableElevation")).get(6).click();
        }
        return new AccessPointData().withType(accessPointType).withDescription(accessPointDescription);
    }

    public void modify(SurfaceData modifedSurface) {
        click(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
        //click on edit surfaces button
        click(By.cssSelector("[data-testid=\"surfaces-details\"] button[aria-label=\"Edit\"]"));
        fillSurfaceForm(modifedSurface);
        submitSurfaceCreation();
    }

    public void modifyAccessPoint(AccessPointData modifiedAccessPointData) {
        click(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
        //click on edit surfaces button
        click(By.cssSelector("[data-testid=\"surfaces-details\"] button[aria-label=\"Edit\"]"));
        fillAccessPointForm(modifiedAccessPointData);
        submitAccessPointCreation();
    }

    public void modifyFeature(FeatureData modifiedFeatureData) {
        click(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
        //click on edit surfaces button
        click(By.cssSelector("[data-testid=\"surfaces-details\"] button[aria-label=\"Edit\"]"));
        fillFeatureForm(modifiedFeatureData);
        submitFeatureCreation();
    }

    public void modifySpace(SpaceData modifiedSpaceData) {
        click(By.cssSelector("[data-testid=\"dti-form-tab-surfaces\"]"));
        //click on edit surfaces button
        click(By.cssSelector("[data-testid=\"surfaces-details\"] button[aria-label=\"Edit\"]"));
        fillSpaceForm(modifiedSpaceData);
        submitSpaceCreation();
    }

    public void modifyLocationSurfaceAccessPoint(LocationData modifiedLocation, AccessPointData modifiedAccessPointData) {
        selectLocationById(modifiedLocation.getId());
        SurfaceHelper surfaceHelper = new SurfaceHelper(wd);
        surfaceHelper.modifyAccessPoint(modifiedAccessPointData);
    }

    public void modifyLocationSurfaceFeature(LocationData modifiedLocation, FeatureData modifiedFeatureData) {
        selectLocationById(modifiedLocation.getId());
        SurfaceHelper surfaceHelper = new SurfaceHelper(wd);
        surfaceHelper.modifyFeature(modifiedFeatureData);
    }

    public void modifyLocationSurfaceSpace(LocationData modifiedLocation, SpaceData modifiedSpaceData) {
        selectLocationById(modifiedLocation.getId());
        SurfaceHelper surfaceHelper = new SurfaceHelper(wd);
        surfaceHelper.modifySpace(modifiedSpaceData);
    }
}

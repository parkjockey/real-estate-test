package com.reeftechnology.realestate.webui.appmanager;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ApplicationManager {
    public final Properties properties;
    WebDriver wd;
    JavascriptExecutor js;

    private NavigationHelper navigationHelper;
    private LocationHelper locationHelper;
    private SessionHelper sessionHelper;
    private SurfaceHelper surfaceHelper;
    private ServicesHelper servicesHelper;
    private StructureHelper structureHelper;
    private String browser;
    public String target;
    private String remote;

    public ApplicationManager(String browser) {
        this.browser = browser;
        properties = new Properties();
    }

    public WebDriver getWd() {
        return wd;
    }

    public Properties getProperties() {
        return properties;
    }

    public void init() throws IOException {
        target = System.getProperty("target", "dev");
        properties.load(new FileReader(new File(String.format("src/test/resources/config/%s.properties", target))));
        remote = System.getProperty("remote", "false");

        if (remote.equals("false")) {
            if (browser.equals(BrowserType.FIREFOX)) {
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.setCapability("webdriverClick", false);
                wd = new FirefoxDriver(firefoxOptions);
            /*MAC OS - Firefox geckodriver will not work on Catalina if you manually
            download it through another notarized program - % xattr -r -d com.apple.quarantine geckodriver*/
            } else if (browser.equals(BrowserType.CHROME)) {
                ChromeOptions options = new ChromeOptions();
                //options.addArguments("--headless", "--window-size=1920,1200", "--ignore-certificate-errors");
                wd = new ChromeDriver(options);
            } else if (browser.equals(BrowserType.SAFARI)) {
                wd = new SafariDriver();
            } else if (browser.equals(BrowserType.EDGE)) {
                wd = new EdgeDriver();
            } else if (browser.equals(BrowserType.IE)) {
                wd = new InternetExplorerDriver();
            }
        } else {
            String remoteUrlPropertyName = (System.getProperty("remoteEndpoint").equalsIgnoreCase("internal")) ? "selenium.server.internal" : "selenium.server.external";
            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            desiredCapabilities.setBrowserName(browser);
            desiredCapabilities.setPlatform(Platform.fromString(System.getProperty("platform", "LINUX")));//"LINUX")));
            wd = new RemoteWebDriver(new URL(properties.getProperty(remoteUrlPropertyName)), desiredCapabilities);
        }

        js = (JavascriptExecutor) wd;
        wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wd.manage().window().maximize();

        locationHelper = new LocationHelper(wd);
        navigationHelper = new NavigationHelper(wd, properties);
        surfaceHelper = new SurfaceHelper(wd);
        sessionHelper = new SessionHelper(wd);
        servicesHelper = new ServicesHelper(wd);
        structureHelper = new StructureHelper(wd);
        wd.get(properties.getProperty("web.BaseUrl"));

        if ("https://dashboard-dev2.co.reefplatform.com".equals(properties.getProperty("web.BaseUrl"))) {
            sessionHelper.login(properties.getProperty("web.OwnerLogin"), properties.getProperty("web.OwnerPassword"));
        } else if ("https://dashboard-qa2.co.reefplatform.com/".equals(properties.getProperty("web.BaseUrl"))) {
            sessionHelper.login(properties.getProperty("web.OwnerLogin"), properties.getProperty("web.OwnerPassword"));
        } else if ("https://dashboard-stage2.co.reefplatform.com/".equals(properties.getProperty("web.BaseUrl"))) {
            sessionHelper.loginWithSSO(properties.getProperty("web.OwnerLogin"), properties.getProperty("web.OwnerPassword"));
        } else {
            System.out.println("Application URL is not provided");
            return;
        }
    }

    public void stop() {
        wd.quit();
    }

    public LocationHelper location() {
        return locationHelper;
    }

    public NavigationHelper goTo() {
        return navigationHelper;
    }

    public SurfaceHelper surface() {
        return surfaceHelper;
    }

    public StructureHelper structure() {
        return structureHelper;
    }

    public ServicesHelper services() {
        return servicesHelper;
    }

    public SessionHelper session() {
        return sessionHelper;
    }
}

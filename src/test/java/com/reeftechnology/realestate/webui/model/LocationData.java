package com.reeftechnology.realestate.webui.model;

import com.google.gson.annotations.Expose;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import java.time.LocalDate;

@XStreamAlias("Location")
public class LocationData {
    @XStreamOmitField //excludes field from serialization/deserialization
    private int id = 0;
    private String campusName;

    public String getRegion() {
        return region;
    }

    public String getCampusName() {
        return campusName;
    }

    private String region;

    public String getFullID() {
        return fullID;
    }

    public LocationData withFullID(String fullID) {
        this.fullID = fullID;
        return this;
    }

    @Expose
    private String fullID;
    @Expose //includes field for serialization/deserialization
    private String locationName;
    @Expose
    private String locationType;
    @Expose
    private String locationSubType;
    @Expose
    private String locationMode;
    @Expose
    private String locationStatus;
    @Expose
    private String addressLine1;
    @Expose
    private String addressLine2;
    @Expose
    private String city;
    @Expose
    private String zipCode;
    @Expose
    private String country;
    @Expose
    private String state;
    @Expose
    private String propertyName;
    @Expose
    private String allAddresses;
    @Expose
    private String externalIdType;
    @Expose
    private String externalIdValue;
    @Expose
    private double latitude;
    @Expose
    private double longitude;
    @Expose
    private String propertyType;
    @Expose
    private String operatingCompany;

    public String getPropertyType() {
        return propertyType;
    }

    public LocationData withPropertyType(String propertyType) {
        this.propertyType = propertyType;
        return this;
    }

    public String getOperatingCompany() {
        return operatingCompany;
    }

    public LocationData withOperatingCompany(String operatingCompany) {
        this.operatingCompany = operatingCompany;
        return this;
    }

    public int getId() {
        return id;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public String getCity() {
        return city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getAllAddresses() {
        return allAddresses;
    }

    public String getLocationType() {
        return locationType;
    }

    public String getLocationMode() {
        return locationMode;
    }

    public String getLocationStatus() {
        return locationStatus;
    }

    public String getLocationSubType() {
        return locationSubType;
    }

    public String getExternalIdType() {
        return externalIdType;
    }

    public String getExternalIdValue() {
        return externalIdValue;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public LocationData withLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public LocationData withLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public LocationData withId(int maxRealEstateId) {
        this.id = maxRealEstateId;
        return this;
    }

    public LocationData withExternalIdValue(String externalIdValue) {
        this.externalIdValue = externalIdValue;
        return this;
    }

    public LocationData withExternalIdType(String externalIdType) {
        this.externalIdType = externalIdType;
        return this;
    }

    public LocationData withLocationStatus(String locationStatus) {
        this.locationStatus = locationStatus;
        return this;
    }

    public LocationData withLocationMode(String locationMode) {
        this.locationMode = locationMode;
        return this;
    }

    public LocationData withLocationType(String locationType) {
        this.locationType = locationType;
        return this;
    }

    public LocationData withLocationSubType(String locationSubType) {
        this.locationSubType = locationSubType;
        return this;
    }


    public LocationData withLocationName(String locationName) {
        this.locationName = locationName;
        return this;
    }

    public LocationData withAddressLine1(String address1) {
        this.addressLine1 = address1;
        return this;
    }

    public LocationData withAddressLine2(String address2) {
        this.addressLine2 = address2;
        return this;
    }

    public LocationData withAllAddresses(String allAddresses) {
        this.allAddresses = allAddresses;
        return this;
    }

    public LocationData withCity(String city) {
        this.city = city;
        return this;
    }

    public LocationData withZipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public LocationData withCountry(String country) {
        this.country = country;
        return this;
    }

    public LocationData withState(String state) {
        this.state = state;
        return this;
    }

    public LocationData withPropertyName(String propertyName) {
        this.propertyName = propertyName;
        return this;
    }

    @Override
    public String toString() {
        return "LocationData{" +
                "id=" + id +
                ", fullID='" + fullID + '\'' +
                ", locationName='" + locationName + '\'' +
                ", locationType='" + locationType + '\'' +
                ", locationSubType='" + locationSubType + '\'' +
                ", locationMode='" + locationMode + '\'' +
                ", locationStatus='" + locationStatus + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", city='" + city + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", propertyName='" + propertyName + '\'' +
                ", allAddresses='" + allAddresses + '\'' +
                ", externalIdType='" + externalIdType + '\'' +
                ", externalIdValue='" + externalIdValue + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", propertyType='" + propertyType + '\'' +
                ", operatingCompany='" + operatingCompany + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocationData that = (LocationData) o;

        if (id != that.id) return false;
        if (Double.compare(that.latitude, latitude) != 0) return false;
        if (Double.compare(that.longitude, longitude) != 0) return false;
        if (fullID != null ? !fullID.equals(that.fullID) : that.fullID != null) return false;
        if (locationName != null ? !locationName.equals(that.locationName) : that.locationName != null) return false;
        if (locationType != null ? !locationType.equals(that.locationType) : that.locationType != null) return false;
        if (locationSubType != null ? !locationSubType.equals(that.locationSubType) : that.locationSubType != null)
            return false;
        if (locationMode != null ? !locationMode.equals(that.locationMode) : that.locationMode != null) return false;
        if (locationStatus != null ? !locationStatus.equals(that.locationStatus) : that.locationStatus != null)
            return false;
        if (addressLine1 != null ? !addressLine1.equals(that.addressLine1) : that.addressLine1 != null) return false;
        if (addressLine2 != null ? !addressLine2.equals(that.addressLine2) : that.addressLine2 != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) return false;
        if (allAddresses != null ? !allAddresses.equals(that.allAddresses) : that.allAddresses != null) return false;
        if (externalIdType != null ? !externalIdType.equals(that.externalIdType) : that.externalIdType != null)
            return false;
        if (externalIdValue != null ? !externalIdValue.equals(that.externalIdValue) : that.externalIdValue != null)
            return false;
        if (propertyType != null ? !propertyType.equals(that.propertyType) : that.propertyType != null) return false;
        return operatingCompany != null ? operatingCompany.equals(that.operatingCompany) : that.operatingCompany == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (fullID != null ? fullID.hashCode() : 0);
        result = 31 * result + (locationName != null ? locationName.hashCode() : 0);
        result = 31 * result + (locationType != null ? locationType.hashCode() : 0);
        result = 31 * result + (locationSubType != null ? locationSubType.hashCode() : 0);
        result = 31 * result + (locationMode != null ? locationMode.hashCode() : 0);
        result = 31 * result + (locationStatus != null ? locationStatus.hashCode() : 0);
        result = 31 * result + (addressLine1 != null ? addressLine1.hashCode() : 0);
        result = 31 * result + (addressLine2 != null ? addressLine2.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (propertyName != null ? propertyName.hashCode() : 0);
        result = 31 * result + (allAddresses != null ? allAddresses.hashCode() : 0);
        result = 31 * result + (externalIdType != null ? externalIdType.hashCode() : 0);
        result = 31 * result + (externalIdValue != null ? externalIdValue.hashCode() : 0);
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (propertyType != null ? propertyType.hashCode() : 0);
        result = 31 * result + (operatingCompany != null ? operatingCompany.hashCode() : 0);
        return result;
    }

    public LocationData withRegion(String region) {
        this.region = region;
        return this;
    }

    public LocationData withCampusName(String campusName) {
        this.campusName = campusName;
        return this;
    }
}

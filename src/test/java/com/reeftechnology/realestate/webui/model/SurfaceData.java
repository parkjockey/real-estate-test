package com.reeftechnology.realestate.webui.model;

import java.io.File;

public class SurfaceData {
    private int id = Integer.MAX_VALUE;
    private String type;
    private String mode;
    private String description;
    private double latitude;
    private double longitude;
    private File photoLinkToGeocoordinates;

    public int getId() {
        return id;
    }

    public SurfaceData withId(int id) {
        this.id = id;
        return this;
    }

    public String getType() {
        return type;
    }

    public SurfaceData withType(String type) {
        this.type = type;
        return this;
    }

    public String getMode() {
        return mode;
    }

    public SurfaceData withMode(String mode) {
        this.mode = mode;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public SurfaceData withDescription(String description) {
        this.description = description;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    private void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    private void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public SurfaceData withGeoCoordinates(double latitiude, double longitude) {
        setLatitude(latitiude);
        setLongitude(longitude);
        return this;
    }

    public File getPhotoLinkToGeoCoordinates() {
        return photoLinkToGeocoordinates;
    }

    public SurfaceData withPhotoLinkToGeoCoordinates(File photoLinkToGeocoordinates) {
        this.photoLinkToGeocoordinates = photoLinkToGeocoordinates;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SurfaceData that = (SurfaceData) o;

        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (mode != null ? !mode.equals(that.mode) : that.mode != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (mode != null ? mode.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SurfaceData{" +
                "type='" + type + '\'' +
                ", mode='" + mode + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}

package com.reeftechnology.realestate.webui.model;

public class AccessPointData {
    private String type;
    private String description;

    public String getType() {
        return type;
    }

    public AccessPointData withType(String type) {
        this.type = type;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public AccessPointData withDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public String toString() {
        return "AccessPointData{" +
                "type='" + type + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessPointData that = (AccessPointData) o;

        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}

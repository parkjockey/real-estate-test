package com.reeftechnology.realestate.webui.model;

public class ServicesData {
    private String type;
    private String subtype;
    private String serviceID;
    private String description;

    public String getType() {
        return type;
    }

    public ServicesData withType(String type) {
        this.type = type;
        return this;
    }

    public String getSubtype() {
        return subtype;
    }

    public ServicesData withSubtype(String subtype) {
        this.subtype = subtype;
        return this;
    }

    public String getServiceID() {
        return serviceID;
    }

    public ServicesData withServiceID(String serviceID) {
        this.serviceID = serviceID;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ServicesData withDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public String toString() {
        return "ServicesData{" +
                "type='" + type + '\'' +
                ", subtype='" + subtype + '\'' +
                ", serviceID='" + serviceID + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServicesData that = (ServicesData) o;

        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (subtype != null ? !subtype.equals(that.subtype) : that.subtype != null) return false;
        if (serviceID != null ? !serviceID.equals(that.serviceID) : that.serviceID != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (subtype != null ? subtype.hashCode() : 0);
        result = 31 * result + (serviceID != null ? serviceID.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}

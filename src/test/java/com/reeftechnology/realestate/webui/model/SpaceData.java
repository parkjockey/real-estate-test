package com.reeftechnology.realestate.webui.model;

public class SpaceData {
    private String type;
    private String mode;
    private int quantity;

    public String getType() {
        return type;
    }

    public SpaceData withType(String type) {
        this.type = type;
        return this;
    }

    public String getMode() {
        return mode;
    }

    public SpaceData withMode(String mode) {
        this.mode = mode;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public SpaceData withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    @Override
    public String toString() {
        return "SpaceData{" +
                "type='" + type + '\'' +
                ", mode='" + mode + '\'' +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpaceData spaceData = (SpaceData) o;

        if (quantity != spaceData.quantity) return false;
        if (type != null ? !type.equals(spaceData.type) : spaceData.type != null) return false;
        return mode != null ? mode.equals(spaceData.mode) : spaceData.mode == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (mode != null ? mode.hashCode() : 0);
        result = 31 * result + (int) (quantity ^ (quantity >>> 32));
        return result;
    }
}

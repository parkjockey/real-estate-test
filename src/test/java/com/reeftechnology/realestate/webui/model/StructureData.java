package com.reeftechnology.realestate.webui.model;

import com.google.gson.annotations.Expose;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("Structure")
public class StructureData {
    @XStreamOmitField //excludes field from serialization/deserialization
    private int id = Integer.MAX_VALUE;
    @Expose //includes field for serialization/deserialization
    private String structureName;
    @Expose
    private String structureType;
    @Expose
    private String structureSubType;
    @Expose
    private String structureMode;
    @Expose
    private String structureEquipmentType;
    @Expose
    private String description;

    public int getId() {
        return id;
    }


    public String getStructureName() {
        return structureName;
    }

    public String getDescription() {
        return description;
    }

    public String getStructureType() {
        return structureType;
    }

    public String getStructureSubType() {
        return structureSubType;
    }

    public String getStructureMode() {
        return structureMode;
    }

    public String getStructureEquipmentType() {
        return structureEquipmentType;
    }


    public StructureData withStructureEquipmentType(String structureEquipmentType) {
        this.structureEquipmentType = structureEquipmentType;
        return this;
    }

    public StructureData withStructureMode(String structureMode) {
        this.structureMode = structureMode;
        return this;
    }

    public StructureData withStructureType(String structureType) {
        this.structureType = structureType;
        return this;
    }

    public StructureData withStructureSubType(String structureSubType) {
        this.structureSubType = structureSubType;
        return this;
    }


    public StructureData withStructureName(String structureName) {
        this.structureName = structureName;
        return this;
    }

    public StructureData withDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public String toString() {
        return "StructureData{" +
                "id=" + id +
                ", structureName='" + structureName + '\'' +
                ", structureType='" + structureType + '\'' +
                ", structureSubType='" + structureSubType + '\'' +
                ", structureMode='" + structureMode + '\'' +
                ", structureEquipmentType='" + structureEquipmentType + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StructureData that = (StructureData) o;

        if (id != that.id) return false;
        if (structureName != null ? !structureName.equals(that.structureName) : that.structureName != null)
            return false;
        if (structureType != null ? !structureType.equals(that.structureType) : that.structureType != null)
            return false;
        if (structureSubType != null ? !structureSubType.equals(that.structureSubType) : that.structureSubType != null)
            return false;
        if (structureMode != null ? !structureMode.equals(that.structureMode) : that.structureMode != null)
            return false;
        if (structureEquipmentType != null ? !structureEquipmentType.equals(that.structureEquipmentType) : that.structureEquipmentType != null)
            return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (structureName != null ? structureName.hashCode() : 0);
        result = 31 * result + (structureType != null ? structureType.hashCode() : 0);
        result = 31 * result + (structureSubType != null ? structureSubType.hashCode() : 0);
        result = 31 * result + (structureMode != null ? structureMode.hashCode() : 0);
        result = 31 * result + (structureEquipmentType != null ? structureEquipmentType.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}

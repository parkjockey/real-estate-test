package com.reeftechnology.realestate.webui.model;

import com.google.common.collect.ForwardingSet;

import java.util.HashSet;
import java.util.Set;

public class Locations extends ForwardingSet<LocationData> {

    private Set<LocationData> delegate;

    public Locations(Locations locations) {
        this.delegate = new HashSet<LocationData>(locations.delegate);
    }

    public Locations() {
        this.delegate = new HashSet<LocationData>();
    }

    @Override
    protected Set<LocationData> delegate() {
        return delegate;
    }

    public Locations withAdded(LocationData location){
        Locations locations = new Locations(this);
        locations.add(location);
        return locations;
    }

    public Locations withOut(LocationData location){
        Locations locations = new Locations(this);
        locations.remove(location);
        return locations;
    }

}

package com.reeftechnology.realestate.webui.generators;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.thoughtworks.xstream.XStream;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

//arguments - number of records, path to folder from repository root/locations.file and file format xml/csv
public class LocationDataGenerator {
    @Parameter(names = "-c", description = "Location records count")
    public int count;

    @Parameter(names = "-f", description = "Path to folder from repository root/Target file")
    public String file;

    @Parameter(names = "-d", description = "Target data file format")
    public String format;

    public static void main(String[] args) throws IOException {
        LocationDataGenerator generator = new LocationDataGenerator();
        JCommander jCommander = new JCommander(generator);
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            jCommander.usage();
            return;
        }
        generator.run();
    }

    private void run() throws IOException {
        List<LocationData> locations = generateLocations(count);
        if (format.equals("csv")) {
            saveAsCsv(new File(file), locations);
        } else if (format.equals("xml")) {
            saveAsXml(new File(file), locations);
        } else if (format.equals("json")) {
            saveAsJson(new File(file), locations);
        } else {
            System.out.println("Unrecognized file format:" + format);
        }
    }

    private void saveAsJson(File file, List<LocationData> locations) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting()
                .excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(locations);
        try (Writer writer = new FileWriter(file)) {
            writer.write(json);
        }
    }

    private void saveAsXml(File file, List<LocationData> locations) throws IOException {
        XStream xstream = new XStream();
        xstream.processAnnotations(LocationData.class);
        String xml = xstream.toXML(locations);
        try (Writer writer = new FileWriter(file)) {
            writer.write(xml);
        }
    }

    private static void saveAsCsv(File file, List<LocationData> locations) throws IOException {
        System.out.println(new File(".").getAbsolutePath());
        try (Writer writer = new FileWriter(file)) {
            for (LocationData locationData : locations) {
                writer.write(String.format("%s;%s;%s;%s;%s;%s;%s;%s\n",
                        locationData.getLocationName(),
                        locationData.getPropertyName(),
                        locationData.getAddressLine1(),
                        locationData.getAddressLine2(),
                        locationData.getCity(), locationData.getZipCode(),
                        locationData.getCountry(), locationData.getState()));
            }
        }

    }

    private static List<LocationData> generateLocations(int count) {
        List<LocationData> locations = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            locations.add(new LocationData()
                    .withLocationName(String.format("Test %s SampleLocationName", i))
                    .withPropertyName(String.format("Test %s SamplePropertyName", i))
                    .withAddressLine1(String.format("%s Sample Street", i))
                    .withAddressLine2(String.format("Apartment 10%s", i))
                    .withCity("Miami").withZipCode("33131")
                    .withCountry("US").withState("FL"));
        }
        return locations;
    }
}

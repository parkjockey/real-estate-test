package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.AccessPointData;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.SurfaceData;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddAccessPointTests extends TestBase {

    LocationData createdLocation;
    SurfaceData existingSurface;

    @DataProvider
    public Iterator<Object[]> validAccessPointDataFromCsv() throws IOException {
        List<Object[]> accessPointData = new ArrayList<Object[]>();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/accessPointsToSurface.csv")))) {
            String line = reader.readLine();
            while (line != null) {
                String[] split = line.split(";");
                accessPointData.add(new Object[]{new AccessPointData().withType(split[0])
                        .withDescription(split[1])});
                line = reader.readLine();
            }
            return accessPointData.iterator();
        }
    }

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        createdLocation = new LocationData()
                .withLocationName("SurfaceAccessPoint-Location-Name" + HelperBase.randInt(1, 100))
                .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Suite 1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");
        app.location().createLocationAddSurface(createdLocation);
        existingSurface = new SurfaceData()
                .withType("ASPHALT")
                .withMode("FLAT")
                .withDescription("Sample Access Point to Surface ASPHALT>FLAT");
        app.surface().create(existingSurface, false);
    }

    @Test
    public void testAddAccessPointEntranceExitToSurface() throws IOException {
        testrailID = 9738;

        AccessPointData createdAccessPointData = new AccessPointData()
                .withType("ENTRANCE_EXIT")
                .withDescription("ENTRANCE_EXIT access point description");
        app.surface().addAccessPoint(createdAccessPointData);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());

        //validate created surface>access point data
        LocationData newLocationFromList = app.location().all().iterator().next();
        AccessPointData accessPointData = app.location().getLocationSurfaceAccessPointInfo(newLocationFromList.getId());

        assertThat(accessPointData, equalTo(createdAccessPointData));
    }

    @Test(dataProvider = "validAccessPointDataFromCsv", enabled = false)
    public void testAddAccessPointsToSurface(AccessPointData createdAccessPointData) throws IOException {
        app.surface().addAccessPoint(createdAccessPointData);

        //validate access point data for surface
        AccessPointData accessPointDataFromSurfaceCreate = app.surface()
                .infoOnAccessPointFromSurfaceForm(true, true);
        Assert.assertEquals(accessPointDataFromSurfaceCreate, createdAccessPointData);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());

        //validate modified surface>access point data
        LocationData newLocationFromList = app.location().all().iterator().next();
        AccessPointData accessPointData = app.location().getLocationSurfaceAccessPointInfo(newLocationFromList.getId());

        assertThat(accessPointData, equalTo(createdAccessPointData));
    }

}



package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.Locations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class DeleteLocationTests extends TestBase {

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        if (app.location().all().size() == 0) {
            app.location().create(new LocationData()
                    .withLocationName("*SampleLocationName" + Integer.MAX_VALUE)
                    .withPropertyName("Sample property name")
                    .withAddressLine1("0123 Sample Street").withAddressLine2("Apartment 100")
                    .withCity("Miami").withZipCode("33131")
                    .withCountry("US").withState("FL"));
        }
    }

    @Test(enabled = false)
    public void testLocationDeletion() {
        Locations before = app.location().all();
        LocationData deletedLocation = before.iterator().next();
        app.location().delete(deletedLocation);
        Assert.assertEquals(app.location().count(), before.size() - 1);
        Locations after = app.location().all();

        Assert.assertEquals(before, after);
        assertThat(after, equalTo(before.withOut(deletedLocation)));
    }

}

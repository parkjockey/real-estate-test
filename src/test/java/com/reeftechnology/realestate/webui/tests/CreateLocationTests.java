package com.reeftechnology.realestate.webui.tests;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.Identifiers;
import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.Locations;
import com.thoughtworks.xstream.XStream;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.ContentType;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class CreateLocationTests extends TestBase {

    private String realEstateID;

    @DataProvider
    public Iterator<Object[]> validLocationsFromCsv() throws IOException {
        List<Object[]> locations = new ArrayList<Object[]>();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations.csv")))) {
            String line = reader.readLine();
            while (line != null) {
                String[] split = line.split(";");
                locations.add(new Object[]{new LocationData().withLocationName(split[0])
                        .withPropertyName(split[1])
                        .withAddressLine1(split[2])
                        .withAddressLine2(split[3])
                        .withCity(split[4]).withZipCode(split[5])
                        .withCountry(split[6]).withState(split[7])});
                line = reader.readLine();
            }
            return locations.iterator();
        }
    }

    @DataProvider
    public Iterator<Object[]> validLocationsFromXml() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations.xml")))) {

            String xml = "";
            String line = reader.readLine();
            while (line != null) {
                xml += line;
                line = reader.readLine();
            }
            XStream xstream = new XStream();
            List<LocationData> locations = (List<LocationData>) xstream.fromXML(xml);
            return locations.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    @DataProvider
    public Iterator<Object[]> validLocationsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations-web.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<LocationData> locations = gson.fromJson(json,
                    new TypeToken<List<LocationData>>() {
                    }.getType()); //List <LocationData>.class
            return locations.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    @Test(priority = 1)
    public void testInvalidAddressLocationCreation() throws IOException {
        testrailID = 8631;
        app.goTo().locationsPage();
        LocationData locationData = new LocationData()
                .withLocationName("Test Invalid location" + HelperBase.randInt(1, 100))
                .withAddressLine1("`").withAddressLine2("`")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL");
        app.location().searchForLocation(locationData.getLocationName());
        Locations before = app.location().all();

        app.goTo().locationsPage();
        app.location().initLocationCreation();
        app.location().fillAddLocationForm(locationData);
        app.location().initLocationCreationFromDialogueForm();
        assertThat(app.location().isCreateLocationValidationMessageVisible(), equalTo(true));
        //assertThat(app.location().isCreateLocationDisabled(), equalTo(true));

        app.location().refreshPage();
        app.goTo().locationsPage();
        app.location().searchForLocation(locationData.getLocationName());
        Locations after = app.location().all();
        assertThat(after.size(), equalTo(before.size()));
        assertThat(after, equalTo(before));
    }

    @Test(priority = 1)
    public void testInvalidNameLocationCreation() throws IOException {
        testrailID = 8630;
        app.goTo().locationsPage();
        LocationData locationData = new LocationData()
                .withLocationName("[] = < > ? : { } | _")
                .withAddressLine1("601 Brickell Key Dr #1000").withAddressLine2("1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL");
        app.location().searchForLocation(locationData.getLocationName());
        Locations before = app.location().all();

        app.goTo().locationsPage();
        app.location().initLocationCreation();
        app.location().fillAddLocationForm(locationData);
        app.location().initLocationCreationFromDialogueForm();
        assertThat(app.location().isCreateLocationValidationMessageVisible(), equalTo(true));
        //assertThat(app.location().isCreateLocationDisabled(), equalTo(true));

        app.location().refreshPage();
        app.goTo().locationsPage();
        app.location().searchForLocation(locationData.getLocationName());
        Locations after = app.location().all();
        assertThat(after.size(), equalTo(before.size()));
        assertThat(after, equalTo(before));
    }

    @Test(dataProvider = "validLocationsFromJson")
    public void testLocationsCreationWithValidatedAddress(LocationData createdLocation) throws IOException {
        testrailID = 11766;
        app.goTo().locationsPage();
        createdLocation.withLocationName(createdLocation.getLocationName() + HelperBase.randInt(1, 999))
                .withRegion("")
                .withCampusName("");
        app.location().create(createdLocation);
        app.goTo().locationsPage();

        //Search in list for created location by exact location name
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData location = app.location().all().iterator().next();
        assertThat(location.getLocationName(), equalTo(createdLocation.getLocationName()));
    }

    @Test(priority = 2)
    public void testLocationCreationCanada() throws IOException {
        testrailID = 6860;
        app.goTo().locationsPage();
        LocationData createdLocation = new LocationData()
                .withLocationName("Canada Waterfront Centre" + HelperBase.randInt(1, 100))
                .withAddressLine1("200 Burrard St")
                .withAddressLine2("Parking")
                .withCity("Vancouver").withZipCode("V6C 3L6")
                .withCountry("CA").withState("BC");
        app.location().createDraftLocation(createdLocation);
        app.goTo().locationsPage();

        //Search in list for created location by exact location name
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData location = app.location().all().iterator().next();
        assertThat(location.getLocationName(), equalTo(createdLocation.getLocationName()));
    }

    @Test(priority = 2)
    public void testLocationCreationUS() throws IOException {
        testrailID = 6855;
        app.goTo().locationsPage();
        LocationData createdLocation = new LocationData()
                .withLocationName("US 250 Hospital Pkwy" + HelperBase.randInt(1, 100))
                .withAddressLine1("250 Hospital Pkwy").withAddressLine2("Kaiser Parking")
                .withCity("San Jose").withZipCode("95119")
                .withCountry("US").withState("CA");
        app.location().createDraftLocation(createdLocation);
        app.goTo().locationsPage();

        //Search in list for created location by exact location name
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData location = app.location().all().iterator().next();
        assertThat(location.getLocationName(), equalTo(createdLocation.getLocationName()));
    }

    @Test(enabled = false)
    public void testLocationsCreationWithGeoCoord() throws IOException {
        app.goTo().locationsPage();
        LocationData createdLocation = new LocationData()
                .withLocationName("*ABC*SampleLocationName" + Integer.MAX_VALUE)
                .withPropertyName("Sample property name")
                .withAddressLine1("0123 Sample Street").withAddressLine2("Apartment 100")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL");
        app.location().searchForLocation(createdLocation.getLocationName());
        Locations before = app.location().all();
        app.location().create(createdLocation);
        app.location().searchForLocation(createdLocation.getLocationName());
        assertThat(app.location().count(), equalTo(before.size()));
        Locations after = app.location().all();
        Assert.assertEquals(before, after);
        assertThat(after, equalTo(
                before.withAdded(createdLocation.withId(after.stream().mapToInt((g) -> g.getId()).max().getAsInt()))));

        //Search in list for created location by exact location name
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData location = app.location().all().iterator().next();
        assertThat(location.getLocationName(), equalTo(createdLocation.getLocationName()));
    }

    @Test(dataProvider = "validLocationsFromJson")
    public void testLocationsCreationWithManualID(LocationData createdLocation) throws IOException {
        Identifiers newIdentifierData = new Identifiers()
                .withCountry(createdLocation.getCountry())
                .withState(createdLocation.getState());
        JsonElement generatedIDData = createNewIdentifier(newIdentifierData);
        String realEstateID = generatedIDData.getAsJsonObject().get("id").getAsString();

        testrailID = 11766;
        app.goTo().locationsPage();
        createdLocation.withLocationName(createdLocation.getLocationName() + HelperBase.randInt(1, 999))
                .withFullID(realEstateID)
                .withRegion("")
                .withCampusName("");
        app.location().create(createdLocation);
        app.goTo().locationsPage();

        //Search in list for created location by exact location name
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData location = app.location().all().iterator().next();
        assertThat(location.getLocationName(), equalTo(createdLocation.getLocationName()));
    }

    private JsonElement createNewIdentifier(Identifiers newIdentifierData) throws IOException {
        String json = null;
        try {
            json = Executor.newInstance().auth("", "")
                    .execute(Request
                            .Post(app.properties.getProperty("rest.iDServiceEndPointURI"))
                            .addHeader("Content-Type", "application/json")
                            .addHeader("accept", "application/json")
                            .bodyString("{ \"country\": \"" + newIdentifierData.getCountry() + "\"," +
                                            " \"externalIds\": []," +
                                            " \"state\": \"" + newIdentifierData.getState() + "\"," +
                                            " \"type\": \"" + "RE" + "\"}",
                                    ContentType.APPLICATION_JSON)).returnContent().asString();
        } catch (Exception e) {
            System.out.println("HTTP connection is not established");
            e.getMessage();
        }
        JsonElement parsed = new JsonParser().parse(json);
        System.out.println(parsed);
        return parsed;
    }


}

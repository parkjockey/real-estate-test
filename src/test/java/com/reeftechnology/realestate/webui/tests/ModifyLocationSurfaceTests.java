package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.SurfaceData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ModifyLocationSurfaceTests extends TestBase {

    LocationData existingLocation;
    SurfaceData existingSurface;

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        existingLocation = new LocationData()
                .withLocationName("Surface-SampleLocation-Name" + HelperBase.randInt(1, 100))
                .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Suite 1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");
        app.location().createLocationAddSurface(existingLocation);
        existingSurface = new SurfaceData()
                .withType("ASPHALT")
                .withMode("FLAT")
                .withDescription("Sample surface description ASPHALT>FLAT");
        app.surface().create(existingSurface, true);
    }

    @Test
    public void testLocationModificationWithOneSurface() throws IOException {
        testrailID = 12053;

        SurfaceData modifedSurface = new SurfaceData()
                .withType("GRAVEL")
                .withMode("FLAT")
                .withDescription("Modified surface description GRAVEL>FLAT");

        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());
        LocationData modifiedLocation = app.location().all().iterator().next();

        app.location().modifyLocationSurface(modifiedLocation, modifedSurface, existingSurface);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());

        //validate modified location data with updated surface data
        SurfaceData expectedSurface = app.location().getLocationSurfaceInfo(app.location().all().iterator().next().getId());

        assertThat(expectedSurface, equalTo(modifedSurface));
    }

}

package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.SurfaceData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddSurfaceTests extends TestBase {

    LocationData createdLocation;
    int createdlocationID;

    @DataProvider
    public Iterator<Object[]> validSurfaceDataFromCsv() throws IOException {
        List<Object[]> surfaceData = new ArrayList<Object[]>();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/surfaces.csv")))) {
            String line = reader.readLine();
            while (line != null) {
                String[] split = line.split(";");
                surfaceData.add(new Object[]{new SurfaceData().withType(split[0])
                        .withMode(split[1])
                        .withDescription(split[2])});
                line = reader.readLine();
            }
            return surfaceData.iterator();
        }
    }

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
         createdLocation = new LocationData()
                .withLocationName("Surface-SampleLocation-Name" + HelperBase.randInt(1, 100))
                .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Suite 1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");
        app.location().createLocationAddSurface(createdLocation);
    }

    @Test(enabled = true)
    public void testAddSurfaceToLocationAsphaltFlat() throws IOException {
        testrailID = 9584;
        SurfaceData surface = new SurfaceData()
                .withType("ASPHALT")
                .withMode("FLAT")
                .withDescription("Sample surface description ASPHALT>FLAT");
        app.surface().create(surface, true);
        //SurfaceData createdSurface = app.surface().infoFromSurfaceForm();

        //get location id
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData newLocationFromList = app.location().all().iterator().next();
        createdlocationID = newLocationFromList.getId();

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(String.valueOf(createdlocationID));

        //validate modified surface data
        SurfaceData createdSurface = app.location().getLocationSurfaceInfo(createdlocationID);
        assertThat(createdSurface, equalTo(surface));
    }

    @Test(dataProvider = "validSurfaceDataFromCsv", enabled = false)
    public void testAddSurfaces(SurfaceData surface) throws IOException {
        app.surface().create(surface, true);

        //validate that created location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());

        //validate added surface data
        LocationData newLocationFromList = app.location().all().iterator().next();
        SurfaceData createdSurface = app.location().getLocationSurfaceInfo(newLocationFromList.getId());

        assertThat(createdSurface, equalTo(surface));
    }

    @Test(enabled = false)
    public void testAddSurfaceToLocationCreationUS() {
        File photo = new File("src/test/resources/images/reef.jpg");
        SurfaceData surface = new SurfaceData()
                .withType("Post-tension concrete")
                .withMode("Flat")
                .withDescription("Sample surface description")
                .withGeoCoordinates(25.766932, -80.185885)
                .withPhotoLinkToGeoCoordinates(photo);
        app.surface().fillSurfaceForm(surface);
    }

    @Test(enabled = false)
    public void testCurrentDir() {
        File currentDir = new File(".");
        System.out.println(currentDir.getAbsolutePath());
        File photo = new File("src/test/resources/images/reef.jpg");
        System.out.println(photo.getAbsolutePath());
        System.out.println(photo.exists());
    }

}



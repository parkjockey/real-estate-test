package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.SpaceData;
import com.reeftechnology.realestate.webui.model.SurfaceData;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddSpaceTests extends TestBase {
    LocationData createdLocation;
    SurfaceData existingSurface;

    @DataProvider
    public Iterator<Object[]> validSpacesFromCsv() throws IOException {
        List<Object[]> spaces = new ArrayList<Object[]>();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/spacesToSurface.csv")))) {
            String line = reader.readLine();
            while (line != null) {
                String[] split = line.split(";");
                spaces.add(new Object[]{new SpaceData()
                        .withType(split[0]).withMode(split[1])
                        .withQuantity(Integer.parseInt(split[2]))});
                line = reader.readLine();
            }
            return spaces.iterator();
        }
    }

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        createdLocation = new LocationData()
                .withLocationName("Surface-SampleLocation-Name" + HelperBase.randInt(1, 100))
                .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Suite 1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");
        app.location().createLocationAddSurface(createdLocation);
        existingSurface = new SurfaceData()
                .withType("ASPHALT")
                .withMode("FLAT")
                .withDescription("Sample Space to Surface description ASPHALT>FLAT");
        app.surface().create(existingSurface, false);
    }

    @Test(enabled = true)
    public void testAddSpaceMotorCycleReservedToSurface() throws IOException {
        testrailID = 9746;
        SpaceData createdSpaceData = new SpaceData()
                .withType("MOTORCYCLE")
                .withMode("RESERVED")
                .withQuantity(2);
        app.surface().addSpace(createdSpaceData);

        //validate that created location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());

        //validate created space data
        LocationData newLocationFromList = app.location().all().iterator().next();
        SpaceData spaceData = app.location().getLocationSurfaceSpaceInfo(newLocationFromList.getId());

        assertThat(spaceData, equalTo(createdSpaceData));
    }

    @Test(dataProvider = "validSpacesFromCsv")
    public void testAddSpacesToSurface(SpaceData createdSpaceData) throws IOException {
        app.surface().addSpace(createdSpaceData);

        //validate that form filled with space date
        SpaceData spaceData = app.surface().infoOnSpaceFromSurfaceForm(true, true);
        Assert.assertEquals(createdSpaceData, spaceData);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());

        //validate modified space data
        LocationData newLocationFromList = app.location().all().iterator().next();
        SpaceData modifedSpaceData = app.location().getLocationSurfaceSpaceInfo(newLocationFromList.getId());

        assertThat(modifedSpaceData, equalTo(createdSpaceData));
    }
}



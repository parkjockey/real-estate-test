package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.ApplicationManager;
import com.reeftechnology.realestate.webui.appmanager.TestRailHelpers;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.BrowserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.TestNG;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class TestBase {

    Logger logger = LoggerFactory.getLogger(TestBase.class);
    public int testrailID;
    protected static TestNG testNG = new TestNG();

    protected static final ApplicationManager app =
            new ApplicationManager(System.getProperty("browser", BrowserType.CHROME));

    @BeforeSuite
    public void setUp() throws IOException, ParseException {
        app.init();
        TestRailHelpers.createTestRailInstance();
        TestRailHelpers.setProjectSuiteId("REEF Cloud - Real Estate", "Master");
        TestRailHelpers.createRun("Web UI");
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        app.stop();
        TestRailHelpers.closeRun();
    }

    @BeforeMethod
    public void logbackTestStart(Method method, Object[] p) {
        logger.info("Start test: " + method.getName() + " with parameters " + Arrays.asList(p));
    }

    @AfterMethod(alwaysRun = true)
    public void logbackTestStop(Method method, Object[] p) throws IOException {
        takeScreenshot(method);
        if (testrailID > 1) {
            try {
                addTestRailID(testrailID, p);
            } catch (NullPointerException e) {
                System.out.println("Test case ID is invalid: " + testrailID);
            }
        }
        logger.info("Stop test: " + method.getName());
    }

    public void takeScreenshot(Method method) throws IOException {
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH_mm_ss");
        // Take a screenshot of the current page
        File screenshot = ((TakesScreenshot) app.getWd()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("reports/webui/screenshot" +
                method.getName() + dateTime.format(formatter) + ".png"));
    }

    @BeforeClass
    public static void beforeTestSuiteActions() throws ParseException {

    }

    @AfterClass
    public static void afterTestSuiteActions() {

    }

    public static void addTestRailID(int caseId, Object[] p) {
        TestRailHelpers.updateRun(caseId);
        TestRailHelpers.addResult("Test have been executed with parameters" + Arrays.asList(p), caseId);
        try {
            System.out.println("TestNG status is: " + testNG.getStatus());
            if (testNG.getStatus() == 0) {
                TestRailHelpers.addStatusForCase(1, caseId);
            } else if (testNG.getStatus() == 1) {
                TestRailHelpers.addStatusForCase(5, caseId);
            }
        } catch (NullPointerException e) {
            System.out.println("For testrailID value with: " + caseId + " TestNG m_status is not defined");
            TestRailHelpers.addStatusForCase(4, caseId);
        }
    }
}
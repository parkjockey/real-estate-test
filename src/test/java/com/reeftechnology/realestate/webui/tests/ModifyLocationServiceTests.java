package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.ServicesData;
import com.reeftechnology.realestate.webui.model.SurfaceData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ModifyLocationServiceTests extends TestBase {

    LocationData existingLocation;
    ServicesData existingservices;

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        existingLocation = new LocationData()
                .withLocationName("Service-SampleLocation-Name" + HelperBase.randInt(1, 100))
                .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Suite 1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");
        app.location().createLocationAddSurface(existingLocation);
        existingservices = new ServicesData()
                .withType("Convenience")
                .withSubtype("General")
                .withServiceID("AB-1234567")
                .withDescription("Sample description to convenience general services");
        app.services().create(existingservices);
    }

    @Test
    public void testLocationModificationWithOneService() throws IOException {
        testrailID = 17552;

        ServicesData modifedService = new ServicesData()
                .withType("Convenience")
                .withSubtype("General")
                .withServiceID("AB-1234567")
                .withDescription("Modified service convenience general services");

        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());
        LocationData modifiedLocation = app.location().all().iterator().next();

        app.location().modifyLocationService(modifiedLocation, modifedService);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());

        //validate created services data
        ServicesData expectedService = app.location().getLocationServiceInfo(app.location().all().iterator().next().getId());
        assertThat(expectedService, equalTo(modifedService));
    }

}

package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddExternalIDCreateLocationTests extends TestBase {

    @Test
    public void testLocationsCreationAddExternalLocationID() throws IOException {
        testrailID = 11759;
        LocationData createdLocation = new LocationData()
                .withLocationName("CanadaSampleLocationName" + HelperBase.randInt(1, 100))
                .withPropertyName("With external location id")
                .withAddressLine1("0123 Sample Street").withAddressLine2("Apartment 100")
                .withCity("Athabasca").withZipCode("V6X 3P8")
                .withCountry("CA").withState("AB")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED")
                .withExternalIdType("Location ID").withExternalIdValue("Location ID AP30291100");

        app.goTo().locationsPage();
        app.location().createLocationAddExternalId(createdLocation, true);

        //Search in list for created location by exact location name
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData foundLocation = app.location().all().iterator().next();
        assertThat(foundLocation.getLocationName(), equalTo(createdLocation.getLocationName()));
    }

    @Test
    public void testLocationsCreationAddExternalLotID() throws IOException {
        testrailID = 11776;
        LocationData createdLocation = new LocationData()
                .withLocationName("USSampleLocationName" + HelperBase.randInt(1, 100))
                .withPropertyName("Sample-property-name")
                .withAddressLine1("0123 Sample Street").withAddressLine2("Apartment 100")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED")
                .withExternalIdType("Lot ID").withExternalIdValue("Lot ID THIRD_0042212");

        app.goTo().locationsPage();
        app.location().createLocationAddExternalId(createdLocation, true);

        //Search in list for created location by exact location name
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData foundLocation = app.location().all().iterator().next();
        assertThat(foundLocation.getLocationName(), equalTo(createdLocation.getLocationName()));
    }

    @Test
    public void testLocationsCreationAddInvalidExternalLotIDValue() throws IOException {
        testrailID = 11761;
        LocationData createdLocation = new LocationData()
                .withLocationName("USSampleLocationName" + HelperBase.randInt(1, 100))
                .withPropertyName("Sample-property-name")
                .withAddressLine1("0123 Sample Street").withAddressLine2("Apartment 100")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED")
                .withExternalIdType("Lot ID").withExternalIdValue("@!#$");

        app.goTo().locationsPage();
        app.location().createLocationAddExternalId(createdLocation, false);
        assertThat(app.location().getAddLocationDetailsValidationErrorMessage(), equalTo("Invalid Format"));

        //Search in list for location by exact location name
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());
        assertThat(app.location().getUnableToRetrieveLocationsMessage(), equalTo(true));
    }

}

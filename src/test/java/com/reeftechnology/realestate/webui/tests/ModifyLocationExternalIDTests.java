package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ModifyLocationExternalIDTests extends TestBase {

    LocationData existingLocation;

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        existingLocation = new LocationData()
                .withLocationName("CanadaSampleLocationName" + HelperBase.randInt(1, 100))
                .withPropertyName("Sample-property-name")
                .withAddressLine1("0123 Sample Street").withAddressLine2("Apartment 100")
                .withCity("Athabasca").withZipCode("V6X 3P8")
                .withCountry("CA").withState("AB")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED")
                .withExternalIdType("Location ID").withExternalIdValue("Location ID value 123");
        app.location().createLocationAddExternalId(existingLocation, true);
    }

    @Test
    public void testLocationModificationWithExternalID() throws IOException {
        testrailID = 12055;

        LocationData modifiedLocation = new LocationData()
                .withLocationName("Modified-EXTID-USSampleLocationName" + HelperBase.randInt(1, 100))
                .withPropertyName("Sample-property-name")
                .withAddressLine1("601 Brickell Key").withAddressLine2("Suite 1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withExternalIdType("Lot ID").withExternalIdValue("Location ID modified 789");

        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());
        LocationData existingLocation = app.location().all().iterator().next();
        app.location().modifyLocationExternalId(existingLocation, modifiedLocation);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(modifiedLocation.getLocationName());

        //validate modified location data with external id type and value
        LocationData expectedLocationInfo = app.location()
                .getLocationExternalIdInfo(app.location().all().iterator().next().getId());

        assertThat(expectedLocationInfo, equalTo(modifiedLocation));
    }

}

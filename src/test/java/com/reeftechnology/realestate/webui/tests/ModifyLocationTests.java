package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.Locations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ModifyLocationTests extends TestBase {

    LocationData existingLocation;

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        existingLocation = new LocationData()
                .withLocationName("US 250 Hospital Pkwy" + HelperBase.randInt(1, 100))
                .withAddressLine1("250 Hospital Pkwy").withAddressLine2("Kaiser Parking")
                .withCity("Sacramento").withZipCode("95119")
                .withCountry("US").withState("CA");
        app.location().createDraftLocation(existingLocation);
    }

    @Test
    public void testLocationModification() throws IOException {
        testrailID = 12049;

        LocationData modifiedLocation = new LocationData()
                .withLocationName("Modified 1100 Lincoln" + HelperBase.randInt(1, 100))
                .withAddressLine1("1623 Alton Rd").withAddressLine2("Modified Suite 100")
                .withCity("Miami").withZipCode("33139")
                .withCountry("US").withState("FL")
                .withRegion("")
                .withCampusName("");

        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());
        Locations before = app.location().all();
        LocationData existingLocation = before.iterator().next();
        app.location().modify(existingLocation, modifiedLocation);

        //validate that locations list size is not modified so no duplicate was created
        app.goTo().locationsPage();
        app.location().searchForLocation(modifiedLocation.getLocationName());
        Locations after = app.location().all();
        assertThat(after.size(), equalTo(before.size()));

        //validate that modified location data is expected
        LocationData expectedLocationData = app.location().getLocationInfo(app.location().all().iterator().next().getId());
        assertThat(expectedLocationData, equalTo(modifiedLocation));
    }

    @Test(enabled = false)
    public void testLocationModificationBase() throws IOException {
        Locations before = app.location().all();
        LocationData modifiedLocation = before.iterator().next();
        LocationData location = new LocationData()
                .withId(modifiedLocation.getId())
                .withLocationName("*SampleLocationNameModify" + Integer.MAX_VALUE)
                .withPropertyName("Sample property name")
                .withAddressLine1("0123 Sample Street").withAddressLine2("Apartment 100")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL");
        app.location().modify(modifiedLocation, modifiedLocation);
        Assert.assertEquals(app.location().count(), before.size());
        Locations after = app.location().all();
        Assert.assertEquals(before, after);
        assertThat(after, equalTo(
                before.withOut(modifiedLocation).withAdded(location)));
    }


}

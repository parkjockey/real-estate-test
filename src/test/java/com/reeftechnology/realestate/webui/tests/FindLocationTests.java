package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import org.testng.annotations.*;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class FindLocationTests extends TestBase {

    String locationName;
    int realEstateIdNumber;

    @BeforeSuite
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        if (app.location().all().size() == 0) {
            locationName = "FIND-SampleLocation-Name" + HelperBase.randInt(1, 100);
            LocationData createdLocation = new LocationData()
                    .withLocationName(locationName)
                    .withPropertyName("Sample-property-name")
                    .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Suite 1000")
                    .withCity("Miami").withZipCode("33131")
                    .withCountry("US").withState("FL");
            app.location().create(createdLocation);
            app.goTo().locationsPage();
            realEstateIdNumber = app.location().all().iterator().next().getId();
        } else {
            realEstateIdNumber = app.location().all().iterator().next().getId();
            locationName = app.location().all().iterator().next().getLocationName();
        }
    }

    @Test
    public void testFindLocationByLocationID() throws IOException {
        testrailID = 6872;

        app.location().searchForLocation(String.valueOf(realEstateIdNumber));

        assertThat(realEstateIdNumber, equalTo(
                app.location().all().iterator().next().getId()));
    }

    @Test
    public void testFindLocationByLocationName() throws IOException {
        testrailID = 6866;

        app.location().searchForLocation(locationName);

        assertThat(locationName, equalTo(
                app.location().all().iterator().next().getLocationName()));
    }

    @Test()
    public void testFindLocationByInvalidLocationName() throws IOException {
        testrailID = 12119;

        app.location().searchForLocation("Non existing " + locationName);

        assertThat(app.location().noResultsFound(), equalTo(true));
    }

    @Test()
    public void testFindLocationByInvalidLocationID() throws IOException {
        testrailID = 15487;

        app.location().searchForLocation("00" + realEstateIdNumber);

        assertThat(app.location().noResultsFound(), equalTo(true));
    }

}

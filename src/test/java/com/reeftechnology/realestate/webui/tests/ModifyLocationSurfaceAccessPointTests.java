package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.AccessPointData;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.SurfaceData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ModifyLocationSurfaceAccessPointTests extends TestBase {

    LocationData existingLocation;
    SurfaceData existingSurface;
    AccessPointData existingAccessPointData;

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        existingLocation = new LocationData()
                .withLocationName("Surface-AP-SampleLocation-Name" + HelperBase.randInt(1, 100))
                .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Suite 1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");
        app.location().createLocationAddSurface(existingLocation);
        existingSurface = new SurfaceData()
                .withType("ASPHALT")
                .withMode("FLAT")
                .withDescription("Sample surface description ASPHALT>FLAT");
        app.surface().create(existingSurface, false);
        existingAccessPointData = new AccessPointData()
                .withType("ENTRANCE_EXIT")
                .withDescription("ENTRANCE_EXIT access point description");
        app.surface().addAccessPoint(existingAccessPointData);
    }

    @Test
    public void testLocationModificationWithOneSurfaceWithOneAccessPoint() throws IOException {
        testrailID = 12068;

        AccessPointData modifiedAccessPointData = new AccessPointData()
                .withType("ENTRANCE")
                .withDescription("ENTRANCE modified access point");

        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());
        LocationData modifiedLocation = app.location().all().iterator().next();

        app.surface().modifyLocationSurfaceAccessPoint(modifiedLocation, modifiedAccessPointData);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());

        //validate modified surface>access point data
        AccessPointData expectedAccessPointData = app.location().getLocationSurfaceAccessPointInfo(app.location().all().iterator().next().getId());

        assertThat(expectedAccessPointData, equalTo(modifiedAccessPointData));
    }

}

package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ModifyLocationGCTests extends TestBase {

    LocationData existingLocation;

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        existingLocation = new LocationData()
                .withLocationName("UserDataSampleLocationName" + HelperBase.randInt(1, 100))
                .withAddressLine1("601 Brickell Key Dr")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");
            app.location().createLocationAddGeocoordinates(existingLocation, true, true, false);
    }

    @Test
    public void testLocationModificationWithGeoCoordinatesUsePreciselyProvidedData() throws IOException {
        testrailID = 17054;

        LocationData modifiedLocation = new LocationData()
                .withLocationName("Modified-GEOC-USSampleLocationName" + HelperBase.randInt(1, 100))
                .withAddressLine1("3575 Lehigh Dr").withAddressLine2("St 10")
                .withCity("Santa Clara").withZipCode("95051-6072")
                .withCountry("US").withState("CA")
                .withLocationType("PARKING").withLocationSubType("SURFACE_LOT").withLocationMode("GATED")
                .withLatitude(37.33552).withLongitude(121.99469);

        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());
        LocationData existingLocation = app.location().all().iterator().next();
        app.location().modifyLocationGeocoordinates(existingLocation, modifiedLocation, true);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(modifiedLocation.getLocationName());

        //validate modified location data with update geocoordinates
        LocationData expectedLocationInfo = app.location()
                .getLocationGeocoordinatesInfo(app.location().all().iterator().next().getId());

        assertThat(expectedLocationInfo, equalTo(modifiedLocation));
    }

    @Test
    public void testLocationModificationWithGeoCoordinatesUseUserProvidedData() throws IOException {
        testrailID = 12059;

        LocationData modifiedLocation = new LocationData()
                .withLocationName("Modified-GEOC-USSampleLocationName" + HelperBase.randInt(1, 100))
                .withAddressLine1("3575 Lehigh Dr").withAddressLine2("St 10")
                .withCity("Santa Clara").withZipCode("95051-6072")
                .withCountry("US").withState("CA")
                .withLocationType("PARKING").withLocationSubType("SURFACE_LOT").withLocationMode("GATED")
                .withLatitude(37.33552).withLongitude(121.99469);

        app.goTo().locationsPage();
        app.location().searchForLocation(existingLocation.getLocationName());
        LocationData existingLocation = app.location().all().iterator().next();
        app.location().modifyLocationGeocoordinates(existingLocation, modifiedLocation, false);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(modifiedLocation.getLocationName());

        //validate modified location data with update geocoordinates
        LocationData expectedLocationInfo = app.location()
                .getLocationGeocoordinatesInfo(app.location().all().iterator().next().getId());

        assertThat(expectedLocationInfo, equalTo(modifiedLocation));
    }

}

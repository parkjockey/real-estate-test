package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.StructureData;
import com.reeftechnology.realestate.webui.model.SurfaceData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ApproveLocationTests extends TestBase {

    static LocationData createdLocation;
    int createdlocationID;

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        createdLocation = new LocationData()
                .withLocationName("Vista Garage and surface lot" + HelperBase.randInt(1,999))
                .withAddressLine1("1301 ASSEMBLY STREET").withAddressLine2("1")
                .withCity("COLUMBIA").withZipCode("29201")
                .withCountry("US").withState("SC")
                .withPropertyName("Vista Business Park")
                .withPropertyType("Commercial")
                .withOperatingCompany("AMP001")
                .withRegion("US Central")
                .withCampusName("California State University");
        app.location().createLocationForStructure(createdLocation);
        app.location().saveGeoCoordinatesBasedOnAddressData(createdLocation);
        StructureData structureData = new StructureData()
                .withStructureName("ABOVE GROUND GARAGE 1")
                .withStructureType("PARKING")
                .withStructureSubType("ABOVE_GROUND_GARAGE")
                .withStructureMode("GATED")
                .withStructureEquipmentType("Flash")
                .withDescription("Sample structure description");
        app.structure().create(structureData);
        SurfaceData surface = new SurfaceData()
                .withType("ASPHALT")
                .withMode("FLAT")
                .withDescription("Sample surface description ASPHALT>FLAT");
        app.surface().create(surface, true);
        app.location().submitLocationForApproval();
        app.session().logout();
    }

    @Test(enabled = true)
    public void testApproveLocationSentForApproval() throws IOException {
        testrailID = 13865;
        //login as user with Location approver role
        app.session().login(app.getProperties().getProperty("web.ApproverLogin"), app.getProperties().getProperty("web.ApproverPassword"));

        //get location id and approve
        app.goTo().locationsApprovalPage();
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData foundLocationFromList = app.location().pendingAll().iterator().next();
        createdlocationID = foundLocationFromList.getId();
        app.location().approveLocation(createdlocationID);

        //validate that approved location is NOT searchable in locations list
        app.goTo().locationsApprovalPage();
        app.location().searchForLocation(String.valueOf(createdlocationID));
        assertThat(app.location().noLocationsFound(), equalTo(true));
        //logout with that role
        app.session().logout();

        //login as user with Location owner role
        app.session().login(app.getProperties().getProperty("web.OwnerLogin"),
                app.getProperties().getProperty("web.OwnerPassword"));

        //go to find location table and search approved location
        app.goTo().locationsPage();
        app.location().searchForLocation(String.valueOf(createdlocationID));

        app.location().openLocationToView("US-SC-RE-" + createdlocationID
                /*this is because locationdata object does have field id = int*/);

        assertThat(app.location().getLocationsApprovalStatus(), equalTo("Published"));
    }

}



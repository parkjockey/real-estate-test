package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.FeatureData;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.SurfaceData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddFeatureTests extends TestBase {

    LocationData createdLocation;
    SurfaceData existingSurface;

    @DataProvider
    public Iterator<Object[]> validFeaturesFromCsv() throws IOException {
        List<Object[]> features = new ArrayList<Object[]>();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/featuresToSurface.csv")))) {
            String line = reader.readLine();
            while (line != null) {
                String[] split = line.split(";");
                features.add(new Object[]{new FeatureData().withType(split[0])
                        .withDescription(split[1])});
                line = reader.readLine();
            }
            return features.iterator();
        }
    }

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
        createdLocation = new LocationData()
                .withLocationName("SurfaceFeature-SampleLocation-Name" + HelperBase.randInt(1, 100))
                .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Suite 1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");
        app.location().createLocationAddSurface(createdLocation);
        existingSurface = new SurfaceData()
                .withType("ASPHALT")
                .withMode("FLAT")
                .withDescription("Sample Space to Surface description ASPHALT>FLAT");
        app.surface().create(existingSurface, false);
    }

    @Test
    public void testAddFeatureElectricityToSurface() throws IOException {
        testrailID = 9753;

        FeatureData createdFeatureData = new FeatureData()
                .withType("ELECTRICITY")
                .withDescription("ELECTRICITY feature description");
        app.surface().addFeature(createdFeatureData);

        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());

        //validate created feature data
        LocationData newLocationFromList = app.location().all().iterator().next();
        FeatureData featureData = app.location().getLocationSurfaceFeatureInfo(newLocationFromList.getId());

        assertThat(featureData, equalTo(createdFeatureData));
    }

    @Test(dataProvider = "validFeaturesFromCsv")
    public void testAddFeaturesToSurface(FeatureData createdFeatureData) throws IOException {
        app.surface().addFeature(createdFeatureData);

        //validate that form filled with feature date
        FeatureData featureData = app.surface().infoOnFeatureFromSurfaceForm(true, true);
        assertThat(featureData, equalTo(createdFeatureData));


        //validate that modified location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());

        //validate created feature data
        LocationData newLocationFromList = app.location().all().iterator().next();
        FeatureData modifedFeatureData = app.location().getLocationSurfaceFeatureInfo(newLocationFromList.getId());

        assertThat(modifedFeatureData, equalTo(createdFeatureData));
    }

}



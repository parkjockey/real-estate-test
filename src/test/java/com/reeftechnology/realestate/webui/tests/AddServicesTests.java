package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import com.reeftechnology.realestate.webui.model.ServicesData;
import com.reeftechnology.realestate.webui.model.SurfaceData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddServicesTests extends TestBase {

    private LocationData createdLocation;
    int createdlocationID;

    @DataProvider
    public Iterator<Object[]> validServicesDataFromCsv() throws IOException {
        List<Object[]> servicesData = new ArrayList<Object[]>();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/surfaces.csv")))) {
            String line = reader.readLine();
            while (line != null) {
                String[] split = line.split(";");
                servicesData.add(new Object[]{new ServicesData().withType(split[0])
                        .withSubtype(split[1])
                        .withServiceID(split[2])
                        .withDescription(split[3])});
                line = reader.readLine();
            }
            return servicesData.iterator();
        }
    }

    @BeforeMethod
    public void ensurePreconditions() throws IOException {
        app.goTo().locationsPage();
         createdLocation = new LocationData()
                .withLocationName("Services-SampleLocation-Name" + HelperBase.randInt(1, 100))
                .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Suite 1000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");
        app.location().createLocationAddServices(createdLocation);
    }

    @Test(enabled = true)
    public void testAddServicesParkingGeneralToLocation() throws IOException {
        testrailID = 16560;
        ServicesData services = new ServicesData()
                .withType("Parking")
                .withSubtype("General")
                .withServiceID("AB-1234567")
                .withDescription("Sample description to parking general services");
        app.services().create(services);

        //get location id
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData newLocationFromList = app.location().all().iterator().next();
        createdlocationID = newLocationFromList.getId();

        //validate that created location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(String.valueOf(createdlocationID));

        //validate created services data
        ServicesData createdServices = app.location().getLocationServiceInfo(createdlocationID);
        assertThat(createdServices, equalTo(services));
    }

    @Test(enabled = false)
    public void testAddServicesConvenienceGeneralToLocation() throws IOException {
        testrailID = 16560;
        ServicesData services = new ServicesData()
                .withType("Convenience")
                .withSubtype("General")
                .withServiceID("AB-1234567")
                .withDescription("Sample description to convenience general services");
        app.services().create(services);

        //get location id
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData newLocationFromList = app.location().all().iterator().next();
        createdlocationID = newLocationFromList.getId();

        //validate that created location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(String.valueOf(createdlocationID));

        //validate created services data
        ServicesData createdServices = app.location().getLocationServiceInfo(createdlocationID);
        assertThat(createdServices, equalTo(services));
    }

    @Test(dataProvider = "validServicesDataFromCsv", enabled = false)
    public void testAddServices(SurfaceData surface) throws IOException {
        app.surface().create(surface, true);

        //validate that created location is searchable in locations list
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());

        //validate added surface data
        LocationData newLocationFromList = app.location().all().iterator().next();
        SurfaceData createdSurface = app.location().getLocationSurfaceInfo(newLocationFromList.getId());

        assertThat(createdSurface, equalTo(surface));
    }

}



package com.reeftechnology.realestate.webui.tests;

import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import com.reeftechnology.realestate.webui.model.LocationData;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AddGeoCoordinateCreateLocationTests extends TestBase {

    @Test
    public void testLocationsCreationAddGeocoordinatesUseUserProvidedData() throws IOException {
        testrailID = 15155;
        LocationData createdLocation = new LocationData()
                .withLocationName("UserDataSampleLocationName" + HelperBase.randInt(1, 100))
                .withPropertyName("Sample-USproperty")
                .withAddressLine1("601 Brickell Key Dr").withAddressLine2("Ste 10000")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");

        app.goTo().locationsPage();
        app.location().createLocationAddGeocoordinates(createdLocation, true, true, false);

        //Search in list for created location by exact location name
        app.location().refreshPage();
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData foundLocation = app.location().all().iterator().next();
        assertThat(foundLocation.getLocationName(), equalTo(createdLocation.getLocationName()));
    }

    @Test
    public void testLocationsCreationAddGeocoordinatesUsePreciselyData() throws IOException {
        testrailID = 15282;
        LocationData createdLocation = new LocationData()
                .withLocationName("PBDataSampleLocationName" + HelperBase.randInt(1, 100))
                .withPropertyName("Sample-USproperty")
                .withAddressLine1("602 Brickell Key Dr").withAddressLine2("Ste 1")
                .withCity("Miami").withZipCode("33131")
                .withCountry("US").withState("FL")
                .withLocationType("PARKING")
                .withLocationSubType("SURFACE_LOT")
                .withLocationMode("GATED");

        app.goTo().locationsPage();
        app.location().createLocationAddGeocoordinates(createdLocation, true, true, true);

        //Search in list for created location by exact location name
        app.location().refreshPage();
        app.goTo().locationsPage();
        app.location().searchForLocation(createdLocation.getLocationName());
        LocationData foundLocation = app.location().all().iterator().next();
        assertThat(foundLocation.getLocationName(), equalTo(createdLocation.getLocationName()));
    }

}

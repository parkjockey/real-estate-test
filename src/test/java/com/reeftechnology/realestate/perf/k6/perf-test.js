import http from "k6/http";
import {Trend} from "k6/metrics";
import {sleep} from "k6";
import k6 from "k6";
import {Rate} from "k6/metrics";

export let errorRate = new Rate("errors");
module.exports.options = {
    stages: [
        {target: 2, duration: '5m'},
        {target: 5, duration: '10m'},
        {target: 20, duration: '10m'}
    ],
    thresholds: {
        requests: ['count < 100'],
        http_req_duration: ["p(90)<1000"], // 90 percent of response times must be below 1000ms
        errors: ["rate<0.1"] // <10% errors
    }
}

let re_Owner_QA_accessToken = "eyJraWQiOiJlcHVxckZtVERPd0ZvcTFqUktGcUthajVRbkxEQkxrdDZkQUFtYURpUjVFPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJmNWJhODhhYS0yZjQ0LTQ0MDMtOWU3Ni1kZDU3MjgwMWI5ZTEiLCJldmVudF9pZCI6ImIzM2YxMTEzLTA2ZjAtNDNkOS04OWMxLWExZmJlNjZjZDJhNSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4gb3BlbmlkIHByb2ZpbGUgZW1haWwiLCJhdXRoX3RpbWUiOjE2MDEzNDgwODgsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTIuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0yXzF1UG0wZ29wbiIsImV4cCI6MTYwMTM1MTY4OCwiaWF0IjoxNjAxMzQ4MDg4LCJ2ZXJzaW9uIjoyLCJqdGkiOiI3ODcxZWM4Yy0yN2FiLTRhMGUtYWE4Ni02YWIxYmRkNGY3NTUiLCJjbGllbnRfaWQiOiI1YXJ2a3FqdGI1c2cxNmcxa2puNTVpanVwNSIsInVzZXJuYW1lIjoiZjViYTg4YWEtMmY0NC00NDAzLTllNzYtZGQ1NzI4MDFiOWUxIn0.Vax5vn4BeJeZnnrgnpA3Swgnd_sM4bm6ZyKcMxwKidmXjWuA9L6hI6lNnEsDnCxG_whmLvaJ5Q16c-KBYaewv82Cfx2gugjlMnfJ_jESQ3p71gX2izHTYXcNW7yUnGclqiRqcCP3txMoXWca-5ue_HPCbqh-paG97cg5N0ermDyvxAfjyYW7DvKyVgfg3V461ez2vEDUzZmdHcK0cns-PxqeKCKISgojz5isV9JPqSjuzbN02mMLY2YTX6YB20IhhvxtcO_Y0Y-Su7VA1zKRuJzQa3L-2d5_Q1iLonqJHT5h9hTm5OiJLbjR3UoxFR0aCJFn4UM-dEE4VXvRm6nVsQ";
let graphqlendpoint = "https://middleware-dev.co.reefplatform.com/api/re";

var myTrendSort = new Trend("Sorting location list by real estate ID > ASC ");
var myTrendSearch = new Trend("Searching location by name ");
var myTrendGetLocationByID = new Trend("Get location info by real estate ID ");
var myTrendAddNewLocation = new Trend("Add new draft location ");
var myTrendUpdateExistingLocation = new Trend("Updating location name for existing draft location ");

module.exports.default = function () {
    const before = new Date().getTime();
    const T = 5; // time needed to complete a VU iteration

    let queryPagination = "query getLocations($after: String, $before: String, $searchString: String, $approvalStatuses: ApprovalStatus) " +
        "{  locations(pageSize: 100, after: $after, before: $before, searchString: $searchString, " +
        "orderBy: realEstateId, " +
        "sortOrder: ASC, " +
        "approvalStatuses: $approvalStatuses) {    cursors {      after      before      __typename    }    hasMore    totalSize    locations {      realEstateId      locationName      locationType      addressLine1      addressLine2      city      state      locationStatus      numberOfSpaces      numberOfSurfaces      approvalStatus      hasApprovedVersion      currentVersion      __typename    }    __typename  }}";
    let querySearchByLocationName = "query getLocations($after: String, $before: String, $approvalStatuses: ApprovalStatus) " +
        "{  locations(pageSize: 100, after: $after, before: $before, " +
        "searchString: \"River Rock Casino Resort\", " +
        "orderBy: realEstateId, sortOrder: ASC, approvalStatuses: $approvalStatuses) {    cursors {      after      before      __typename    }    hasMore    totalSize    locations {      realEstateId      locationName      locationType      addressLine1      addressLine2      city      state      locationStatus      numberOfSpaces      numberOfSurfaces      approvalStatus      hasApprovedVersion      " +
        "currentVersion      __typename    }    __typename  }}";
    let realEstateId = "US-CA-RE-1009192";
    let querySearchByLocationID = "query getLocationById" +
        "{  getLocation(locationId: \"" + realEstateId + "\", displayMode: EDIT) " +
        "{    realEstateId    draft    approvalStatus    hasApprovedVersion    currentVersion    locationName    propertyName    locationType    locationSubType    locationMode    locationStatus    equipmentType    addressLine1    addressLine2    state    city    country    zipCode    latitude    longitude    preciselyAddressLine1    preciselyAddressLine2    preciselyCity    preciselyCountry    preciselyLatitude    preciselyLongitude    preciselyPbKey    preciselyState    preciselyTzId    preciselyZipCode    numberOfSpaces    numberOfSurfaces    numberOfFeatures    numberOfAccessPoints    externalIds {      externalIdType      value      __typename    }    surfaces {      surfaceId      surfaceType      surfaceMode      description      accessPoints {        accessPointId        accessPointType        description        __typename      }      features {        featureId        featureType        description        __typename      }      spaces {        quantity        spaceType        spaceMode        __typename      }      __typename    }    services {      code      description      serviceId      serviceType      serviceSubType  " +
        "    __typename    }    __typename  }}";
    let locationName = "PerfTest location delete";
    let queryAddNewLocation = "mutation {\n" +
        "  addLocation(\n" +
        "    locationInput: {\n" +
        "      locationName: \"" + locationName + "\"\n" +
        "      locationType: \"PARKING\"\n" +
        "      locationSubType: \"SURFACE_LOT\"\n" +
        "      locationMode: \"GATED\"\n" +
        "      locationStatus: \"ACTIVE\"\n" +
        "      propertyName: \"Testing-rest-Property\"\n" +
        "      addressLine1: \"123 main street\"\n" +
        "      addressLine2: \"\"\n" +
        "      city: \"Santa Clara\"\n" +
        "      state: \"CA\"\n" +
        "      country: \"US\"\n" +
        "      latitude: null\n" +
        "      zipCode: \"95051\"\n" +
        "      timeStamp: \"2020-06-25T19:08:02.256Z\"\n" +
        "      approvalStatus:\"IN_PROGRESS\"\n" +
        "      preciselyStreetDataType:\"Master Location 1\"\n" +
        "      equipmentType:\"other\"\n" +
        "      externalIds: [{ externalIdType: \"Location ID\", value: \"666666\" }]\n" +
        "      surfaces: [\n" +
        "        {\n" +
        "          surfaceType: \"test1\"\n" +
        "          features: [{ featureType: \"feature1\", description: \"feature desc\" }]\n" +
        "          accessPoints: [\n" +
        "            { accessPointType: \"test1\", description: \"access point desc\" }\n" +
        "          ]\n" +
        "        }\n" +
        "        {\n" +
        "          surfaceType: \"test2\"\n" +
        "          features: [{ featureType: \"feature2\", description: \"description\" }]\n" +
        "        }\n" +
        "        {\n" +
        "          surfaceType: \"test3\"\n" +
        "          accessPoints: [\n" +
        "            {\n" +
        "              accessPointType: \"AP-test-1\"\n" +
        "              description: \"access point testing\"\n" +
        "            }\n" +
        "          ]\n" +
        "        }\n" +
        "      ]\n" +
        "    }\n" +
        "  ) {\n" +
        "    currentVersion\n" +
        "    approvalStatus\n" +
        "    realEstateId\n" +
        "    locationName\n" +
        "    locationType\n" +
        "    locationSubType\n" +
        "    locationStatus\n" +
        "    locationMode\n" +
        "    propertyName\n" +
        "    addressLine1\n" +
        "    addressLine2\n" +
        "    preciselyStreetDataType\n" +
        "    equipmentType\n" +
        "    city\n" +
        "    state\n" +
        "    country\n" +
        "    zipCode\n" +
        "    createdBy {\n" +
        "      sourceId\n" +
        "      sourceType\n" +
        "    }\n" +
        "    createdDate\n" +
        "    updatedBy {\n" +
        "      sourceId\n" +
        "      sourceType\n" +
        "    }\n" +
        "    updatedDate\n" +
        "    surfaces {\n" +
        "      surfaceId\n" +
        "      surfaceType\n" +
        "      features {\n" +
        "        featureId\n" +
        "        featureType\n" +
        "      }\n" +
        "      accessPoints {\n" +
        "        accessPointId\n" +
        "        accessPointType\n" +
        "      }\n" +
        "    }\n" +
        "  }\n" +
        "}";

    let headers = {
        'Authorization': `${re_Owner_QA_accessToken}`,
        "Content-Type": "application/json",
        "Content-Length": "35305",
        "Host": "middleware-dev.co.reefplatform.com",
        "Organization": "1",
    };

    //pagination - sort by real estate id asc request
    var resPagination = http.post(graphqlendpoint,
        JSON.stringify({query: queryPagination}),
        {headers: headers});
    myTrendSort.add(resPagination.timings.sending + resPagination.timings.receiving);
    const resultPagination = k6.check(resPagination, {
        "Query pagination for location status was 200": r => r.status === 200
    });
    errorRate.add(!resultPagination);
    let bodyPagination = JSON.parse(resPagination.body);
    let totalsizePagination = bodyPagination.data.locations.totalSize;
    k6.check(resPagination, {
        'Total size of sorted locations is valid': totalsizePagination >= 1,
    });
    sleep(0.3);

    //search by location name request
    var resSearchByLocationName = http.post(graphqlendpoint,
        JSON.stringify({query: querySearchByLocationName}),
        {headers: headers});
    myTrendSearch.add(resSearchByLocationName.timings.sending + resSearchByLocationName.timings.receiving);
    const resultSearchByLocationName = k6.check(resSearchByLocationName, {
        "Query search for location by name status was 200": r => r.status === 200
    });
    errorRate.add(!resultSearchByLocationName);
    let bodyresultSearchByLocationName = JSON.parse(resSearchByLocationName.body);
    let totalsizeSearchByLocationName = bodyresultSearchByLocationName.data.locations.totalSize;
    k6.check(resSearchByLocationName, {
        'Total size of found locations is valid': totalsizeSearchByLocationName >= 1,
    });
    sleep(0.3);

    //add new location request
    var resAddNewLocation = http.post(graphqlendpoint,
        JSON.stringify({query: queryAddNewLocation}),
        {headers: headers});
    myTrendAddNewLocation.add(resAddNewLocation.timings.sending + resAddNewLocation.timings.receiving);
    const resultAddNewLocation = k6.check(resAddNewLocation, {
        "Query add new location was 200": r => r.status === 200
    });
    errorRate.add(!resultAddNewLocation);
    let bodyresultAddnewLocation = JSON.parse(resAddNewLocation.body);
    let totalsizebodyresultrealEstateId = bodyresultAddnewLocation.data.addLocation.realEstateId;
    let totalsizebodyresultrealEstateLocationName = bodyresultAddnewLocation.data.addLocation.locationName;
    console.log('Added new location with real estate ID '+ totalsizebodyresultrealEstateId);
    k6.check(resAddNewLocation, {
        'Added new location with location name ': totalsizebodyresultrealEstateLocationName == locationName,
    });
    sleep(0.3);

    //get location by ID request
    var resGetLocationByID = http.post(graphqlendpoint,
        JSON.stringify({query: querySearchByLocationID}),
        {headers: headers});
    console.log(JSON.stringify(resGetLocationByID.body))
    myTrendGetLocationByID.add(resGetLocationByID.timings.sending + resGetLocationByID.timings.receiving);
    const resultGetLocationById = k6.check(resGetLocationByID, {
        "Query search for location by real estate ID status was 200": r => r.status === 200
    });
    errorRate.add(!resultGetLocationById);
    let bodyresultGetLocationById = JSON.parse(resGetLocationByID.body);
    let responserealEstateId = bodyresultGetLocationById.data.getLocation.realEstateId;
    k6.check(resGetLocationByID, {
        'Location by requred real estate ID is valid': responserealEstateId == realEstateId,
    });
    sleep(0.3);

    const after = new Date().getTime();
    const diff = (after - before) / 1000;
    const remainder = T - diff;
    k6.check(remainder, {'Reached request rate': remainder > 0});
    if (remainder > 0) {
        sleep(remainder);
    } else {
        console.warn(
            `Timer exhausted! The execution time of the test took longer than ${T} seconds`
        );
    }
}
import http from "k6/http";
import {Trend} from "k6/metrics";
import {sleep} from "k6";
import k6 from "k6";
import {Rate} from "k6/metrics";

export let errorRate = new Rate("errors");
module.exports.options = {
    stages: [
        {target: 1, duration: '5s'},
        // {target: 5, duration: '10m'},
        // {target: 20, duration: '10m'}
    ],
    thresholds: {
        requests: ['count < 100'],
        http_req_duration: ["p(90)<1000"], // 90 percent of response times must be below 1000ms
        errors: ["rate<0.1"] // <10% errors
    }
}

let re_Admin_accessToken = "eyJraWQiOiJZTXk4ZTRYOGFjdndnM2gxZTAyU0pCSFwvdzhXc1NTaEhWMktKTjdyOWhQbz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJmMTI1MjNkNC0wMDFiLTQxOTAtYjMxMC0yN2E2NzdkNDY2NWUiLCJldmVudF9pZCI6IjNhMjg3MzNjLTE0YzktNDkyMi05MmU2LTEyYmMzZjVmYWFhYSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4gb3BlbmlkIHByb2ZpbGUgZW1haWwiLCJhdXRoX3RpbWUiOjE2MDI4NTg3MDEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTIuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0yX1pBNUV6eUs1UyIsImV4cCI6MTYwMjg4NzUwMSwiaWF0IjoxNjAyODU4NzAxLCJ2ZXJzaW9uIjoyLCJqdGkiOiIzZTA5MGRmZi04ZGJmLTQzMGUtYmIyYi1kNzFlMGYyMGI2NjciLCJjbGllbnRfaWQiOiI0ZWw4cHNrNGh2cjlsZW1lN2FvbWRnbjJubCIsInVzZXJuYW1lIjoiZjEyNTIzZDQtMDAxYi00MTkwLWIzMTAtMjdhNjc3ZDQ2NjVlIn0.DtTkEpSF3lSjMFy__yFKDlnMqlciWEu4uVeztd3cJhqSkmYqX6MVlJETyf6XEy1w4NZ8UfyHpfP1d_Q3-ex7u0fcuQwZUjbuMxtw-k5HP2WW1ImYVjd-UOQaTgoLF5oH6A9r6g6I2DmD4sSIccVOq2Vszl0bHISyPGc1L7B9gXA1msFgN6PBo7RGZjDDlki7UsHlmGOIKQvygBsagkzAXekBAeDZBrr8Ba2JgJGWdCx_-iW3-AMU34JFzsBjzOmuTaLI0eaT4JuGQhfw2uYy9DLK4LqcBMPG2utKM-_9QPv72rf-IVTUOu-fpemzxa8CbSWZ25q8vJxRx5RUT4ltlA";
let graphqlendpoint = "https://middleware-qa.co.reefplatform.com/api/re";

var myTrendSort = new Trend("Sorting location list by real estate ID > ASC ");
var myTrendSearch = new Trend("Searching location by name ");
var myTrendGetLocationByID = new Trend("Get location info by real estate ID ");
var myTrendAddNewLocation = new Trend("Add new draft location ");
var myTrendUpdateExistingLocation = new Trend("Updating location name for existing draft location ");

module.exports.default = function () {
    const before = new Date().getTime();
    const T = 5; // time needed to complete a VU iteration

    let queryPagination = "query getLocations($after: String, $before: String, $searchString: String, $approvalStatuses: ApprovalStatus) " +
        "{  locations(pageSize: 100, after: $after, before: $before, searchString: $searchString, " +
        "orderBy: realEstateId, " +
        "sortOrder: ASC, " +
        "approvalStatuses: $approvalStatuses) {    cursors {      after      before      __typename    }    hasMore    totalSize    locations {      realEstateId      locationName      locationType      addressLine1      addressLine2      city      state      locationStatus      numberOfSpaces      numberOfSurfaces      approvalStatus      hasApprovedVersion      currentVersion      __typename    }    __typename  }}";
    let querySearchByLocationName = "query getLocations($after: String, $before: String, $approvalStatuses: ApprovalStatus) " +
        "{  locations(pageSize: 100, after: $after, before: $before, " +
        "searchString: \"River Rock Casino Resort\", " +
        "orderBy: realEstateId, sortOrder: ASC, approvalStatuses: $approvalStatuses) {    cursors {      after      before      __typename    }    hasMore    totalSize    locations {      realEstateId      locationName      locationType      addressLine1      addressLine2      city      state      locationStatus      numberOfSpaces      numberOfSurfaces      approvalStatus      hasApprovedVersion      " +
        "currentVersion      __typename    }    __typename  }}";
    let realEstateId = "US-CA-RE-1000002";
    let querySearchByLocationID = "query getLocationById" +
        "{  getLocation(locationId: \"" + realEstateId + "\", displayMode: EDIT) " +
        "{    realEstateId    draft    approvalStatus    hasApprovedVersion    currentVersion    locationName    propertyName    locationType    locationSubType    locationMode    locationStatus    equipmentType    addressLine1    addressLine2    state    city    country    zipCode    latitude    longitude    preciselyAddressLine1    preciselyAddressLine2    preciselyCity    preciselyCountry    preciselyLatitude    preciselyLongitude    preciselyPbKey    preciselyState    preciselyTzId    preciselyZipCode    numberOfSpaces    numberOfSurfaces    numberOfFeatures    numberOfAccessPoints    externalIds {      externalIdType      value      __typename    }    surfaces {      surfaceId      surfaceType      surfaceMode      description      accessPoints {        accessPointId        accessPointType        description        __typename      }      features {        featureId        featureType        description        __typename      }      spaces {        quantity        spaceType        spaceMode        __typename      }      __typename    }    services {      code      description      serviceId      serviceType      serviceSubType  " +
        "    __typename    }    __typename  }}";
    let locationName = "PerfTest location delete";
    let queryAddNewLocation = "mutation {\n" +
        "  addLocation(\n" +
        "    locationInput: {\n" +
        "      locationName: \"" + locationName + "\"\n" +
        "      locationType: \"PARKING\"\n" +
        "      locationSubType: \"SURFACE_LOT\"\n" +
        "      locationMode: \"GATED\"\n" +
        "      locationStatus: \"ACTIVE\"\n" +
        "      propertyName: \"Testing-rest-Property\"\n" +
        "      addressLine1: \"450 10th St\"\n" +
        "      addressLine2: \"\"\n" +
        "      city: \"San Francisco\"\n" +
        "      state: \"CA\"\n" +
        "      country: \"US\"\n" +
        "      latitude: null\n" +
        "      zipCode: \"94103\"\n" +
        //"      timeStamp: \"2020-06-25T19:08:02.256Z\"\n" +
        "      approvalStatus:\"IN_PROGRESS\"\n" +
        "      preciselyStreetDataType:\"Master Location 1\"\n" +
        "      equipmentType:\"other\"\n" +
        "      externalIds: [{ externalIdType: \"Location ID\", value: \"666666\" }]\n" +
        "      surfaces: [\n" +
        "        {\n" +
        "          surfaceType: \"test1\"\n" +
        "          features: [{ featureType: \"feature1\", description: \"feature desc\" }]\n" +
        "          accessPoints: [\n" +
        "            { accessPointType: \"test1\", description: \"access point desc\" }\n" +
        "          ]\n" +
        "        }\n" +
        "        {\n" +
        "          surfaceType: \"test2\"\n" +
        "          features: [{ featureType: \"feature2\", description: \"description\" }]\n" +
        "        }\n" +
        "        {\n" +
        "          surfaceType: \"test3\"\n" +
        "          accessPoints: [\n" +
        "            {\n" +
        "              accessPointType: \"AP-test-1\"\n" +
        "              description: \"access point testing\"\n" +
        "            }\n" +
        "          ]\n" +
        "        }\n" +
        "      ]\n" +
        "    }\n" +
        "  ) {\n" +
        "    currentVersion\n" +
        "    approvalStatus\n" +
        "    realEstateId\n" +
        "    locationName\n" +
        "    locationType\n" +
        "    locationSubType\n" +
        "    locationStatus\n" +
        "    locationMode\n" +
        "    propertyName\n" +
        "    addressLine1\n" +
        "    addressLine2\n" +
        "    preciselyStreetDataType\n" +
        "    equipmentType\n" +
        "    city\n" +
        "    state\n" +
        "    country\n" +
        "    zipCode\n" +
        "    createdBy {\n" +
        "      sourceId\n" +
        "      sourceType\n" +
        "    }\n" +
        "    createdDate\n" +
        "    updatedBy {\n" +
        "      sourceId\n" +
        "      sourceType\n" +
        "    }\n" +
        "    updatedDate\n" +
        "    surfaces {\n" +
        "      surfaceId\n" +
        "      surfaceType\n" +
        "      features {\n" +
        "        featureId\n" +
        "        featureType\n" +
        "      }\n" +
        "      accessPoints {\n" +
        "        accessPointId\n" +
        "        accessPointType\n" +
        "      }\n" +
        "    }\n" +
        "  }\n" +
        "}";

    let headers = {
        'Authorization': `${re_Admin_accessToken}`,
        "Content-Type": "application/json",
        "Content-Length": "35305",
        "Host": "middleware-qa.co.reefplatform.com",
        "Organization": "1",
    };

    //pagination - sort by real estate id asc request
    var resPagination = http.post(graphqlendpoint,
        JSON.stringify({query: queryPagination}),
        {headers: headers});
    myTrendSort.add(resPagination.timings.sending + resPagination.timings.receiving);
    const resultPagination = k6.check(resPagination, {
        "Query pagination for location status was 200": r => r.status === 200
    });
    errorRate.add(!resultPagination);
    let bodyPagination = JSON.parse(resPagination.body);

    console.log(bodyPagination);

    let totalsizePagination = bodyPagination.data.locations.totalSize;
    k6.check(resPagination, {
        'Total size of sorted locations is valid': totalsizePagination >= 1,
    });
    sleep(0.3);

    //search by location name request
    var resSearchByLocationName = http.post(graphqlendpoint,
        JSON.stringify({query: querySearchByLocationName}),
        {headers: headers});
    myTrendSearch.add(resSearchByLocationName.timings.sending + resSearchByLocationName.timings.receiving);

    console.log(resSearchByLocationName);

    const resultSearchByLocationName = k6.check(resSearchByLocationName, {
        "Query search for location by name status was 200": r => r.status === 200
    });
    errorRate.add(!resultSearchByLocationName);
    let bodyresultSearchByLocationName = JSON.parse(resSearchByLocationName.body);
    let totalsizeSearchByLocationName = bodyresultSearchByLocationName.data.locations.totalSize;
    k6.check(resSearchByLocationName, {
        'Total size of found locations is valid': totalsizeSearchByLocationName >= 1,
    });
    sleep(0.3);

    //add new location request
    var resAddNewLocation = http.post(graphqlendpoint,
        JSON.stringify({query: queryAddNewLocation}),
        {headers: headers});

    console.log(resAddNewLocation);

    myTrendAddNewLocation.add(resAddNewLocation.timings.sending + resAddNewLocation.timings.receiving);
    const resultAddNewLocation = k6.check(resAddNewLocation, {
        "Query add new location was 200": r => r.status === 200
    });
    errorRate.add(!resultAddNewLocation);
    let bodyresultAddnewLocation = JSON.parse(resAddNewLocation.body);
    let totalsizebodyresultrealEstateId = bodyresultAddnewLocation.data.addLocation.realEstateId;
    let totalsizebodyresultrealEstateLocationName = bodyresultAddnewLocation.data.addLocation.locationName;
    console.log('Added new location with real estate ID '+ totalsizebodyresultrealEstateId);
    k6.check(resAddNewLocation, {
        'Added new location with location name ': totalsizebodyresultrealEstateLocationName == locationName,
    });
    sleep(0.3);

    //get location by ID request
    var resGetLocationByID = http.post(graphqlendpoint,
        JSON.stringify({query: querySearchByLocationID}),
        {headers: headers});

    console.log(resGetLocationByID);

    console.log(JSON.stringify(resGetLocationByID.body))
    myTrendGetLocationByID.add(resGetLocationByID.timings.sending + resGetLocationByID.timings.receiving);
    const resultGetLocationById = k6.check(resGetLocationByID, {
        "Query search for location by real estate ID status was 200": r => r.status === 200
    });
    errorRate.add(!resultGetLocationById);
    let bodyresultGetLocationById = JSON.parse(resGetLocationByID.body);
    let responserealEstateId = bodyresultGetLocationById.data.getLocation.realEstateId;
    k6.check(resGetLocationByID, {
        'Location by requred real estate ID is valid': responserealEstateId == realEstateId,
    });
    sleep(0.3);

    const after = new Date().getTime();
    const diff = (after - before) / 1000;
    const remainder = T - diff;
    k6.check(remainder, {'Reached request rate': remainder > 0});
    if (remainder > 0) {
        sleep(remainder);
    } else {
        console.warn(
            `Timer exhausted! The execution time of the test took longer than ${T} seconds`
        );
    }
}
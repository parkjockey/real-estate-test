package com.reeftechnology.realestate.rest.scripts;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.Locations;
import com.reeftechnology.realestate.rest.tests.TestBase;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public class PopulateLocations extends TestBase {

    @DataProvider
    public Iterator<Object[]> validLocationsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations-rest.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Locations> locations = gson.fromJson(json,
                    new TypeToken<List<Locations>>() {
                    }.getType());
            return locations.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    @Test(dataProvider = "validLocationsFromJson", invocationCount = 100)
    public void testCreateLocation(Locations createdLocation) throws IOException {
        testrailID = 9240;
        Locations newLocation = new Locations()
                .withLocationName("Populate_" + createdLocation.getLocationName())
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus());
        String locationID = createLocationLong(newLocation);
        System.out.println("Location with real estate id: " + locationID + " exist");
    }

    private String createLocationLong(Locations newLocation) throws IOException {
        String json = getExecutor().execute(Request
                .Post(app.endPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader)
                .bodyString("{\n" +
                                "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                                "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                                "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                                "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                                "  \"state\": \"" + newLocation.getState() + "\",\n" +
                                "  \"locationType\": \"" + newLocation.getPropertyType() + "\",\n" +
                                "  \"locationSubType\": \"" + newLocation.getLocationSubType() + "\",\n" +
                                "  \"locationMode\": \"" + newLocation.getLocationMode() + "\",\n" +
                                "  \"propertyName\": \"" + newLocation.getPropertyName() + "\",\n" +
                                "  \"addressLine2\": \"" + newLocation.getAddressLine2() + "\",\n" +
                                "  \"locationStatus\": \"" + newLocation.getLocationStatus() + "\",\n" +
                                "  \"latitude\": \"" + newLocation.getLatitude() + "\",\n" +
                                "  \"longitude\": \"" + newLocation.getLongitude() + "\",\n" +
                                "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                                "}"
                        , ContentType.APPLICATION_JSON))
                .returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        System.out.println(parsed);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }
}

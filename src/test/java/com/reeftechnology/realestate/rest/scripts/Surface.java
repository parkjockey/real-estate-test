package com.reeftechnology.realestate.rest.scripts;

import java.util.List;

public class Surface {

    public String surfaceId;
    public String surfaceType;
    public String surfaceMode;
    public String description;
    public List<Space> spaces;
    public List<AccessPoint> accessPoints;
    public List<Feature> features;

    public Surface withSurfaceId(String surfaceId) {
        this.surfaceId = surfaceId;
        return this;
    }

    public Surface withSurfaceType(String surfaceType) {
        this.surfaceType = surfaceType;
        return this;
    }

    public Surface withSurfaceMode(String surfaceMode) {
        this.surfaceMode = surfaceMode;
        return this;
    }

    public Surface withDescription(String description) {
        this.description = description;
        return this;
    }

    public Surface withSpaces(List<Space> spaces) {
        this.spaces = spaces;
        return this;
    }

    public Surface withAccessPoints(List<AccessPoint> accessPoints) {
        this.accessPoints = accessPoints;
        return this;
    }

    public Surface withFeatures(List<Feature> features) {
        this.features = features;
        return this;
    }
}

class AccessPoint {

    public String accessPointId;
    public String accessPointType;
    public String description;

    public AccessPoint withAccessPointId(String accessPointId) {
        this.accessPointId = accessPointId;
        return this;
    }

    public AccessPoint withAccessPointType(String accessPointType) {
        this.accessPointType = accessPointType;
        return this;
    }

    public AccessPoint withDescription(String description) {
        this.description = description;
        return this;
    }

}

class Feature {

    public String featureId;
    public String featureType;
    public String description;

    public Feature withFeatureId(String featureId) {
        this.featureId = featureId;
        return this;
    }

    public Feature withFeatureType(String featureType) {
        this.featureType = featureType;
        return this;
    }

    public Feature withDescription(String description) {
        this.description = description;
        return this;
    }

}

class Space {

    public String spaceType;
    public String spaceMode;
    public int quantity;

    public Space withSpaceType(String spaceType) {
        this.spaceType = spaceType;
        return this;
    }

    public Space withSpaceMode(String spaceMode) {
        this.spaceMode = spaceMode;
        return this;
    }

    public Space withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

}
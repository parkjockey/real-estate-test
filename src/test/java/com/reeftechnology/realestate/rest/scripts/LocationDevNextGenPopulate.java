package com.reeftechnology.realestate.rest.scripts;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.Locations;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;

public class LocationDevNextGenPopulate {

    //    private String endPointURI = "http://api-dev.re.reefplatform.com/location/v1/locations";
    private String endPointURI = "https://realestate-backend-dev.api.re.reefplatform.com/location/v1/locations";
    String authorizationHeader = "eyJraWQiOiJlcHVxckZtVERPd0ZvcTFqUktGcUthajVRbkxEQkxrdDZkQUFtYURpUjVFPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI1MjhiZWEyOC05ZWQ3LTRkNjQtYTRlZC1hNjZmOTViOTY5NDkiLCJjb2duaXRvOmdyb3VwcyI6WyJ1cy1lYXN0LTJfMXVQbTBnb3BuX01pY3Jvc29mdCJdLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIG9wZW5pZCBwcm9maWxlIGVtYWlsIiwiYXV0aF90aW1lIjoxNjAwNjg4MDc3LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl8xdVBtMGdvcG4iLCJleHAiOjE2MDA2OTE2NzcsImlhdCI6MTYwMDY4ODA3NywidmVyc2lvbiI6MiwianRpIjoiNjY1ZWFhNDAtODQyMC00NTYwLWFmYzUtYzQ0YTM4ODIxZjU1IiwiY2xpZW50X2lkIjoiNWFydmtxanRiNXNnMTZnMWtqbjU1aWp1cDUiLCJ1c2VybmFtZSI6Im1pY3Jvc29mdF9xcTl0eG40bm5zaGh1c3ZlajF1eXZ6NGpuYnM5c2hkcnpjc3puYXN0dXBhIn0.u8a5BYW-S8pYIjKQoIoGrmU-A7NNWmWOJdP1ucCCKvmSz4gxRGB4NotcOzK-QpammIDMKklePmsKGF2a8UTCuEp6SaDfsHC1DgEguhFhn0gKnHsQozEOvTrVg1lZMjzF1rk9-i_znw1dzg390JYltPZAcW6kZITys2pw82OugxdOpLPWnt7VXfLBZzN6hwU_EPtcDc7iN51q7uz_2344IRpZBu8teNUbQl3xCR6ATtZvAY5--Ycyjh6QYJVKEIgdPPM9rDhDG_RQeI17BMcaIMXXzwN2iz0fhY9eUfjHTZ_gUkew4Ex8yxyTde9KNNRHs9GiQ1ONjj0UgEWOE956Aw";

    @DataProvider
    public Iterator<Object[]> validLocationsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations-stage-5000.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Locations> locations = gson.fromJson(json,
                    new TypeToken<List<Locations>>() {
                    }.getType());
            return locations.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    //login credentials added to http request
    @BeforeClass
    private Executor getExecutor() {
        return Executor.newInstance().auth("", "");
    }

    @Test(dataProvider = "validLocationsFromJson", invocationCount = 200)
    public void taskPopulateLocations(Locations createdLocation) throws IOException {
        Locations newLocation = new Locations()
                .withLocationName(createdLocation.getLocationName())
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus());
        String locationID = createLocationWithRequiredAndOptionalData(newLocation);
        System.out.println("Location with real estate id: " + locationID + " exist");

        Locations expectedLocation = getLocationinfoById(locationID);
        assertThat(expectedLocation, equalTo(newLocation));
    }

    @Test(enabled = false)
    public void taskPopulateLocation(Locations createdLocation) throws IOException {
        Set<Locations> oldLocations = getLocations();
        Locations newLocation = new Locations()
                .withLocationName(createdLocation.getLocationName())
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLocationStatus(createdLocation.getLocationStatus())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withSurfaces(createdLocation.getSurfaces())
                .withExternalIds(createdLocation.getExternalIds());

        String locationID = createLocationNew(newLocation);
        Set<Locations> newLocations = getLocations();
        oldLocations.add(newLocation.withRealEstateId(locationID));
        assertEquals(newLocations, oldLocations);
    }

    private Set<Locations> getLocations() throws IOException {
        String json = getExecutor().execute(Request
                .Get(endPointURI + "?pageSize=1000&sortBy=desc%3ArealEstateId")
                .addHeader("Authorization", authorizationHeader))
                .returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        JsonElement locations = parsed.getAsJsonObject().get("locations");
        return new Gson().fromJson(locations, new TypeToken<Set<Locations>>() {
        }.getType());
    }

    private String createLocationNew(Locations newLocation) throws IOException {
        String json = null;
        String sampleBodyLocation = "{ \"locationName\": \"" + newLocation.getLocationName() + "\"," +
                " \"locationType\": \"" + newLocation.getPropertyType() + "\"," +
                " \"locationSubType\": \"" + newLocation.getLocationSubType() + "\"," +
                " \"locationMode\": \"" + newLocation.getLocationMode() + "\"," +
                " \"propertyName\": \"" + newLocation.getPropertyName() + "\"," +
                " \"addressLine1\": \"" + newLocation.getAddressLine1() + "\"," +
                " \"addressLine2\": \"" + newLocation.getAddressLine2() + "\"," +
                " \"city\": \"" + newLocation.getCity() + "\"," +
                " \"state\": \"" + newLocation.getState() + "\"," +
                " \"country\": \"" + newLocation.getCountry() + "\"," +
                " \"zipCode\": \"" + newLocation.getZipCode() + "\"," +
                " \"locationStatus\": \"" + newLocation.getLocationStatus() + "\"," +
                " \"latitude\": " + newLocation.getLatitude() + "," +
                " \"longitude\": " + newLocation.getLongitude();


        json = getExecutor().execute(Request
                .Post(endPointURI)
                .addHeader("Authorization", authorizationHeader)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .bodyString(sampleBodyLocation + "," +
                                " \"surfaces\":" +
                                " [ { \"surfaceType\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1).surfaceType + "\"," +
                                " \"surfaceMode\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1).surfaceMode + "\"," +
                                " \"description\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1).description + "\"," +
                                " \"spaces\":" +
                                " [ { \"spaceType\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                .spaces.get(newLocation.getSurfaces().size() - 1).spaceType + "\"," +
                                " \"spaceMode\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                .spaces.get(newLocation.getSurfaces().size() - 1).spaceMode + "\"," +
                                " \"quantity\": " + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                .spaces.get(newLocation.getSurfaces().size() - 1).quantity + " } ]," +
                                " \"accessPoints\":" +
                                " [ { \"accessPointType\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                .accessPoints.get(newLocation.getSurfaces().size() - 1).accessPointType + "\"," +
                                " \"description\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                .accessPoints.get(newLocation.getSurfaces().size() - 1).description + "\" } ]," +
                                " \"features\":" +
                                " [ { \"featureType\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                .features.get(newLocation.getSurfaces().size() - 1).featureType + "\"," +
                                " \"description\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                .features.get(newLocation.getSurfaces().size() - 1).description + "\" } ] } ]," +
                                " \"externalIds\":" +
                                " [ { \"externalIdType\": \"" + newLocation.getExternalIds().get(newLocation.getExternalIds().size() - 1)
                                .getExternalIdType() + "\"," +
                                " \"value\": \"" + newLocation.getExternalIds().get(newLocation.getExternalIds().size() - 1)
                                .getExternalIdValue() + "\" } ] }",
                        ContentType.APPLICATION_JSON)).returnContent().asString();


        JsonElement parsed = new JsonParser().parse(json);
        System.out.println(parsed);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }

    private Locations getLocationinfoById(String locationID) throws IOException {
        String json = getExecutor().execute(Request
                .Get(endPointURI + "/" + locationID + "?displayMode=EDIT")
                .addHeader("Authorization", authorizationHeader))
                .returnContent().asString();
        JsonObject locationJson = new JsonParser().parse(json).getAsJsonObject();

        return new Locations().withLocationName(locationJson.get("locationName").getAsString())
                .withLocationPropertyType(locationJson.get("locationType").getAsString())
                .withLocationSubType(locationJson.get("locationSubType").getAsString())
                .withLocationMode(locationJson.get("locationMode").getAsString())
                .withPropertyName(locationJson.get("propertyName").getAsString())
                .withAddress1(locationJson.get("addressLine1").getAsString())
                .withAddress2(locationJson.get("addressLine2").getAsString())
                .withCity(locationJson.get("city").getAsString())
                .withState(locationJson.get("state").getAsString())
                .withCountry(locationJson.get("country").getAsString())
                .withZipCode(locationJson.get("zipCode").getAsString())
                .withLatitude(locationJson.get("latitude").getAsDouble())
                .withLongitude(locationJson.get("longitude").getAsDouble())
                .withLocationStatus(locationJson.get("locationStatus").getAsString());
    }

    private String createLocationWithRequiredAndOptionalData(Locations newLocation) throws IOException {
        String json = getExecutor().execute(Request
                .Post(endPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .addHeader("Authorization", authorizationHeader)
                .bodyString("{\n" +
                                "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                                "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                                "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                                "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                                "  \"state\": \"" + newLocation.getState() + "\",\n" +
                                "  \"locationType\": \"" + newLocation.getPropertyType() + "\",\n" +
                                "  \"locationSubType\": \"" + newLocation.getLocationSubType() + "\",\n" +
                                "  \"locationMode\": \"" + newLocation.getLocationMode() + "\",\n" +
                                "  \"propertyName\": \"" + newLocation.getPropertyName() + "\",\n" +
                                "  \"addressLine2\": \"" + newLocation.getAddressLine2() + "\",\n" +
                                "  \"locationStatus\": \"" + newLocation.getLocationStatus() + "\",\n" +
                                "  \"latitude\": \"" + newLocation.getLatitude() + "\",\n" +
                                "  \"longitude\": \"" + newLocation.getLongitude() + "\",\n" +
                                "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                                "}"
                        , ContentType.APPLICATION_JSON))
                .returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }
}

package com.reeftechnology.realestate.rest.scripts;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.ExternalId;
import com.reeftechnology.realestate.rest.model.Identifiers;
import com.reeftechnology.realestate.webui.model.SpaceData;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class IdGenerationPRODpopulate {

//    private String iDServiceEndPointURI = "https://id-service-dev2.co.reefplatform.com/id/v1/identifier";
    private String iDServiceEndPointURI = "https://id-service-prod2.co.reefplatform.com/id/v1/identifier/";
    private String xApiKey = "3Xvxq7xKIL5ElPv7oP6Gp5yNsfHcnP5C1hjGKgFc";


    @DataProvider
    public Iterator<Object[]> validIdsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/ids-prod.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Identifiers> ids = gson.fromJson(json,
                    new TypeToken<List<Identifiers>>() {
                    }.getType()); //List <Identifiers>.class
            return ids.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    @DataProvider
    public Iterator<Object[]> validIdsFromCsv() throws IOException {
        List<Object[]> newIdentifier = new ArrayList<Object[]>();
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/proddata/ids-prod-07282021.csv")))) {
            String line = reader.readLine();
            while (line != null) {
                String[] split = line.split(";");
                newIdentifier.add(new Object[]{new Identifiers()
                        .withLotid(split[0]).withType("RE")
                        .withState(split[1]).withCountry(split[2])});
                line = reader.readLine();
            }
            return newIdentifier.iterator();
        }
    }

    //login credentials added to http request
    @BeforeClass
    private Executor getExecutor() {
        return Executor.newInstance().auth("", "");
    }

    @Test(dataProvider = "validIdsFromCsv")
    public void testCreateNewidentifiers(Identifiers createdIdentifier) throws IOException {
        Identifiers newIdentifierData = new Identifiers()
                .withCountry(createdIdentifier.getCountry())
                .withLotid(createdIdentifier.getLotid())
                .withState(createdIdentifier.getState());
        JsonElement generatedIDData = createNewIdentifier(newIdentifierData);
        String externalIdValue = generatedIDData.getAsJsonObject().get("externalIds").getAsJsonArray().get(0)
                .getAsJsonObject().get("value").getAsString();
        assertThat(externalIdValue, equalTo(newIdentifierData.getLotid()));
    }

    private JsonElement createNewIdentifier(Identifiers newIdentifier) throws IOException {
        String json = getExecutor().execute(Request
                .Post(iDServiceEndPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .addHeader("x-api-key", xApiKey)
                .bodyString("{ \"country\": \"" + newIdentifier.getCountry() + "\"," +
                                " \"externalIds\": [ { \"externalIdType\": \"" + "Lot ID"
                                + "\", \"value\": \"" + newIdentifier.getLotid() + "\" } ]," +
                                " \"state\": \"" + newIdentifier.getState() + "\"," +
                                " \"type\": \"" + "RE" + "\"}",
                        ContentType.APPLICATION_JSON)).returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        System.out.println(parsed);
        return parsed;
    }

}

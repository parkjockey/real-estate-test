package com.reeftechnology.realestate.rest.scripts;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.Locations;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public class LocationStageNextGenPopulate {

    String endPointURI = "https://realestate-backend-stage.api.re.reefplatform.com/location/v1/locations";

    @DataProvider
    public Iterator<Object[]> validLocationsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations-stage.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Locations> locations = gson.fromJson(json,
                    new TypeToken<List<Locations>>() {
                    }.getType());
            return locations.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    //login credentials added to http request
    @BeforeClass
    private Executor getExecutor() {
        return Executor.newInstance().auth("", "");
    }

    @Test(dataProvider = "validLocationsFromJson")
    public void taskPopulateLocation(Locations createdLocation) throws IOException {
        Set<Locations> oldLocations = getLocations();
        Locations newLocation = new Locations()
                .withLocationName(createdLocation.getLocationName())
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLocationStatus(createdLocation.getLocationStatus())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withSurfaces(createdLocation.getSurfaces())
                .withExternalIds(createdLocation.getExternalIds());

        String locationID = createLocationNew(newLocation);
        Set<Locations> newLocations = getLocations();
        oldLocations.add(newLocation.withRealEstateId(locationID));
        assertEquals(newLocations, oldLocations);
    }

    private Set<Locations> getLocations() throws IOException {
        String json = getExecutor().execute(Request
                .Get(endPointURI + "?pageSize=1000&sortBy=desc%3ArealEstateId"))
                .returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        JsonElement locations = parsed.getAsJsonObject().get("locations");
        return new Gson().fromJson(locations, new TypeToken<Set<Locations>>() {
        }.getType());
    }

    private String createLocationNew(Locations newLocation) throws IOException {
        String json = null;
        String sampleBodyLocation = "{ \"locationName\": \"" + newLocation.getLocationName() + "\"," +
                " \"locationType\": \"" + newLocation.getPropertyType() + "\"," +
                " \"locationSubType\": \"" + newLocation.getLocationSubType() + "\"," +
                " \"locationMode\": \"" + newLocation.getLocationMode() + "\"," +
                " \"propertyName\": \"" + newLocation.getPropertyName() + "\"," +
                " \"addressLine1\": \"" + newLocation.getAddressLine1() + "\"," +
                " \"addressLine2\": \"" + newLocation.getAddressLine2() + "\"," +
                " \"city\": \"" + newLocation.getCity() + "\"," +
                " \"state\": \"" + newLocation.getState() + "\"," +
                " \"country\": \"" + newLocation.getCountry() + "\"," +
                " \"zipCode\": \"" + newLocation.getZipCode() + "\"," +
                " \"locationStatus\": \"" + newLocation.getLocationStatus() + "\"," +
                " \"latitude\": " + newLocation.getLatitude() + "," +
                " \"longitude\": " + newLocation.getLongitude();

        if (newLocation.getExternalIds() == null) {
            json = getExecutor().execute(Request
                    .Post(endPointURI)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("accept", "application/json")
                    .bodyString(sampleBodyLocation + "}"
                            , ContentType.APPLICATION_JSON))
                    .returnContent().asString();
        } else if (!(newLocation.getExternalIds() == null) & (newLocation.getSurfaces().size() == 1)) {
            json = getExecutor().execute(Request
                    .Post(endPointURI)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("accept", "application/json")
                    .bodyString(sampleBodyLocation + "," +
                                    " \"surfaces\":" +
                                    " [ { \"surfaceType\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1).surfaceType + "\"," +
                                    " \"surfaceMode\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1).surfaceMode + "\"," +
                                    " \"description\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1).description + "\"," +
                                    " \"spaces\":" +
                                    " [ { \"spaceType\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                    .spaces.get(newLocation.getSurfaces().size() - 1).spaceType + "\"," +
                                    " \"spaceMode\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                    .spaces.get(newLocation.getSurfaces().size() - 1).spaceMode + "\"," +
                                    " \"quantity\": " + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                    .spaces.get(newLocation.getSurfaces().size() - 1).quantity + " } ]," +
                                    " \"accessPoints\":" +
                                    " [ { \"accessPointType\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                    .accessPoints.get(newLocation.getSurfaces().size() - 1).accessPointType + "\"," +
                                    " \"description\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                    .accessPoints.get(newLocation.getSurfaces().size() - 1).description + "\" } ]," +
                                    " \"features\":" +
                                    " [ { \"featureType\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                    .features.get(newLocation.getSurfaces().size() - 1).featureType + "\"," +
                                    " \"description\": \"" + newLocation.getSurfaces().get(newLocation.getSurfaces().size() - 1)
                                    .features.get(newLocation.getSurfaces().size() - 1).description + "\" } ] } ]," +
                                    " \"externalIds\":" +
                                    " [ { \"externalIdType\": \"" + newLocation.getExternalIds().get(newLocation.getExternalIds().size() - 1)
                                    .getExternalIdType() + "\"," +
                                    " \"value\": \"" + newLocation.getExternalIds().get(newLocation.getExternalIds().size() - 1)
                                    .getExternalIdValue() + "\" } ] }",
                            ContentType.APPLICATION_JSON)).returnContent().asString();

        } else if (!(newLocation.getExternalIds() == null) & (newLocation.getSurfaces().size() > 1)) {
            json = getExecutor().execute(Request
                    .Post(endPointURI)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("accept", "application/json")
                    .bodyString(sampleBodyLocation + "," +
                                    " \"surfaces\": [ { \"surfaceType\": \"ASPHALT\", \"surfaceMode\": \"FLAT\", \"description\": \"Level 1\", \"spaces\": [ { \"spaceType\": \"VEHICLE\", \"spaceMode\": \"UNRESERVED\", \"quantity\": 200 } ], \"accessPoints\": [ { \"accessPointType\": \"ENTRANCE_EXIT\", \"description\": \"access Points description\" }, { \"accessPointType\": \"ENTRANCE_EXIT\", \"description\": \"access Points description\" }, { \"accessPointType\": \"ENTRANCE_EXIT\", \"description\": \"access Points description\" } ], \"features\": [ { \"featureType\": \"CELLULAR_SIGNAL\", \"description\": \"features description\" }, { \"featureType\": \"ELECTRICITY\", \"description\": \"features description\" } ] }, { \"surfaceType\": \"ASPHALT\", \"surfaceMode\": \"FLAT\", \"description\": \"Level 2\", \"spaces\": [ { \"spaceType\": \"VEHICLE\", \"spaceMode\": \"UNRESERVED\", \"quantity\": 200 } ], \"accessPoints\": [], \"features\": [ { \"featureType\": \"CELLULAR_SIGNAL\", \"description\": \"features description\" } ] } ], \"externalIds\": [ { \"externalIdType\": \"Location ID\", \"value\": \"WPNY000123\" } ] }",
                            ContentType.APPLICATION_JSON)).returnContent().asString();
        }
        JsonElement parsed = new JsonParser().parse(json);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }
}

package com.reeftechnology.realestate.rest.model;

import com.google.gson.annotations.Expose;

public class Campus {
    @Expose
    private String campusName;
    @Expose
    private String campusType;
    @Expose
    private String createdBy;
    @Expose
    private String createdDate;
    @Expose
    private String description;

    public String getCampusName() {
        return campusName;
    }

    public Campus withCampusName(String campusName) {
        this.campusName = campusName;
        return this;
    }

    public String getCampusType() {
        return campusType;
    }

    public Campus withCampusType(String campusType) {
        this.campusType = campusType;
        return this;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Campus withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public Campus withCreatedDate(String createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Campus withDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public String toString() {
        return "Campus{" +
                "campusName='" + campusName + '\'' +
                ", campusType='" + campusType + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Campus campus = (Campus) o;

        if (campusName != null ? !campusName.equals(campus.campusName) : campus.campusName != null) return false;
        if (campusType != null ? !campusType.equals(campus.campusType) : campus.campusType != null) return false;
        if (createdBy != null ? !createdBy.equals(campus.createdBy) : campus.createdBy != null) return false;
        if (createdDate != null ? !createdDate.equals(campus.createdDate) : campus.createdDate != null) return false;
        return description != null ? description.equals(campus.description) : campus.description == null;
    }

    @Override
    public int hashCode() {
        int result = campusName != null ? campusName.hashCode() : 0;
        result = 31 * result + (campusType != null ? campusType.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}

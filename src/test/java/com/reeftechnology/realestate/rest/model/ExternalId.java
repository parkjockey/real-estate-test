package com.reeftechnology.realestate.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExternalId {
    @SerializedName("externalIdType")
    @Expose
    private String externalIdType;
    @SerializedName("value")
    @Expose
    private String value;

    public String getExternalIdType() {
        return externalIdType;
    }

    public ExternalId withExternalIdType(String externalIdType) {
        this.externalIdType = externalIdType;
        return this;
    }

    public String getExternalIdValue() {
        return value;
    }

    public ExternalId withExternalIdValue(String value) {
        this.value = value;
        return this;
    }

    @Override
    public String toString() {
        return "ExternalId{" +
                "externalIdType='" + externalIdType + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExternalId that = (ExternalId) o;

        if (externalIdType != null ? !externalIdType.equals(that.externalIdType) : that.externalIdType != null)
            return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = externalIdType != null ? externalIdType.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}

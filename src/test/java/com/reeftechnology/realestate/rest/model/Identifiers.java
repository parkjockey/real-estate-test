package com.reeftechnology.realestate.rest.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Identifiers {

    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("externalIds")
    @Expose
    private List<ExternalId> externalIds;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("type")
    @Expose
    private String type;

    public String getLotid() {
        return lotid;
    }

    public Identifiers withLotid(String lotid) {
        this.lotid = lotid;
        return this;
    }

    @SerializedName("lotid")
    @Expose
    private String lotid;

    public String getCountry() {
        return country;
    }

    public Identifiers withCountry(String country) {
        this.country = country;
        return this;
    }

    public List<ExternalId> getExternalIds() {
        return externalIds;
    }

    public Identifiers withExternalIds(List<ExternalId> externalIds) {
        this.externalIds = externalIds;
        return this;
    }

    public String getState() {
        return state;
    }

    public Identifiers withState(String state) {
        this.state = state;
        return this;
    }

    public String getType() {
        return type;
    }

    public Identifiers withType(String type) {
        this.type = type;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Identifiers that = (Identifiers) o;

        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (externalIds != null ? !externalIds.equals(that.externalIds) : that.externalIds != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return lotid != null ? lotid.equals(that.lotid) : that.lotid == null;
    }

    @Override
    public int hashCode() {
        int result = country != null ? country.hashCode() : 0;
        result = 31 * result + (externalIds != null ? externalIds.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (lotid != null ? lotid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Identifiers{" +
                "country='" + country + '\'' +
                ", externalIds=" + externalIds +
                ", state='" + state + '\'' +
                ", type='" + type + '\'' +
                ", lotid='" + lotid + '\'' +
                '}';
    }

}


package com.reeftechnology.realestate.rest.model;

import com.google.gson.annotations.Expose;
import com.reeftechnology.realestate.rest.scripts.Surface;

import java.util.List;

public class Locations {
    private String realEstateId;
    @Expose //includes field for serialization/deserialization
    private String locationName;
    @Expose
    private String propertyType;
    @Expose
    private String locationSubType;
    @Expose
    private String locationMode;
    @Expose
    private String propertyName;
    @Expose
    private String addressLine1;
    @Expose
    private String addressLine2;
    @Expose
    private String city;
    @Expose
    private String state;
    @Expose
    private String country;
    @Expose
    private String zipCode;
    @Expose
    private String locationStatus;
    @Expose
    private double latitude;
    @Expose
    private double longitude;
    @Expose
    private List<Surface> surfaces;
    @Expose
    private List<ExternalId> externalIds;
    @Expose
    private String preciselyPbKey;
    @Expose
    private String preciselyStreetDataType;
    @Expose
    private String preciselyTzId;
    @Expose
    private String preciselyZipCode;
    @Expose
    private String region;

    public String getPreciselyZipCode() {
        return preciselyZipCode;
    }

    public Locations withPreciselyZipCode(String preciselyZipCode) {
        this.preciselyZipCode = preciselyZipCode;
        return this;
    }

    public String getPreciselyPbKey() {
        return preciselyPbKey;
    }

    public Locations withPreciselyPbKey(String preciselyPbKey) {
        this.preciselyPbKey = preciselyPbKey;
        return this;
    }

    public String getPreciselyStreetDataType() {
        return preciselyStreetDataType;
    }

    public Locations withPreciselyStreetDataType(String preciselyStreetDataType) {
        this.preciselyStreetDataType = preciselyStreetDataType;
        return this;
    }

    public String getPreciselyTzId() {
        return preciselyTzId;
    }

    public Locations withPreciselyTzId(String preciselyTzId) {
        this.preciselyTzId = preciselyTzId;
        return this;
    }


    public List<ExternalId> getExternalIds() {
        return externalIds;
    }

    public Locations withExternalIds(List<ExternalId> externalIds) {
        this.externalIds = externalIds;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public Locations withLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public Locations withLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public Locations withLocationPropertyType(String propertyType) {
        this.propertyType = propertyType;
        return this;
    }

    public List<Surface> getSurfaces() {
        return surfaces;
    }

    public Locations withSurfaces(List<Surface> surfaces) {
        this.surfaces = surfaces;
        return this;
    }

    public String getLocationSubType() {
        return locationSubType;
    }

    public Locations withLocationSubType(String locationSubType) {
        this.locationSubType = locationSubType;
        return this;
    }

    public String getLocationMode() {
        return locationMode;
    }

    public Locations withLocationMode(String locationMode) {
        this.locationMode = locationMode;
        return this;
    }

    public String getLocationStatus() {
        return locationStatus;
    }

    public Locations withLocationStatus(String locationStatus) {
        this.locationStatus = locationStatus;
        return this;
    }

    public String getRealEstateId() {
        return realEstateId;
    }

    public Locations withRealEstateId(String realEstateId) {
        this.realEstateId = realEstateId;
        return this;
    }

    public String getLocationName() {
        return locationName;
    }

    public Locations withLocationName(String locationName) {
        this.locationName = locationName;
        return this;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public Locations withAddress1(String address1) {
        this.addressLine1 = address1;
        return this;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public Locations withAddress2(String address2) {
        this.addressLine2 = address2;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Locations withCity(String city) {
        this.city = city;
        return this;
    }

    public String getZipCode() {
        return zipCode;
    }

    public Locations withZipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Locations withCountry(String country) {
        this.country = country;
        return this;
    }

    public String getState() {
        return state;
    }

    public Locations withState(String state) {
        this.state = state;
        return this;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public Locations withPropertyName(String propertyName) {
        this.propertyName = propertyName;
        return this;
    }

    @Override
    public String toString() {
        return "Locations{" +
                "realEstateId='" + realEstateId + '\'' +
                ", locationName='" + locationName + '\'' +
                ", propertyType='" + propertyType + '\'' +
                ", locationSubType='" + locationSubType + '\'' +
                ", locationMode='" + locationMode + '\'' +
                ", propertyName='" + propertyName + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", locationStatus='" + locationStatus + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", surfaces=" + surfaces +
                ", externalIds=" + externalIds +
                ", preciselyPbKey='" + preciselyPbKey + '\'' +
                ", preciselyStreetDataType='" + preciselyStreetDataType + '\'' +
                ", preciselyTzId='" + preciselyTzId + '\'' +
                ", preciselyZipCode='" + preciselyZipCode + '\'' +
                ", region='" + region + '\'' +
                '}';
    }

    public String getRegion() {
        return region;
    }

    public Locations withRegion(String region) {
        this.region = region;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Locations locations = (Locations) o;

        if (realEstateId != null ? !realEstateId.equals(locations.realEstateId) : locations.realEstateId != null)
            return false;
        if (locationName != null ? !locationName.equals(locations.locationName) : locations.locationName != null)
            return false;
        if (addressLine1 != null ? !addressLine1.equals(locations.addressLine1) : locations.addressLine1 != null)
            return false;
        if (addressLine2 != null ? !addressLine2.equals(locations.addressLine2) : locations.addressLine2 != null)
            return false;
        if (city != null ? !city.equals(locations.city) : locations.city != null) return false;
        if (state != null ? !state.equals(locations.state) : locations.state != null) return false;
        if (country != null ? !country.equals(locations.country) : locations.country != null) return false;
        return zipCode != null ? zipCode.equals(locations.zipCode) : locations.zipCode == null;
    }

    @Override
    public int hashCode() {
        int result = realEstateId != null ? realEstateId.hashCode() : 0;
        result = 31 * result + (locationName != null ? locationName.hashCode() : 0);
        result = 31 * result + (addressLine1 != null ? addressLine1.hashCode() : 0);
        result = 31 * result + (addressLine2 != null ? addressLine2.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        return result;
    }
}

package com.reeftechnology.realestate.rest.tests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.Locations;
import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ApproveLocationTests extends TestBase {

    @DataProvider
    public Iterator<Object[]> validLocationsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations-rest.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Locations> locations = gson.fromJson(json,
                    new TypeToken<List<Locations>>() {
                    }.getType());
            return locations.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }


    @Test(dataProvider = "validLocationsFromJson")
    public void testSendForApprovalLocationWithStructure(Locations createdLocation) throws IOException {
        testrailID = 23409;
        //arrange
        Set<Locations> oldLocations = readLocationsList();
        Locations existingLocation = new Locations()
                .withLocationName(createdLocation.getLocationName() + HelperBase.randInt(1, 100))
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus())
                .withPreciselyStreetDataType(createdLocation.getPreciselyStreetDataType())
                .withPreciselyTzId(createdLocation.getPreciselyTzId())
                .withPreciselyPbKey(createdLocation.getPreciselyPbKey())
                .withPreciselyZipCode(createdLocation.getPreciselyZipCode());
        String locationID = createLocationWithRequiredAndOptionalData(existingLocation);
        System.out.println("Location with real estate id: " + locationID + " exist");
        //verify that locations list updated with new location
        Set<Locations> newLocations = readLocationsList();
        oldLocations.add(existingLocation.withRealEstateId(locationID));
        assertThat(newLocations.size(), equalTo(oldLocations.size()));

        //add region to location
        existingLocation.withRegion("US Northeast");
        modifyLocationWithRequiredAndOptionalData(existingLocation, locationID, 1);

        //act
        String approvalStatus = updateApprovalStatusLocationByRealEstateID("READY_FOR_APPROVAL", locationID, 2);
        //verify that locations list is not changed after update
        assertThat(approvalStatus, equalTo("READY_FOR_APPROVAL"));

        //assert - verify that updated location info is available by real estate id
        Locations expectedLocation = getLocationinfoById(locationID);
        expectedLocation.withRealEstateId(locationID);
        assertThat(expectedLocation, equalTo(existingLocation));
    }

    @Test(dataProvider = "validLocationsFromJson")
    public void testApproveLocationWithStructure(Locations createdLocation) throws IOException {
        testrailID = 23410;
        //arrange
        Set<Locations> oldLocations = readLocationsList();
        Locations existingLocation = new Locations()
                .withLocationName(createdLocation.getLocationName() + HelperBase.randInt(1, 100))
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus())
                .withPreciselyStreetDataType(createdLocation.getPreciselyStreetDataType())
                .withPreciselyTzId(createdLocation.getPreciselyTzId())
                .withPreciselyPbKey(createdLocation.getPreciselyPbKey())
                .withPreciselyZipCode(createdLocation.getPreciselyZipCode());
        String locationID = createLocationWithRequiredAndOptionalData(existingLocation);
        System.out.println("Location with real estate id: " + locationID + " exist");
        //verify that locations list updated with new location
        Set<Locations> newLocations = readLocationsList();
        oldLocations.add(existingLocation.withRealEstateId(locationID));
        assertThat(newLocations.size(), equalTo(oldLocations.size()));

        //add region to location
        existingLocation.withRegion("US Northeast");
        modifyLocationWithRequiredAndOptionalData(existingLocation, locationID, 1);

        //act
        //send location for approval
        String approvalStatusReady = updateApprovalStatusLocationByRealEstateID("READY_FOR_APPROVAL", locationID, 2);
        assertThat(approvalStatusReady, equalTo("READY_FOR_APPROVAL"));
        //approve location
        String approvalStatusApproved = updateApprovalStatusLocationByRealEstateID("APPROVED", locationID, 3);
        //verify that locations list is not changed after update
        assertThat(approvalStatusApproved, equalTo("APPROVED"));

        //assert - verify that updated location info is available by real estate id
        Locations expectedLocation = getLocationinfoById(locationID);
        expectedLocation.withRealEstateId(locationID);
        assertThat(expectedLocation, equalTo(existingLocation));
    }

}

package com.reeftechnology.realestate.rest.tests;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.appmanager.ApplicationManager;
import com.reeftechnology.realestate.rest.model.Campus;
import com.reeftechnology.realestate.rest.model.ExternalId;
import com.reeftechnology.realestate.rest.model.Identifiers;
import com.reeftechnology.realestate.rest.model.Locations;
import com.reeftechnology.realestate.webui.appmanager.TestRailHelpers;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.TestNG;
import org.testng.annotations.*;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class TestBase {

    public String existingSystemID;
    public String modifiedSystemID;
    Logger logger = LoggerFactory.getLogger(TestBase.class);
    public int testrailID;
    protected static final ApplicationManager app = new ApplicationManager();
    protected static TestNG testNG = new TestNG();

    @BeforeSuite
    public void setUp() throws IOException, ParseException {
        app.init();
        TestRailHelpers.createTestRailInstance();
        TestRailHelpers.setProjectSuiteId("REEF Cloud - Real Estate", "Master");
        TestRailHelpers.createRun("REST API");
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        TestRailHelpers.closeRun();
    }

    @BeforeMethod
    public void logbackTestStart(Method method, Object[] p) {
        logger.info("Start test: " + method.getName() + " with parameters " + Arrays.asList(p));
    }

    @AfterMethod(alwaysRun = true)
    public void logbackTestStop(Method method, Object[] p) throws IOException {
        if (testrailID > 1) {
            try {
                addTestRailID(testrailID, p);
            } catch (NullPointerException e) {
                System.out.println("Test case ID is invalid: " + testrailID);
            }
        }
        logger.info("Stop test: " + method.getName());
    }

    @BeforeClass
    public static void beforeTestSuiteActions() throws ParseException {
    }

    @AfterClass
    public static void afterTestSuiteActions() {
    }

    public static void addTestRailID(int caseId, Object[] p) {
        TestRailHelpers.updateRun(caseId);
        TestRailHelpers.addResult("Test have been executed with parameters" + Arrays.asList(p), caseId);
        try {
            testNG.getStatus();
            if (testNG.getStatus() == 5) {
                TestRailHelpers.addStatusForCase(5, caseId);
            } else if (testNG.getStatus() == 0) {
                TestRailHelpers.addStatusForCase(1, caseId);
            } else {
                TestRailHelpers.addStatusForCase(0, caseId);
            }
        } catch (NullPointerException e) {
            System.out.println("For testrailID value with: " + caseId + " TestNG m_status is not defined");
            TestRailHelpers.addStatusForCase(1, caseId);
        }
    }

    //login credentials added to http request
    @BeforeClass
    protected Executor getExecutor() {
        return Executor.newInstance().auth("", "");
    }

    protected Set<Locations> readLocationsList() throws IOException {
        String json = null;
        try {
            json = getExecutor().execute(Request
                            .Get(app.endPointURI + "?pageSize=9000&sortBy=realEstateId")
                            .addHeader("Authorization", app.authorizationHeader)
                            .addHeader("organization", app.organizationHeader)
                            .addHeader("accept", "application/json"))
                    .returnContent().asString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonElement parsed = new JsonParser().parse(json);
        JsonElement locations = parsed.getAsJsonObject().get("locations");
        return new Gson().fromJson(locations, new TypeToken<Set<Locations>>() {
        }.getType());
    }

    protected String createLocationWithRequiredData(Locations newLocation) throws IOException {
        String json = null;
        try {
            json = getExecutor().execute(Request
                            .Post(app.endPointURI)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("accept", "application/json")
                            .addHeader("Authorization", app.authorizationHeader)
                            .addHeader("organization", app.organizationHeader)
                            .bodyString("{\n" +
                                    "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                                    "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                                    "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                                    "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                                    "  \"locationStatus\": \"" + newLocation.getLocationStatus() + "\",\n" +
                                    "  \"state\": \"" + newLocation.getState() + "\",\n" +
                                    "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                                    "}", ContentType.APPLICATION_JSON))
                    .returnContent().asString();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        }
        JsonElement parsed = new JsonParser().parse(json);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }

    protected String createLocationWithRequiredAndOptionalData(Locations createdLocation) throws IOException {
        String json = "";
        try {
            json = getExecutor().execute(Request
                            .Post(app.endPointURI)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("accept", "application/json")
                            .addHeader("Authorization", app.authorizationHeader)
                            .addHeader("organization", app.organizationHeader)
                            .bodyString("{ \"addressLine1\": \"" + createdLocation.getAddressLine1() + "\", " +
                                            "\"addressLine2\": \"" + createdLocation.getAddressLine2() + "\", " +
                                            "\"approvalStatus\": \"IN_PROGRESS\", " +
                                            "\"campuses\": [ { \"campusId\": \"CU-1002063\", \"campusName\": \"California State University\", \"campusType\": \"Landlord\", \"createdBy\": \"Billy Boy\", \"createdDate\": \"2021-11-11\", \"deletedBy\": \"Billy Boy\", \"deletedDate\": \"2022-11-11\", \"description\": \"California State University\", \"isDeleted\": false, \"numberOfLocations\": 1, \"updatedBy\": \"Billy Boy\", \"updatedDate\": \"2021-11-12\" } ], " +
                                            "\"city\": \"" + createdLocation.getCity() + "\", " +
                                            "\"country\": \"" + createdLocation.getCountry() + "\", " +
                                            "\"latitude\": " + createdLocation.getLatitude() + ", " +
                                            "\"locationName\": \"" + createdLocation.getLocationName() + "\", " +
                                            "\"locationStatus\": \"Active\", " +
                                            "\"longitude\": " + createdLocation.getLongitude() + ", " +
                                            "\"msa\": \"ABBOTSFORD - MISSION, BC\", " +
                                            "\"operatingCompanyCode\": \"IMP043\", " +
                                            "\"preciselyAddressLine1\": \"" + createdLocation.getAddressLine1() + "\", " +
                                            "\"preciselyAddressLine2\": \"" + createdLocation.getAddressLine2() + "\", " +
                                            "\"preciselyCity\": \"" + createdLocation.getCity() + "\", " +
                                            "\"preciselyCountry\": \"" + createdLocation.getCountry() + "\", " +
                                            "\"preciselyLatitude\": " + createdLocation.getLatitude() + ", " +
                                            "\"preciselyLongitude\": " + createdLocation.getLongitude() + ", " +
                                            "\"preciselyPbKey\": 42342, " +
                                            "\"preciselyState\": \"" + createdLocation.getState() + "\", " +
                                            "\"preciselyStreetDataType\": 1, " +
                                            "\"preciselyTimeZone\": \"America/Los_Angeles\", " +
                                            "\"preciselyTzId\": " + createdLocation.getPreciselyTzId() + ", " +
                                            "\"preciselyZipCode\": " + createdLocation.getZipCode() + ", " +
                                            "\"propertyName\": \"Legal name of Location.\", " +
                                            "\"propertyType\": \"Commercial\", " +
                                            "\"region\": \"US Western\", " +
                                            "\"state\": \"" + createdLocation.getState() + "\", " +
                                            "\"structures\": [ { \"address\": { \"addressLine1\": \"1600 Pennsylvania Avenue\", \"addressLine2\": \"Opp. Post Office\", \"city\": \"San Jose\", \"country\": \"US\", \"state\": \"CA\", \"zipCode\": 95126 }, \"contacts\": [ { \"contactEmail\": \"test@domain.com\", \"contactName\": \"John Doe\", \"contactPhone\": \"+12345678989\", \"contactType\": \"City Manager\", \"notes\": \"Sample notes\" } ], \"description\": \"string\", \"equipmentType\": \"String\", \"externalIds\": [ { \"externalIdType\": \"Location ID\", \"value\": 345 } ], \"restrictions\": [ ], \"services\": [ ], \"structureMode\": \"GATED\", \"structureName\": \"Structure 1\", \"structureSubType\": \"ABOVE_GROUND_GARAGE\", \"structureType\": \"PARKING\", \"surfaces\": [ { \"accessPoints\": [ { \"accessPointType\": \"Vehicle - entrance,Vehicle - exit,Vehicle - Entrance/Exit\", \"description\": \"SouthSide entrance of the Academic block\", \"latitude\": -54.6, \"longitude\": -34.5 } ], \"description\": \"Basement of the Academic block\", \"features\": [ { \"description\": \"Fully lit\", \"featureType\": \"Natural Gas\", \"latitude\": -54.6, \"longitude\": -34.5 } ], \"latitude\": -54.6, \"longitude\": -34.5, \"spaces\": [ { \"quantity\": 100, \"spaceMode\": \"Reserved\", \"spaceType\": \"Vehicle\" } ], \"surfaceMode\": \"Flat\", \"surfaceType\": \"Post-tension concrete\" } ] } ], " +
                                            "\"timeStamp\": \"2020-07-30T17:43:48.976Z\", " +
                                            "\"zipCode\": " + createdLocation.getZipCode() + "}"
                                    , ContentType.APPLICATION_JSON))
                    .returnContent().asString();
            JsonElement parsed = new JsonParser().parse(json);
            System.out.println(parsed);
        } catch (NullPointerException e) {
            e.getLocalizedMessage();
        }
        JsonElement parsed = new JsonParser().parse(json);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }

    protected String modifyLocationWithRequiredData(Locations newLocation, String realEstateId, String baseCommitIdString) throws IOException {
        String json = null;
        try {
            json = getExecutor().execute(Request
                            .Put(app.endPointURI)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("accept", "application/json")
                            .addHeader("Authorization", app.authorizationHeader)
                            .addHeader("organization", app.organizationHeader)
                            .bodyString("{\n" +
                                    "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                                    "  \"baseCommitId\": \"" + baseCommitIdString + "\",\n" +
                                    "  \"realEstateId\": \"" + realEstateId + "\",\n" +
                                    "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                                    "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                                    "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                                    "  \"locationStatus\": \"" + newLocation.getLocationStatus() + "\",\n" +
                                    "  \"state\": \"" + newLocation.getState() + "\",\n" +
                                    "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                                    "}", ContentType.APPLICATION_JSON))
                    .returnContent().asString();
        } catch (NullPointerException e) {
            e.getLocalizedMessage();
        }
        System.out.println(getExecutor());
        JsonElement parsed = new JsonParser().parse(json);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }

    protected String modifyLocationWithRequiredAndOptionalData(Locations location, String realEstateId, int baseCommitIdString) throws IOException {
        // Creating a File instance
        /*        String jsonBody = new String((Files.readAllBytes(Paths.get("src/test/resources/testdata/payload/locations-rest-put.json"))));
        JSONObject jsonObject = new JSONObject(jsonBody);
        jsonObject.remove("realEstateId");
        jsonObject.put("realEstateId", realEstateId);
        System.out.print(jsonObject);*/
        String json = "";
        try {
            json = getExecutor().execute(Request
                            .Put(app.endPointURI)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("accept", "application/json")
                            .addHeader("Authorization", app.authorizationHeader)
                            .addHeader("organization", app.organizationHeader)
                            .bodyString("{\"country\":\"" + location.getCountry() + "\"," +
                                            "\"preciselyCity\":\"" + location.getCity() + "\",\"zipCode\":" + location.getZipCode() + "," +
                                            "\"city\":\"" + location.getCity() + "\",\"latitude\":47.151659,\"" +
                                            "preciselyPbKey\":42342,\"preciselyLatitude\":41.857464," +
                                            "\"locationStatus\":\"Active\"," +
                                            "\"propertyType\":\"" + location.getPropertyType() + "\"," +
                                            "\"addressLine1\":\"" + location.getAddressLine1() + "\",\"addressLine2\":\"" + location.getAddressLine2() + "\"," +
                                            "\"state\":\"CA\",\"preciselyStreetDataType\":1,\"preciselyZipCode\":95126,\"longitude\":-122.34161," +
                                            "\"realEstateId\":\"" + realEstateId + "\",\"preciselyTimeZone\":\"America/Los_Angeles\"," +
                                            "\"locationName\":\"" + location.getLocationName() + "\",\"operatingCompanyCode\":\"IMP043\"," +
                                            "\"structures\":[{\"structureMode\":\"GATED\",\"structureSubType\":\"ABOVE_GROUND_GARAGE\"," +
                                            "\"address\":{\"country\":\"US\",\"zipCode\":95126,\"city\":\"San Jose\",\"addressLine1\":\"1600 Pennsylvania Avenue\",\"addressLine2\":\"Opp. Post Office\",\"state\":\"CA\"}," +
                                            "\"structureName\":\"Structure 1\",\"externalIds\":[{\"value\":345,\"externalIdType\":\"Location ID\"}],\"description\":\"string\",\"restrictions\":[],\"structureId\":\"ST-1\",\"services\":[],\"equipmentType\":\"Flash\"," +
                                            "\"surfaces\":[{\"features\":[{\"latitude\":-54.6,\"featureType\":\"Electricity\",\"description\":\"Fully lit\",\"featureId\":\"string\",\"longitude\":-34.5}],\"rangeSpaces\":[{\"rangeStart\":1,\"spaceType\":\"Vehicle\",\"rangeName\":\"Range One\",\"description\":\"This is the first range\",\"rangeEnd\":15}],\"surfaceType\":\"Post-tension concrete\",\"latitude\":-54.6,\"spaces\":[{\"quantity\":1,\"spaceType\":\"Vehicle\",\"spaceMode\":\"Reserved\"}],\"description\":\"Basement of the Academic block\",\"surfaceId\":\"string\",\"accessPoints\":[{\"accessPointType\":\"Vehicle - Entrance/Exit\",\"accessPointId\":\"A1\",\"latitude\":-54.6,\"description\":\"SouthSide entrance of the Academic block\",\"longitude\":-34.5}],\"surfaceMode\":\"Flat\",\"longitude\":-34.5}],\"structureType\":\"PARKING\",\"contacts\":[{\"notes\":\"Sample notes\",\"contactEmail\":\"test@domain.com\",\"contactName\":\"John Doe\",\"contactType\":\"City Manager\",\"contactPhone\":\"+12345678989\"}]}],\"preciselyAddressLine2\":\"Opp. Post Office\",\"preciselyCountry\":\"US\",\"preciselyAddressLine1\":\"1600 Pennsylvania Avenue\",\"timeStamp\":\"2020-07-30T17:43:48.976Z\",\"preciselyTzId\":276," +
                                            "\"propertyName\":\"" + location.getPropertyName() + "\",\"preciselyState\":\"CA\"," +
                                            "\"baseCommitId\":" + baseCommitIdString + ",\"msa\":\"NEW YORK, NY\",\"preciselyLongitude\":-88.3278267,\"region\":\"US Northeast\",\"campuses\":[{\"createdDate\":\"2021-11-11\",\"updatedBy\":\"Billy Boy\",\"isDeleted\":false,\"campusName\":\"California State University\",\"createdBy\":\"Billy Boy\",\"deletedDate\":\"2022-11-11\",\"campusId\":\"CU-1002063\",\"description\":\"California State University\",\"campusType\":\"Landlord\"," +
                                            "\"numberOfLocations\":1,\"updatedDate\":\"2021-11-12\",\"deletedBy\":\"Billy Boy\"}]}",
                                    ContentType.APPLICATION_JSON))
                    .returnContent().asString();
        } catch (NullPointerException e) {
            e.getLocalizedMessage();
        }
        System.out.println(getExecutor());
        JsonElement parsed = new JsonParser().parse(json);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }


    protected Locations getLocationinfoById(String locationID) throws IOException {
        String json = getExecutor().execute(Request
                        .Get(app.endPointURI + "/" + locationID + "?displayMode=EDIT")
                        .addHeader("Authorization", app.authorizationHeader)
                        .addHeader("organization", app.organizationHeader))
                .returnContent().asString();
        JsonObject locationJson = new JsonParser().parse(json).getAsJsonObject();

        if (locationJson.has("region")) {
            return new Locations().withLocationName(locationJson.get("locationName").getAsString())
                    .withPropertyName(locationJson.get("propertyName").getAsString())
                    .withLocationPropertyType(locationJson.get("propertyType").getAsString())
                    .withAddress1(locationJson.get("addressLine1").getAsString())
                    .withAddress2(locationJson.get("addressLine2").getAsString())
                    .withCity(locationJson.get("city").getAsString())
                    .withState(locationJson.get("state").getAsString())
                    .withCountry(locationJson.get("country").getAsString())
                    .withZipCode(locationJson.get("zipCode").getAsString())
                    .withLatitude(locationJson.get("latitude").getAsDouble())
                    .withLongitude(locationJson.get("longitude").getAsDouble())
                    .withLocationStatus(locationJson.get("locationStatus").getAsString())
                    .withPreciselyTzId(locationJson.get("preciselyTzId").getAsString())
                    .withPreciselyZipCode(locationJson.get("preciselyZipCode").getAsString())
//                .withPreciselyPbKey(locationJson.get("preciselyPbKey").getAsString())
                    .withRegion(locationJson.get("region").getAsString())
                    .withPreciselyPbKey("P00002T2YAT1")
                    .withPreciselyStreetDataType("MASTER LOCATION");
        } else {
            return new Locations().withLocationName(locationJson.get("locationName").getAsString())
                    .withPropertyName(locationJson.get("propertyName").getAsString())
                    .withLocationPropertyType(locationJson.get("propertyType").getAsString())
                    .withAddress1(locationJson.get("addressLine1").getAsString())
                    .withAddress2(locationJson.get("addressLine2").getAsString())
                    .withCity(locationJson.get("city").getAsString())
                    .withState(locationJson.get("state").getAsString())
                    .withCountry(locationJson.get("country").getAsString())
                    .withZipCode(locationJson.get("zipCode").getAsString())
                    .withLatitude(locationJson.get("latitude").getAsDouble())
                    .withLongitude(locationJson.get("longitude").getAsDouble())
                    .withLocationStatus(locationJson.get("locationStatus").getAsString())
                    .withPreciselyTzId(locationJson.get("preciselyTzId").getAsString())
                    .withPreciselyZipCode(locationJson.get("preciselyZipCode").getAsString())
                    .withPreciselyPbKey("P00002T2YAT1")
                    .withPreciselyStreetDataType("MASTER LOCATION");
        }
    }

    protected Locations getBriefLocationinfoById(String locationID) throws IOException {
        String json = getExecutor().execute(Request
                        .Get(app.endPointURI + "/" + locationID + "?displayMode=EDIT")
                        .addHeader("Authorization", app.authorizationHeader)
                        .addHeader("organization", app.organizationHeader))
                .returnContent().asString();
        JsonObject locationJson = new JsonParser().parse(json).getAsJsonObject();

        return new Locations()
                .withLocationName(locationJson.get("locationName").getAsString())
                .withAddress1(locationJson.get("addressLine1").getAsString())
                .withCity(locationJson.get("city").getAsString())
                .withState(locationJson.get("state").getAsString())
                .withCountry(locationJson.get("country").getAsString())
                .withZipCode(locationJson.get("zipCode").getAsString())
                .withLocationStatus(locationJson.get("locationStatus").getAsString());
    }

    protected Identifiers createNewIdentifierWithExternalID(Identifiers newIdentifier) throws IOException {
        String json = getExecutor().execute(Request
                .Post(app.iDServiceEndPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("x-api-key", app.idService_x_api_key)
                .addHeader("accept", "application/json")
                .bodyString("{ \"country\": \"" + newIdentifier.getCountry() + "\"," +
                                " \"externalIds\": [ { \"externalIdType\": \"" + newIdentifier.getExternalIds().get(0).getExternalIdType()
                                + "\", \"value\": \"" + newIdentifier.getExternalIds().get(0).getExternalIdValue() + "\" } ]," +
                                " \"state\": \"" + newIdentifier.getState() + "\"," +
                                " \"type\": \"" + newIdentifier.getType() + "\"}",
                        ContentType.APPLICATION_JSON)).returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        String[] parsedIDData = parsed.getAsJsonObject().get("id")
                .getAsString().split("-");
        existingSystemID = parsed.getAsJsonObject().get("id").getAsString();
        String externalIdType = parsed.getAsJsonObject().get("externalIds").getAsJsonArray().get(0)
                .getAsJsonObject().get("externalIdType").getAsString();
        String externalIdValue = parsed.getAsJsonObject().get("externalIds").getAsJsonArray().get(0)
                .getAsJsonObject().get("value").getAsString();
        List<ExternalId> externalIds = new ArrayList<>();
        externalIds.add(new ExternalId().withExternalIdType(externalIdType).withExternalIdValue(externalIdValue));
        if (!parsedIDData[3].isEmpty()) {
            return new Identifiers().withCountry(parsedIDData[0])
                    .withState(parsedIDData[1])
                    .withType(parsedIDData[2])
                    .withExternalIds(externalIds);
        } else return new Identifiers();
    }

    protected Identifiers createNewIdentifier(Identifiers newIdentifier) throws IOException {
        String json = getExecutor().execute(Request
                .Post(app.iDServiceEndPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .addHeader("x-api-key", app.idService_x_api_key)
                .bodyString("{ \"country\": \"" + newIdentifier.getCountry() + "\"," +
                                " \"externalIds\": []," +
                                " \"state\": \"" + newIdentifier.getState() + "\"," +
                                " \"type\": \"" + newIdentifier.getType() + "\"}",
                        ContentType.APPLICATION_JSON)).returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        String[] parsedIDData = parsed.getAsJsonObject().get("id")
                .getAsString().split("-");
        existingSystemID = parsed.getAsJsonObject().get("id").getAsString();
        if (!parsedIDData[3].isEmpty()) {
            return new Identifiers().withCountry(parsedIDData[0])
                    .withState(parsedIDData[1])
                    .withType(parsedIDData[2]);
        } else return new Identifiers();
    }

    protected int createNewIdentifierInvalidData(Identifiers newIdentifier) throws IOException {
        return getExecutor().execute(Request
                .Post(app.iDServiceEndPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("x-api-key", app.idService_x_api_key)
                .addHeader("accept", "application/json")
                .bodyString("{ \"country\": \"" + newIdentifier.getCountry() + "\"," +
                                " \"externalIds\": []," +
                                " \"state\": \"" + newIdentifier.getState() + "\"," +
                                " \"type\": \"" + newIdentifier.getType() + "\"}",
                        ContentType.APPLICATION_JSON)).returnResponse().getStatusLine().getStatusCode();
    }

    protected Identifiers modifyExistingIdentifier(Identifiers newIdentifier, String existingSystemID) throws IOException {
        String json = getExecutor().execute(Request
                .Put(app.iDServiceEndPointURI + "/" + existingSystemID)
                .addHeader("Content-Type", "application/json")
                .addHeader("x-api-key", app.idService_x_api_key)
                .addHeader("accept", "application/json")
                .bodyString("{ \"id\": \"" + existingSystemID + "\"," +
                                " \"externalIds\": [ { \"externalIdType\": \"" + newIdentifier.getExternalIds().get(0).getExternalIdType()
                                + "\", \"value\": \"" + newIdentifier.getExternalIds().get(0).getExternalIdValue() + "\" } ]}",
                        ContentType.APPLICATION_JSON)).returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        String[] parsedIDData = parsed.getAsJsonObject().get("id")
                .getAsString().split("-");
        modifiedSystemID = parsed.getAsJsonObject().get("id").getAsString();
        String externalIdType = parsed.getAsJsonObject().get("externalIds").getAsJsonArray().get(0)
                .getAsJsonObject().get("externalIdType").getAsString();
        String externalIdValue = parsed.getAsJsonObject().get("externalIds").getAsJsonArray().get(0)
                .getAsJsonObject().get("value").getAsString();
        List<ExternalId> externalIds = new ArrayList<>();
        externalIds.add(new ExternalId().withExternalIdType(externalIdType).withExternalIdValue(externalIdValue));
        if (!parsedIDData[3].isEmpty()) {
            return new Identifiers().withCountry(parsedIDData[0])
                    .withState(parsedIDData[1])
                    .withType(parsedIDData[2])
                    .withExternalIds(externalIds);
        } else return new Identifiers();
    }

    protected Identifiers getIdentifier(String existingSystemID) throws IOException {
        String json = getExecutor().execute(Request
                        .Get(app.iDServiceEndPointURI + "/" + existingSystemID)
                        .addHeader("x-api-key", app.idService_x_api_key)
                        .addHeader("accept", "application/json"))
                .returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        String[] parsedIDData = parsed.getAsJsonObject().get("id")
                .getAsString().split("-");
        existingSystemID = parsed.getAsJsonObject().get("id").getAsString();
        if (!parsedIDData[3].isEmpty()) {
            return new Identifiers().withCountry(parsedIDData[0])
                    .withState(parsedIDData[1])
                    .withType(parsedIDData[2]);
        } else return new Identifiers();
    }

    protected int getNonExistingIdentifier(String existingSystemID) throws IOException {
        return getExecutor().execute(Request
                        .Get(app.iDServiceEndPointURI + "/" + existingSystemID)
                        .addHeader("x-api-key", app.idService_x_api_key)
                        .addHeader("accept", "application/json"))
                .returnResponse().getStatusLine().getStatusCode();
    }

    protected Set<Campus> readCampusesList() {
        String json = null;
        try {
            json = getExecutor().execute(Request
                            .Get(app.campusEndPointURI + "?pageNumber=0&pageSize=1000")
                            .addHeader("Authorization", app.authorizationHeader)
                            .addHeader("organization", app.organizationHeader)
                            .addHeader("accept", "application/json"))
                    .returnContent().asString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonElement parsed = new JsonParser().parse(json);
        JsonElement locations = parsed.getAsJsonObject().get("content");
        return new Gson().fromJson(locations, new TypeToken<Set<Campus>>() {
        }.getType());
    }

    protected Set<Campus> getValidatesAdressList() throws IOException {
        String json = null;
        try {
            json = getExecutor().execute(Request
                            .Get(app.addressEndPointURI + "/validateAddress?" +
                                    "addressLine1=1740%20Cesar%20chavez%20street" +
                                    "&city=San%20Francisco" +
                                    "&country=US" +
                                    "&postalCode=94124" +
                                    "&stateProvince=CA")
                            .addHeader("accept", "application/json"))
                    .returnContent().asString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonElement parsed = new JsonParser().parse(json);
        JsonElement locations = parsed.getAsJsonObject().get("content");
        return new Gson().fromJson(locations, new TypeToken<Set<Campus>>() {
        }.getType());
    }

    protected String updateApprovalStatusLocationByRealEstateID(String approvalStatus, String realEstateId, int baseCommitId) throws IOException {
        String json = "{}";
        try {
            json = getExecutor().execute(Request
                    .Put(app.endPointURI + "/" + realEstateId + "/approval-status")
                    .addHeader("Content-Type", "application/json")
                    .addHeader("accept", "application/json")
                    .addHeader("Authorization", app.authorizationHeader)
                    .addHeader("organization", "0")
                    .bodyString("{ \"approvalStatus\": \"" + approvalStatus + "\", \"baseCommitId\": " + baseCommitId + "}"
                            , ContentType.APPLICATION_JSON))
                    .returnContent().asString();
        } catch (IOException e) {
            Thread thread = null;
            try {
                thread.sleep(500);
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
            e.getMessage();
            json = getExecutor().execute(Request
                    .Put(app.endPointURI + "/" + realEstateId + "/approval-status")
                    .addHeader("Content-Type", "application/json")
                    .addHeader("accept", "application/json")
                    .addHeader("Authorization", app.authorizationHeader)
                    .addHeader("organization", "0")
                    .bodyString("{ \"approvalStatus\": \"" + approvalStatus + "\", \"baseCommitId\": " + baseCommitId + "}"
                            , ContentType.APPLICATION_JSON))
                    .returnContent().asString();
        }
        JsonElement parsed = new JsonParser().parse(json);
        return parsed.getAsJsonObject().get("approvalStatus").getAsString();
    }

    protected int getNonExistingLocationinfoById(String locationID) throws IOException {
        String response = getExecutor().execute(Request
                .Get(app.endPointURI + "/" + locationID + "?displayMode=EDIT")
                .addHeader("accept", "application/json")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader))
                .returnResponse().toString();
        System.out.println(response);

        return getExecutor().execute(Request
                .Get(app.endPointURI + "/" + locationID + "?displayMode=EDIT")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader))
                .returnResponse().getStatusLine().getStatusCode();
    }
}
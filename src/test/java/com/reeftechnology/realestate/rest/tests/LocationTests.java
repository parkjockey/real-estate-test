package com.reeftechnology.realestate.rest.tests;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.Locations;
import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LocationTests extends TestBase {

    @DataProvider
    public Iterator<Object[]> validLocationsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations-rest.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Locations> locations = gson.fromJson(json,
                    new TypeToken<List<Locations>>() {
                    }.getType());
            return locations.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    @Test(dataProvider = "validLocationsFromJson", enabled = false)
    public void testCreateLocation(Locations createdLocation) throws IOException {
        testrailID = 9240;
        Set<Locations> oldLocations = getLocations();
        Locations newLocation = new Locations()
                .withLocationName(createdLocation.getLocationName())
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus());
        String locationID = createLocationLong(newLocation);
        Set<Locations> newLocations = getLocations();
        assertTrue(newLocations.contains(newLocation.withRealEstateId(locationID)));
        /*oldLocations.add(newLocation.withRealEstateId(locationID));
        assertEquals(newLocations.size(), oldLocations.size());*/
    }

    @Test
    public void testCreateLocationUS() throws IOException {
        testrailID = 32777;
        Set<Locations> oldLocations = getLocations();
        Locations newLocation = new Locations()
                .withLocationName("REST-test-locationNameUS" + HelperBase.randInt(1, 100))
                .withAddress1("601 Brickell Key Dr #1000")
                .withCity("Miami")
                .withState("FL")
                .withCountry("US")
                .withZipCode("33131");
        String locationID = createLocationBrief(newLocation);
        System.out.println("Created location with real estate id: " + locationID);
        Set<Locations> newLocations = getLocations();
        oldLocations.add(newLocation.withRealEstateId(locationID));
        assertEquals(newLocations.size(), oldLocations.size());
    }

    @Test
    public void testCreateLocationCA() throws IOException {
        testrailID = 9242;
        Set<Locations> oldLocations = getLocations();
        Locations newLocation = new Locations()
                .withLocationName("REST-test-locationNameCA"+ HelperBase.randInt(1, 100))
                .withAddress1("61-95 W Hastings St")
                .withCity("Vancouver")
                .withState("BC")
                .withCountry("CA")
                .withZipCode("V6B 1G4");
        String locationID = createLocationBrief(newLocation);
        System.out.println("Created location with real estate id: " + locationID);
        Set<Locations> newLocations = getLocations();
        oldLocations.add(newLocation.withRealEstateId(locationID));
        assertEquals(newLocations.size(), oldLocations.size());
    }

    @Test
    public void testCreateLocationInvalidAuthorization() throws IOException {
        testrailID = 21798;
        Locations newLocation = new Locations()
                .withLocationName("REST-Invalid-locationNameCA")
                .withAddress1("150 Nassau St Garage")
                .withCity("New York")
                .withState("NY")
                .withCountry("US")
                .withZipCode("10038")
                .withLocationStatus("ACTIVE");
        int statusCode = createInvalidAuthorizationLocation(newLocation);
        assertEquals(statusCode, 401);
    }

    @Test
    public void testCreateLocationCAInvalidZIP() throws IOException {
        testrailID = 9243;
        Locations newLocation = new Locations()
                .withLocationName("REST-Invalid-locationNameCA")
                .withAddress1("150 Nassau St Garage")
                .withCity("New York")
                .withState("NY")
                .withCountry("US")
                .withZipCode("QWER");
        int statusCode = createInvalidLocation(newLocation);
        assertEquals(statusCode, 400);
    }

    @Test(enabled = false)
    public void testModifyCreatedLocationUS() throws IOException {
        Set<Locations> oldLocations = getLocations();
        Locations newLocation = new Locations()
                .withLocationName("REST-test-locationNameUS")
                .withAddress1("123 Sample Street")
                .withCity("Miami")
                .withState("FL")
                .withCountry("US")
                .withZipCode("33131")
                .withLocationStatus("ACTIVE");
        String locationID = createLocationBrief(newLocation);


        Set<Locations> newLocations = getLocations();
        oldLocations.add(newLocation.withRealEstateId(locationID));
        assertEquals(newLocations, oldLocations);
    }

    private Set<Locations> getLocations() throws IOException {
        String json = getExecutor().execute(Request
                .Get(app.endPointURI + "?pageSize=9000&sortBy=desc%3ArealEstateId")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader)
                .addHeader("referer", "https://backend-dev-re.reefplatform.com/location/swagger-ui.html"))
                .returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        JsonElement locations = parsed.getAsJsonObject().get("locations");
        return new Gson().fromJson(locations, new TypeToken<Set<Locations>>() {
        }.getType());
    }

    private String createLocationBrief(Locations newLocation) throws IOException {
        String json = null;
        try {
            json = getExecutor().execute(Request
                    .Post(app.endPointURI)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("accept", "application/json")
                    .addHeader("Authorization", app.authorizationHeader)
                    .addHeader("organization", app.organizationHeader)
                    .bodyString("{\n" +
                            "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                            "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                            "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                            "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                            "  \"state\": \"" + newLocation.getState() + "\",\n" +
                            "  \"locationStatus\": \"ACTIVE\",\n" +
                            "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                            "}", ContentType.APPLICATION_JSON))
                    .returnContent().asString();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        }
        JsonElement parsed = new JsonParser().parse(json);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }

    private String createLocationLong(Locations newLocation) throws IOException {
        String json = getExecutor().execute(Request
                .Post(app.endPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader)
                .bodyString("{\n" +
                                "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                                "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                                "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                                "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                                "  \"state\": \"" + newLocation.getState() + "\",\n" +
                                "  \"locationType\": \"" + newLocation.getPropertyType() + "\",\n" +
                                "  \"locationSubType\": \"" + newLocation.getLocationSubType() + "\",\n" +
                                "  \"locationMode\": \"" + newLocation.getLocationMode() + "\",\n" +
                                "  \"propertyName\": \"" + newLocation.getPropertyName() + "\",\n" +
                                "  \"addressLine2\": \"" + newLocation.getAddressLine2() + "\",\n" +
                                "  \"locationStatus\": \"" + newLocation.getLocationStatus() + "\",\n" +
                                "  \"latitude\": \"" + newLocation.getLatitude() + "\",\n" +
                                "  \"longitude\": \"" + newLocation.getLongitude() + "\",\n" +
                                "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                                "}"
                        , ContentType.APPLICATION_JSON))
                .returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        System.out.println(parsed);
        return parsed.getAsJsonObject().get("realEstateId").getAsString();
    }

    private int createInvalidLocation(Locations newLocation) throws IOException {
        return getExecutor().execute(Request
                .Post(app.endPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader)
                .bodyString("{\n" +
                        "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                        "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                        "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                        "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                        "  \"state\": \"" + newLocation.getState() + "\",\n" +
                        "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                        "}", ContentType.APPLICATION_JSON))
                .returnResponse().getStatusLine().getStatusCode();
    }

    private int createInvalidAuthorizationLocation(Locations newLocation) throws IOException {
        return getExecutor().execute(Request
                .Post(app.endPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .addHeader("organization", app.organizationHeader)
                .bodyString("{\n" +
                        "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                        "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                        "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                        "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                        "  \"state\": \"" + newLocation.getState() + "\",\n" +
                        "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                        "}", ContentType.APPLICATION_JSON))
                .returnResponse().getStatusLine().getStatusCode();
    }
}

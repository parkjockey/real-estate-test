package com.reeftechnology.realestate.rest.tests;

import com.reeftechnology.realestate.rest.model.ExternalId;
import com.reeftechnology.realestate.rest.model.Identifiers;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.Assert.assertEquals;

public class IdUpdateTests extends TestBase {

    public String nonexistingSystemID;

    @Test
    public void testUpdateExistingRealEstateIdentifiers() throws IOException {
        testrailID = 19788;
        List<ExternalId> externalIds = new ArrayList<>();
        externalIds.add(new ExternalId().withExternalIdType("Location ID").withExternalIdValue("Location ID AP30291700"));
        Identifiers newIdentifierData = new Identifiers()
                .withCountry("US")
                .withExternalIds(externalIds)
                .withState("FL")
                .withType("RE");
        Identifiers existingIDData = createNewIdentifierWithExternalID(newIdentifierData);

        externalIds.clear();
        externalIds.add(new ExternalId().withExternalIdType("Lot ID").withExternalIdValue("Modified lot ID L21088900"));
        Identifiers modifiedIdentifier = existingIDData.withExternalIds(externalIds);
        Identifiers expectedIdentifier = modifyExistingIdentifier(modifiedIdentifier, existingSystemID);
        System.out.println("Modified real estate id: " + modifiedSystemID);

        assertThat(expectedIdentifier, equalTo(modifiedIdentifier));
        assertThat(existingSystemID, equalTo(modifiedSystemID));
    }

    @Test
    public void testUpdateWithInvalidRealEstateIdentifiers() throws IOException {
        testrailID = 36717;
        List<ExternalId> externalIds = new ArrayList<>();
        externalIds.add(new ExternalId().withExternalIdType("INVALID").withExternalIdValue(" "));
        Identifiers newIdentifierData = new Identifiers()
                .withCountry("US")
                .withExternalIds(externalIds)
                .withState("FL")
                .withType("RE");

        int statusCode = modifyExistingIdentifierInvalidData(newIdentifierData);
        assertEquals(statusCode, 400);
    }

    @Test
    public void testUpdateNonExistingRealEstateIdentifiers() throws IOException {
        testrailID = 36717;
        List<ExternalId> externalIds = new ArrayList<>();
        externalIds.add(new ExternalId().withExternalIdType("Lot ID").withExternalIdValue("AP30291700"));
        Identifiers newIdentifierData = new Identifiers()
                .withCountry("US")
                .withExternalIds(externalIds)
                .withState("FL")
                .withType("");

        nonexistingSystemID = "XX-FL-RE-1204009";
        int statusCode = modifyNonExistingIdentifier(newIdentifierData, nonexistingSystemID);
        System.out.println("Tried to modify real estate id: " + nonexistingSystemID);

        assertEquals(statusCode, 400);
    }

    @Test
    public void testUpdateThenUpdateAgainExistingRealEstateIdentifiers() throws IOException {
        testrailID = 36716;
        List<ExternalId> externalIds = new ArrayList<>();
        externalIds.add(new ExternalId().withExternalIdType("Lot ID").withExternalIdValue("AP30291700"));
        Identifiers newIdentifierData = new Identifiers()
                .withCountry("US")
                .withExternalIds(externalIds)
                .withState("FL")
                .withType("RE");
        Identifiers existingIDData = createNewIdentifierWithExternalID(newIdentifierData);

        externalIds.clear();
        externalIds.add(new ExternalId().withExternalIdType("Lot ID").withExternalIdValue("Modified L21088900"));
        Identifiers modifiedIdentifier = existingIDData.withExternalIds(externalIds);
        System.out.println("Modified first real estate id: " + modifiedSystemID);

        externalIds.clear();
        externalIds.add(new ExternalId().withExternalIdType("Location ID").withExternalIdValue("Modified ABC123456"));
        modifiedIdentifier = existingIDData.withExternalIds(externalIds);
        Identifiers expectedIdentifier = modifyExistingIdentifier(modifiedIdentifier, existingSystemID);
        System.out.println("Modified second real estate id: " + modifiedSystemID);

        assertThat(expectedIdentifier, equalTo(modifiedIdentifier));
        assertThat(existingSystemID, equalTo(modifiedSystemID));
    }

    private int modifyExistingIdentifierInvalidData(Identifiers newIdentifier) throws IOException {
        return getExecutor().execute(Request
                .Put(app.iDServiceEndPointURI + "/" + existingSystemID)
                .addHeader("Content-Type", "application/json")
                .addHeader("x-api-key", app.idService_x_api_key)
                .addHeader("accept", "application/json")
                .bodyString("{ \"id\": \"" + existingSystemID + "\"," +
                                " \"externalIds\": [ { \"externalIdType\": \"" + newIdentifier.getExternalIds().get(0).getExternalIdType()
                                + "\", \"value\": \"" + newIdentifier.getExternalIds().get(0).getExternalIdValue() + "\" } ]}",
                        ContentType.APPLICATION_JSON)).returnResponse().getStatusLine().getStatusCode();
    }

    private int modifyNonExistingIdentifier(Identifiers newIdentifier, String nonexistingSystemID) throws IOException {
        return getExecutor().execute(Request
                .Put(app.iDServiceEndPointURI + "/" + nonexistingSystemID)
                .addHeader("Content-Type", "application/json")
                .addHeader("x-api-key", app.idService_x_api_key)
                .addHeader("accept", "application/json")
                .bodyString("{ \"id\": \"" + existingSystemID + "\"," +
                                " \"externalIds\": [ { \"externalIdType\": \"" + newIdentifier.getExternalIds().get(0).getExternalIdType()
                                + "\", \"value\": \"" + newIdentifier.getExternalIds().get(0).getExternalIdValue() + "\" } ]}",
                        ContentType.APPLICATION_JSON)).returnResponse().getStatusLine().getStatusCode();
    }

}

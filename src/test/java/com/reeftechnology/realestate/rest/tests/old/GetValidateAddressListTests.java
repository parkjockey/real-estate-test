package com.reeftechnology.realestate.rest.tests.old;

import com.reeftechnology.realestate.rest.model.Campus;
import com.reeftechnology.realestate.rest.tests.TestBase;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Set;

import static org.testng.Assert.assertNotEquals;

public class GetValidateAddressListTests extends TestBase {

    @Test
    public void testGetWithAllOptionalAndRequiredParams() throws IOException {
        testrailID = 39284;
        Set<Campus> addressList = getValidatesAdressList();
        assertNotEquals(addressList.size(), 0);
    }

}

package com.reeftechnology.realestate.rest.tests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.Locations;
import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public class CreateLocationTests extends TestBase {

    @DataProvider
    public Iterator<Object[]> validLocationsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations-rest.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Locations> locations = gson.fromJson(json,
                    new TypeToken<List<Locations>>() {
                    }.getType());
            return locations.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    @Test(dataProvider = "validLocationsFromJson")
    public void testPostWithAllOptionalAndRequiredData(Locations createdLocation) throws IOException {
        testrailID = 9240;
        Set<Locations> oldLocations = readLocationsList();
        Locations newLocation = new Locations()
                .withLocationName(createdLocation.getLocationName() + HelperBase.randInt(1, 100))
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus())
                .withPreciselyStreetDataType(createdLocation.getPreciselyStreetDataType())
                .withPreciselyTzId(createdLocation.getPreciselyTzId())
                .withPreciselyPbKey(createdLocation.getPreciselyPbKey())
                .withPreciselyZipCode(createdLocation.getPreciselyZipCode());

        String locationID = createLocationWithRequiredAndOptionalData(newLocation);
        System.out.println("Created location with real estate id: " + locationID);

        Set<Locations> newLocations = readLocationsList();
        oldLocations.add(newLocation.withRealEstateId(locationID));
        assertEquals(newLocations.size(), oldLocations.size());
    }

    @Test
    public void testPostWithOnlyRequiredData() throws IOException {
        testrailID = 9242;
        Set<Locations> oldLocations = readLocationsList();
        Locations newLocation = new Locations()
                .withLocationName("REST-TEST-61-95 W Hastings St -" + HelperBase.randInt(1, 100))
                .withAddress1("61-95 W Hastings St")
                .withAddress2("250 Hospital")
                .withCity("Vancouver")
                .withState("BC")
                .withCountry("CA")
                .withZipCode("V6B 1G4")
                .withLocationPropertyType("Country Club")
                .withPropertyName("61 & 95 West Hastings Street")
                .withLocationStatus("Active")
                .withLatitude(49.28216)
                .withLongitude(-123.10651);
        String locationID = createLocationWithRequiredData(newLocation);
        System.out.println("Created location with real estate id: " + locationID);
        Set<Locations> newLocations = readLocationsList();
        oldLocations.add(newLocation.withRealEstateId(locationID));
        assertEquals(newLocations.size(), oldLocations.size());
    }

    @Test
    public void testPostWithRequiredDataMissing() throws IOException {
        testrailID = 9241;
        Locations newLocation = new Locations()
                .withLocationName("")
                .withAddress1("123 Sample Street")
                .withCity("Athabasca")
                .withState("AB")
                .withCountry("CA")
                .withZipCode("T9S1A4");
        int statusCode = createInvalidLocation(newLocation);
        assertEquals(statusCode, 400);
    }

    @Test
    public void testPostWithInvalidDataForParameters() throws IOException {
        testrailID = 9243;
        Locations newLocation = new Locations()
                .withLocationName("REST-test-locationNameCA")
                .withAddress1("123 Sample Street")
                .withCity("Athabasca")
                .withState("AB")
                .withCountry("CA")
                .withZipCode("T9S1A44");
        int statusCode = createInvalidLocation(newLocation);
        assertEquals(statusCode, 400);
    }

    private int createInvalidLocation(Locations newLocation) throws IOException {
        String response = getExecutor().execute(Request
                .Post(app.endPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader)
                .bodyString("{\n" +
                        "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                        "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                        "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                        "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                        "  \"state\": \"" + newLocation.getState() + "\",\n" +
                        "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                        "}", ContentType.APPLICATION_JSON))
                .returnResponse().toString();
        System.out.println(response);

        return getExecutor().execute(Request
                .Post(app.endPointURI)
                .addHeader("Content-Type", "application/json")
                .addHeader("accept", "application/json")
                .addHeader("Authorization", app.authorizationHeader)
                .bodyString("{\n" +
                        "  \"addressLine1\": \"" + newLocation.getAddressLine1() + "\",\n" +
                        "  \"city\": \"" + newLocation.getCity() + "\",\n" +
                        "  \"country\": \"" + newLocation.getCountry() + "\",\n" +
                        "  \"locationName\": \"" + newLocation.getLocationName() + "\",\n" +
                        "  \"state\": \"" + newLocation.getState() + "\",\n" +
                        "  \"zipCode\": \"" + newLocation.getZipCode() + "\"\n" +
                        "}", ContentType.APPLICATION_JSON))
                .returnResponse().getStatusLine().getStatusCode();
    }
}

package com.reeftechnology.realestate.rest.tests.old;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.Campus;
import com.reeftechnology.realestate.rest.tests.TestBase;
import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;

public class CreateCampusTests extends TestBase {

    @DataProvider
    public Iterator<Object[]> validCampusesFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/campuses-rest.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Campus> campuses = gson.fromJson(json,
                    new TypeToken<List<Campus>>() {
                    }.getType());
            return campuses.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    @Test(dataProvider = "validCampusesFromJson")
    public void testPostWithAllOptionalAndRequiredData(Campus campusData) throws IOException {
        testrailID = 39255;
        Set<Campus> oldCampuses = readCampusesList();
        Campus newCampus = new Campus()
                .withCampusName(campusData.getCampusName() + HelperBase.randInt(1, 100))
                .withCampusType(campusData.getCampusType())
                .withCreatedBy(campusData.getCreatedBy())
                .withCreatedDate(campusData.getCreatedDate())
                .withDescription(campusData.getDescription());

        JsonElement campus = createNewCampus(newCampus);
        String campusId = campus.getAsJsonObject().get("campusId").getAsString();
        System.out.println("Created campus with id: " + campusId);

        Set<Campus> newCampuses = readCampusesList();
        oldCampuses.add(newCampus);
        assertEquals(newCampuses.size(), oldCampuses.size());
    }

    @Test
    public void testPostWithOnlyRequiredData() throws IOException {
        testrailID = 39256;

        Set<Campus> oldCampuses = readCampusesList();
        Campus newCampus = new Campus()
                .withCampusName("REST-TEST-Campus" + HelperBase.randInt(1, 100))
                .withCampusType("Landlord")
                .withDescription("REST-TEST-Campus desccription");

        JsonElement campus = createNewCampus(newCampus);
        String campusId = campus.getAsJsonObject().get("campusId").getAsString();
        System.out.println("Created campus with id: " + campusId);

        Set<Campus> newCampuses = readCampusesList();
        oldCampuses.add(newCampus);
        assertEquals(newCampuses.size(), oldCampuses.size());
    }

    @Test
    public void testPostWithRequiredDataMissing() throws IOException {
        testrailID = 39257;

        Campus newCampus = new Campus()
                .withCampusName("REST-TEST-Campus" + HelperBase.randInt(1, 100))
                .withCampusType("")
                .withDescription("");

        int statusCode = createInvalidCampus(newCampus);
        assertEquals(statusCode, 400);
    }

    @Test
    public void testPostWithInvalidDataForParametersCampusType() throws IOException {
        testrailID = 39258;

        Set<Campus> oldCampuses = readCampusesList();
        Campus newCampus = new Campus()
                .withCampusName("REST-TEST-Campus" + HelperBase.randInt(1, 100))
                .withCampusType("LANDLORD")
                .withDescription("REST-TEST-Campus desccription");

        int statusCode = createInvalidCampus(newCampus);
        assertEquals(statusCode, 400);

        Set<Campus> newCampuses = readCampusesList();
        assertEquals(newCampuses.size(), oldCampuses.size());

        /*String message = campus.getAsJsonObject().get("validationErrors").getAsJsonArray().get(0)
                .getAsJsonObject().get("message").getAsString();
        System.out.println("Validation error: " + message);
        assertThat(message, equalTo("Campus Type not valid"));

        Set<Campus> newCampuses = readCampusesList();
        assertEquals(newCampuses.size(), oldCampuses.size());*/
    }

    private JsonElement createNewCampus(Campus campus) throws IOException {
        String json = getExecutor().execute(Request
                .Post(app.campusEndPointURI)
                .addHeader("accept", "application/json")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader)
                .bodyString("{ \"campusName\": \"" + campus.getCampusName() + "\", " +
                                "\"campusType\": \"" + campus.getCampusType() + "\", " +
                                "\"createdBy\": \"" + campus.getCreatedBy() + "\", " +
                                "\"createdDate\": \"" + campus.getCreatedDate() + "\", " +
                                "\"description\": \"" + campus.getDescription() + "\"}",
                        ContentType.APPLICATION_JSON)).returnContent().asString();
        JsonElement parsed = new JsonParser().parse(json);
        System.out.println(parsed);
        return parsed;
    }

    private int createInvalidCampus(Campus campus) throws IOException {
        String json = getExecutor().execute(Request
                .Post(app.campusEndPointURI)
                .addHeader("accept", "application/json")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader)
                .bodyString("{ \"campusName\": \"" + campus.getCampusName() + "\", " +
                                "\"campusType\": \"" + campus.getCampusType() + "\", " +
                                "\"createdBy\": \"" + campus.getCreatedBy() + "\", " +
                                "\"createdDate\": \"" + campus.getCreatedDate() + "\", " +
                                "\"description\": \"" + campus.getDescription() + "\"}",
                        ContentType.APPLICATION_JSON)).returnResponse().toString();
        System.out.println(json);

        return getExecutor().execute(Request
                .Post(app.campusEndPointURI)
                .addHeader("accept", "application/json")
                .addHeader("Authorization", app.authorizationHeader)
                .addHeader("organization", app.organizationHeader)
                .bodyString("{ \"campusName\": \"" + campus.getCampusName() + "\", " +
                                "\"campusType\": \"" + campus.getCampusType() + "\", " +
                                "\"createdBy\": \"" + campus.getCreatedBy() + "\", " +
                                "\"createdDate\": \"" + campus.getCreatedDate() + "\", " +
                                "\"description\": \"" + campus.getDescription() + "\"}",
                        ContentType.APPLICATION_JSON))
                .returnResponse().getStatusLine().getStatusCode();
    }

}

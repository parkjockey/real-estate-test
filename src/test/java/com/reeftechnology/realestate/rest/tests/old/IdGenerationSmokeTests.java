package com.reeftechnology.realestate.rest.tests.old;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.ExternalId;
import com.reeftechnology.realestate.rest.model.Identifiers;
import com.reeftechnology.realestate.rest.tests.TestBase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.Assert.assertEquals;

public class IdGenerationSmokeTests extends TestBase {

    @DataProvider
    public Iterator<Object[]> validIdsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/ids-stage.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Identifiers> ids = gson.fromJson(json,
                    new TypeToken<List<Identifiers>>() {
                    }.getType()); //List <Identifiers>.class
            return ids.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }

    @Test
    public void testCreateRealEstateIdentifiers() throws IOException {
        testrailID = 7003;
        Identifiers newIdentifierData = new Identifiers()
                .withCountry("US")
                .withState("FL")
                .withType("RE");
        Identifiers generatedIDData = createNewIdentifier(newIdentifierData);
        System.out.println("Created real estate id: " + existingSystemID);
        assertThat(generatedIDData, equalTo(newIdentifierData));
        assertThat(existingSystemID, notNullValue());
    }

    @Test
    public void testCreateRealEstateIdentifiersWithMissingData() throws IOException {
        testrailID = 7528;
        Identifiers newIdentifierData = new Identifiers()
                .withCountry("")
                .withState("")
                .withType("");
        int statusCode = createNewIdentifierInvalidData(newIdentifierData);
        assertEquals(statusCode, 400);
    }

    @Test
    public void testUpdateExistingRealEstateIdentifiers() throws IOException {
        testrailID = 19788;
        List<ExternalId> externalIds = new ArrayList<>();
        externalIds.add(new ExternalId().withExternalIdType("Lot ID").withExternalIdValue("AP30291700"));
        Identifiers newIdentifierData = new Identifiers()
                .withCountry("US")
                .withExternalIds(externalIds)
                .withState("FL")
                .withType("RE");
        Identifiers existingIDData = createNewIdentifierWithExternalID(newIdentifierData);

        externalIds.clear();
        externalIds.add(new ExternalId().withExternalIdType("Lot ID").withExternalIdValue("Modified L21088900"));
        Identifiers modifiedIdentifier = existingIDData.withExternalIds(externalIds);
        Identifiers expectedIdentifier = modifyExistingIdentifier(modifiedIdentifier, existingSystemID);
        System.out.println("Modified real estate id: " + modifiedSystemID);

        assertThat(expectedIdentifier, equalTo(modifiedIdentifier));
        assertThat(existingSystemID, equalTo(modifiedSystemID));
    }

    @Test
    public void testGetRealEstateIdentifierWhenIdExist() throws IOException {
        testrailID = 11772;
        Identifiers newIdentifierData = new Identifiers()
                .withCountry("US")
                .withState("CA")
                .withType("RE");
        Identifiers generatedIDData = createNewIdentifier(newIdentifierData);
        System.out.println("Created real estate id: " + existingSystemID);

        Identifiers exepectedIDData = getIdentifier(existingSystemID);
        assertThat(generatedIDData, equalTo(exepectedIDData));
    }
}

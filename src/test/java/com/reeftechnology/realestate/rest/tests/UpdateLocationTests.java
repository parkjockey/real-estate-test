package com.reeftechnology.realestate.rest.tests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reeftechnology.realestate.rest.model.Locations;
import com.reeftechnology.realestate.webui.appmanager.HelperBase;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class UpdateLocationTests extends TestBase {

    @DataProvider
    public Iterator<Object[]> validLocationsFromJson() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(
                new File("src/test/resources/testdata/locations-rest.json")))) {
            String json = "";
            String line = reader.readLine();
            while (line != null) {
                json += line;
                line = reader.readLine();
            }
            Gson gson = new Gson();
            List<Locations> locations = gson.fromJson(json,
                    new TypeToken<List<Locations>>() {
                    }.getType());
            return locations.stream().map((l) -> new Object[]{l})
                    .collect(Collectors.toList()).iterator();
        }
    }


    @Test(dataProvider = "validLocationsFromJson")
    public void testUpdateExistingLocation(Locations createdLocation) throws IOException {
        testrailID = 11863;
        //arrange
        Set<Locations> oldLocations = readLocationsList();
        Locations existingLocation = new Locations()
                .withLocationName(createdLocation.getLocationName() + HelperBase.randInt(1, 100))
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus())
                .withPreciselyStreetDataType(createdLocation.getPreciselyStreetDataType())
                .withPreciselyTzId(createdLocation.getPreciselyTzId())
                .withPreciselyPbKey(createdLocation.getPreciselyPbKey())
                .withPreciselyZipCode(createdLocation.getPreciselyZipCode());
        String locationID = createLocationWithRequiredAndOptionalData(existingLocation);
        System.out.println("Location with real estate id: " + locationID + " exist");
        //verify that locations list updated with new location
        Set<Locations> newLocations = readLocationsList();
        oldLocations.add(existingLocation.withRealEstateId(locationID));
        assertThat(newLocations.size(), equalTo(oldLocations.size()));

        //act
        Locations existingLocationModified = new Locations()
                .withLocationName("REST-TEST-Enterprise Ann Parking" + HelperBase.randInt(1, 100))
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName("Enterprise Ann Parking, LLC. 57 Ann Street")
                .withAddress1("57 Ann Street")
                .withAddress2("Parking garage")
                .withCity("New York")
                .withState("NY")
                .withCountry(createdLocation.getCountry())
                .withZipCode("10038")
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus())
                .withPreciselyStreetDataType(createdLocation.getPreciselyStreetDataType())
                .withPreciselyTzId(createdLocation.getPreciselyTzId())
                .withPreciselyPbKey(createdLocation.getPreciselyPbKey())
                .withPreciselyZipCode(createdLocation.getPreciselyZipCode());
        locationID = modifyLocationWithRequiredData(existingLocationModified, locationID, "1");
        //verify that locations list is not changed after update
        newLocations = readLocationsList();
        assertThat(newLocations.size(), equalTo(oldLocations.size()));

        //assert - verify that updated location info is available by real estate id
        Locations expectedLocation = getBriefLocationinfoById(locationID);
        assertThat(expectedLocation.getLocationName(), equalTo(existingLocationModified.getLocationName()));
        assertThat(expectedLocation.getAddressLine1(), equalTo(existingLocationModified.getAddressLine1()));
        assertThat(expectedLocation.getCity(), equalTo(existingLocationModified.getCity()));
    }

    @Test(dataProvider = "validLocationsFromJson")
    public void testUpdateThenUpdateAgainExistingLocation(Locations createdLocation) throws IOException {
        testrailID = 11864;
        //arrange
        Set<Locations> oldLocations = readLocationsList();
        Locations existingLocation = new Locations()
                .withLocationName(createdLocation.getLocationName())
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName(createdLocation.getPropertyName())
                .withAddress1(createdLocation.getAddressLine1())
                .withAddress2(createdLocation.getAddressLine2())
                .withCity(createdLocation.getCity())
                .withState(createdLocation.getState())
                .withCountry(createdLocation.getCountry())
                .withZipCode(createdLocation.getZipCode())
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus());
        String locationID = createLocationWithRequiredAndOptionalData(existingLocation);
        System.out.println("Location with real estate id: " + locationID + " exist");
        //verify that locations list updated with new location
        Set<Locations> newLocations = readLocationsList();
        oldLocations.add(existingLocation.withRealEstateId(locationID));
        assertThat(newLocations.size(), equalTo(oldLocations.size()));

        //act
        Locations existingLocationModified = new Locations()
                .withLocationName("REST-TEST-Enterprise Ann Parking 2-" + HelperBase.randInt(1, 100))
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName("Enterprise Ann Parking, LLC. 57 Ann Street")
                .withAddress1("57 Ann Street")
                .withAddress2("Parking garage")
                .withCity("New York")
                .withState("NY")
                .withCountry(createdLocation.getCountry())
                .withZipCode("10038")
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus())
                .withPreciselyStreetDataType(createdLocation.getPreciselyStreetDataType())
                .withPreciselyTzId(createdLocation.getPreciselyTzId())
                .withPreciselyPbKey(createdLocation.getPreciselyPbKey())
                .withPreciselyZipCode(createdLocation.getPreciselyZipCode());
        locationID = modifyLocationWithRequiredData(existingLocationModified, locationID, "1");
        //verify that locations list is not changed after update
        newLocations = readLocationsList();
        assertThat(newLocations.size(), equalTo(oldLocations.size()));

        //then update same location again
        Locations existingLocationModified2 = new Locations()
                .withLocationName("REST-TEST-2121 Ponce de Leon 3-" + HelperBase.randInt(1, 100))
                .withLocationPropertyType(createdLocation.getPropertyType())
                .withLocationSubType(createdLocation.getLocationSubType())
                .withLocationMode(createdLocation.getLocationMode())
                .withPropertyName("2121 Ponce de Leon")
                .withAddress1("2121 Ponce De Leon Blvd")
                .withCity("Coral Gables")
                .withState("FL")
                .withCountry(createdLocation.getCountry())
                .withZipCode("33134")
                .withLatitude(createdLocation.getLatitude())
                .withLongitude(createdLocation.getLongitude())
                .withLocationStatus(createdLocation.getLocationStatus())
                .withPreciselyStreetDataType(createdLocation.getPreciselyStreetDataType())
                .withPreciselyTzId(createdLocation.getPreciselyTzId())
                .withPreciselyPbKey(createdLocation.getPreciselyPbKey())
                .withPreciselyZipCode(createdLocation.getPreciselyZipCode());
        locationID = modifyLocationWithRequiredData(existingLocationModified2, locationID, "2");
        //verify that locations list is not changed after update
        newLocations = readLocationsList();
        assertThat(newLocations.size(), equalTo(oldLocations.size()));

        //assert - verify that updated location info is available by real estate id
        Locations expectedLocation = getBriefLocationinfoById(locationID);
        assertThat(expectedLocation.getLocationName(), equalTo(existingLocationModified2.getLocationName()));
        assertThat(expectedLocation.getAddressLine1(), equalTo(existingLocationModified2.getAddressLine1()));
        assertThat(expectedLocation.getCity(), equalTo(existingLocationModified2.getCity()));

    }

}

package com.reeftechnology.realestate.rest.tests.old;

import com.reeftechnology.realestate.rest.model.Campus;
import com.reeftechnology.realestate.rest.tests.TestBase;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Set;

import static org.testng.Assert.assertNotEquals;

public class GetCampusListTests extends TestBase {

    @Test
    public void testGetWithAllOptionalAndRequiredParams() throws IOException {
        testrailID = 39284;
        Set<Campus> campuses = readCampusesList();
        assertNotEquals(campuses.size(), 0);
    }

}

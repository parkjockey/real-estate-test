package com.reeftechnology.realestate.rest.appmanager;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ApplicationManager {
    public final Properties properties;
    public String target;
    public String endPointURI;
    public String campusEndPointURI;
    public String addressEndPointURI;
    public String iDServiceEndPointURI;
    public String authorizationHeader;
    public String organizationHeader;
    public String idService_x_api_key;

    public ApplicationManager() {
        properties = new Properties();
    }

    public Properties getProperties() {
        return properties;
    }

    public void init() throws IOException {
        target = System.getProperty("target", "dev");
        properties.load(new FileReader(new File(String.format("src/test/resources/config/%s.properties", target))));
        endPointURI = properties.getProperty("rest.endPointURI");
        authorizationHeader = properties.getProperty("rest.authorizationHeader");
        organizationHeader = properties.getProperty("rest.organizationHeader");
        iDServiceEndPointURI = properties.getProperty("rest.iDServiceEndPointURI");
        campusEndPointURI = properties.getProperty("rest.campusEndPointURI");
        idService_x_api_key = properties.getProperty("rest.x-api-key");
        addressEndPointURI = properties.getProperty("rest.addressServiceEndPointURI");
    }

    public void stop() {
    }
}
